<?php

namespace app\modules\cms\controllers;

use app\fhhlib\ApiAdm;
use yii\helpers\Url;
use yii\web\Controller;
use app\modules\cms\libs\SimpleDataTableInit;
use app\modules\cms\models\SlideFormModel;
use yii\web\UploadedFile;

/**
 * Default controller for the `cms` module
 */
class ImageSlideController extends Controller
{
    public $layout = '@app/views/layouts/backmain.php';

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    function actionLists(){
        $request = \Yii::$app->request->get();
        
        $response = ApiAdm::getListsSlide($request['start'] ?? 0,$request['length'] ?? 2);
        $response = json_decode($response,true);
       
        $dataTable = new SimpleDataTableInit;

        if(!empty($response['data']['list'])){
            $dataTable->draw($request['draw']);
            $dataTable->recordsTotal($response['data']['recordsTotal']);
            $dataTable->recordsFiltered($response['data']['recordsFiltered']);

            $list = [];
            foreach ($response['data']['list'] as $r) {
                $list[] = array(
                    $dataTable->displayImage($r['url_link']),
                    $r['created_ad'] ?? '-',
                    $dataTable->getAction([
                        'edit' => [
                            'url' => Url::to(['/cms/image-slide/update','id'=>$r['id']])
                        ],
                        'delete' => [
                            'url' => Url::to(['/cms/image-slide/delete','id'=>$r['id']])
                        ]
                    ])
                );
            }

            $dataTable->data($list);
        }

        return json_encode($dataTable->results());
    }

    public function actionCreate(){
        $data['slideModel'] = new SlideFormModel();

        return $this->render('form_slide',$data);
    }

    public function actionNew(){
        $request = \Yii::$app->request->post()['SlideFormModel'];

        try {
            $picture = new SlideFormModel();
            $picture->picture = UploadedFile::getInstance($picture, 'picture');

            $data = [
                'judul' => $request['judul'],
                'description' => $request['description'],
                'action_link' => $request['action_link'],
                'url_link' => $picture->upload(),
                'updated_at' => date('Y-m-d H:i:s')
            ];

            $response = ApiAdm::insertSlide($data);
            $response = json_decode($response,true);    
        } catch (\Throwable $th) {
            //throw $th;
        }
        return $this->redirect(['image-slide/index']);
    }

    public function actionUpdate(){
        $request = \Yii::$app->request->get();
        $response = ApiAdm::getSingleSlide($request['id']);
        $response = json_decode($response,true);
        
        $model = new SlideFormModel();
        $d = $response['data'];
        if(!empty($d)){
            $model->hiddenAttr = $d['url_link'];
            $model->url_link = $d['url_link'];
            $model->judul = $d['judul'];
            $model->description = $d['description'];
            $model->action_link = $d['action_link'];
        }

        $data['id'] = $request['id'];
        $data['slideModel'] = $model;

        return $this->render('form_slide',$data);
    }

    public function actionSave(){
        $request = \Yii::$app->request->post();

        $response = ApiAdm::getSingleSlide($request['id']);
        $response = json_decode($response,true)['data'];

        try {
            $picture = new SlideFormModel();
            $picture->picture = UploadedFile::getInstance($picture, 'picture');
            
            $data = [
                'id' => $request['id'],
                'judul' => $request['SlideFormModel']['judul'],
                'description' => $request['SlideFormModel']['description'],
                'action_link' => $request['SlideFormModel']['action_link'],
                'updated_at' => date('Y-m-d H:i:s')
            ];

            if(!empty($picture->picture)){
                unlink(Yii::$app->basePath . '/web/' . $response->url_link);
                $data['url_link'] = $picture->upload();
            }

            $response = ApiAdm::updateSingleSlide($data);
            $response = json_decode($response,true);    
        } catch (\Throwable $th) {
            //throw $th;
        }
        return $this->redirect(['image-slide/index']);
    }

    public function actionDelete(){
        $request = \Yii::$app->request->get();

        try {
            // GET SINGLE DATA
            $response = ApiAdm::getSingleSlide($request['id']);
            $response = json_decode($response,true);
           
            // DELETE DATA
            $response = ApiAdm::deleteSingleSlide($request['id']);
            $response = json_decode($response,true);
    
            if($response['meta']['code'] === 200 && $response['data']['type'] === 'upload'){
                $urlImageLink = $response['data']['url_link'];
                if(!empty($urlImageLink)); unlink(Yii::$app->basePath . '/web/' . $response['data']['url_link']);
            }
        } catch (\Throwable $th) {
            //throw $th;
        }

        return $this->redirect(['image-slide/index']);
    }
}
