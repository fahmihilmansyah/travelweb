<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 17/07/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 09.13
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
 ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title-font judul-donasi-big bold mb-15"><?php echo $list['data']['title'] ?></div>
                <img src="<?php echo $list['data']['img_link'] ?>" alt="Image">
                <div class="mt-15 mr-15 text-smaller">
                    <span>
                        <i class="text-muted la la-user mr-5"></i> <?php echo $list['data']['name_author'] ?>
                    </span>
                    <span class="ml-15">
                        <i class="text-muted la la-calendar mr-5"></i> <?php echo $list['data']['created_ad'] ?>
                    </span>
                </div>
                <div class="mt-25 mb-40">
                    <?php echo $list['data']['desc'] ?>
                </div>
            </div>
        </div>
    </div>
</section>