<?php

namespace app\modules\cms\controllers;

use app\fhhlib\ApiAdm;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Default controller for the `cms` module
 */
class ArtikelController extends Controller
{

    public $layout = '@app/views/layouts/backmain.php';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {

        return $this->render('list_artikel');
    }
    function actionListartikel(){
        $req = \Yii::$app->request->get();
        $length = $req['length'] == NULL ? 10 : $req['length'];
        $start = $req['start'] == NULL ? 0 : $req['start'] ;
        $listperjalanan = ApiAdm::getListArtikel($start,$length);
//        print_r($listperjalanan);exit;
        $listperjalanan = json_decode($listperjalanan,true);
        if($listperjalanan['rc'] != '00' && empty($listperjalanan['data']['list'])){

            $data['draw'] = 1;
            $data['recordsTotal'] = 0;
            $data['recordsFiltered'] = 0;
            $data['start'] = 0;
            $data['length'] = 0;
            $data['data'] = array(
            );
        }else {

            $data['draw'] =  $req['draw'];
            $data['recordsTotal'] = $listperjalanan['data']['recordsTotal'];
            $data['recordsFiltered'] = $listperjalanan['data']['recordsFiltered'];
            foreach ($listperjalanan['data']['list'] as $r) {
                $data['data'][] = array($r['title'],
                    $r['desc_short'],
                    $r['post_date'],
                    $r['name_author'],
                    $r['name_kategori'],
                    '<a href="'.Url::to(['/cms/artikel/editartikel','id'=>$r['id']]).'" class="btn btn-outline-warning"><i class="fa fa-edit"></i></a>'
                );
            }
            $data['start'] = 0;
            $data['length'] = 0;
        }
        echo json_encode($data);
        exit;
    }
    public function actionCreate()
    {
        $target_file = '';
        $filenames = '';
        if (!empty($_FILES['imagefiles']['tmp_name'])) {
            $imgname = $_FILES['imagefiles'];
            $exts = pathinfo($imgname['name'], PATHINFO_EXTENSION);
            $filenames = time() . rand(0, 999) . "." . $exts;
            $target_file = \Yii::getAlias('@webroot') . "/uploads/" . $filenames;
            $check = getimagesize($imgname["tmp_name"]);
            $uploadOk = 0;
            if ($check !== false) {
                $uploadOk = 1;
            }
            try {

                if ($uploadOk == 0) {
                    echo "Sorry, your file was not uploaded.";
                } else {
                    if (!move_uploaded_file($imgname["tmp_name"], $target_file)) {
                        echo "Sorry, there was an error uploading your file.";
                        exit;
                    }
                }
            } catch (\Exception $e) {
                print_r($e->getMessage());
                exit;
            }
        }
        if (!empty($target_file)) {
            $_POST['artikel']['img_link'] = Url::to(['/uploads/' . $filenames], true);
        }
        $data['kategori_artikel'] = json_decode(ApiAdm::getartikelkatogori(), true)['data'];
        $data['detail_artikel'] = [];
        return $this->render('form_artikel',$data);
    }
    public function actionEditartikel()
    {
        $req = \Yii::$app->request->get();
        $data['kategori_artikel'] = json_decode(ApiAdm::getartikelkatogori(), true)['data'];
        $data['detail_artikel'] = json_decode(ApiAdm::getdetailartikel($req['id']), true)['data'];
        return $this->render('form_artikel',$data);
    }
    function actionDocreate(){
        if($_POST){
            $target_file = '';
            $filenames = '';
            if (!empty($_FILES['imagefiles']['tmp_name'])) {
                $imgname = $_FILES['imagefiles'];
                $exts = pathinfo($imgname['name'], PATHINFO_EXTENSION);
                $filenames = time() . rand(0, 999) . "." . $exts;
                $target_file = \Yii::getAlias('@webroot') . "/uploads/" . $filenames;
                $check = getimagesize($imgname["tmp_name"]);
                $uploadOk = 0;
                if ($check !== false) {
                    $uploadOk = 1;
                }
                try {

                    if ($uploadOk == 0) {
                        echo "Sorry, your file was not uploaded.";
                    } else {
                        if (!move_uploaded_file($imgname["tmp_name"], $target_file)) {
                            echo "Sorry, there was an error uploading your file.";
                            exit;
                        }
                    }
                } catch (\Exception $e) {
                    print_r($e->getMessage());
                    exit;
                }
            }
            if (!empty($target_file)) {
                $_POST['artikel']['img_link'] = Url::to(['/uploads/' . $filenames], true);
            }
            if(empty($_POST['id_artikel'])){
                $insertdata = ApiAdm::insertartikel($_POST,'');
            }else{
                $updatedata = ApiAdm::editartikel($_POST,'');
            }
            return $this->redirect(Url::to(['/cms/artikel/index'],true));
        }
        exit;
    }
}
