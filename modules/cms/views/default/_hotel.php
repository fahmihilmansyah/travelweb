<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 09.16
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
$perjalananhotel = !empty($detailperjalanan['data']['perjalanan_hotel']) ? $detailperjalanan['data']['perjalanan_hotel'] : [];

?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-3">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Hotel</span>
                    <select class="form-control reshotel" id="hoteltmb">
                        <?php foreach ($infohotel['data'] as $r): ?>
                            <option value="<?php echo $r['id'] ?>"><?php echo $r['nama_hotel'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-3">
                <br>
                <label id="btnaddhotel" class="btn btn-outline-primary">Tambah Hotel</label>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Nama Hotel</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody id="reshotel">
                        <?php foreach ( $perjalananhotel as $r): ?>
                        <tr>
                            <td><?php echo $r['nama_hotel'] ?><input type="hidden" name="perjalanan_hotel[id][]" value="<?php echo $r['id'] ?>"></td>
                            <td><label class="btn btn-outline-danger btn-sm removetr"><span class="fa fa-trash"></span></label></td>
                            </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    jQuery(document).on('click','#btnaddhotel',function(){
        var hoteltmbid = $("#hoteltmb").val();
        var hoteltmb = $("#hoteltmb option:selected").html();
        $("#reshotel").append('<tr>'+
            '<td>'+hoteltmb+'<input type="hidden" name="perjalanan_hotel[id_hotel][]" value="'+hoteltmbid+'"></td>'+
            '<td><label class="btn btn-outline-danger btn-sm removetr"><span class="fa fa-trash"></span></label></td>'+
            '</tr>');
        $('.reshotel').val('');
    });
</script>