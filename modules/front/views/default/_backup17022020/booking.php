<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 02/12/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 09.51
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
//print_r($detail_perjalanan);exit;
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>
<div class="col-md-8">
    <div class="row">
        <div class="col-md-12" style="padding-bottom: 10px;">
            <h5><?php echo $detail_perjalanan['judul'] ?></h5>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <span class="fa fa-plane fa-4x"></span>
                                </div>
                                <div class="col-md-10">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span style="color: #5a6268">Berangkat:</span><span><?php echo $detail_perjalanan['berangkat_dari'] ?>, <?php echo date("d M Y", strtotime($detail_perjalanan['waktu_berangkat'])); ?></span>
                                        </div>
                                        <div class="col-md-12">
                                            <span style="color: #5a6268">Pulang:</span><span><?php echo $detail_perjalanan['berangkat_dari'] ?>, <?php echo date("d M Y", strtotime($detail_perjalanan['waktu_pulang'])); ?></span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <hr/>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <span style="color: #5a6268">Kelas Hotel:</span>
                                    <?php for ($i = 0; $i < 5; $i++) {
                                        $is_cheked = '';
                                        if ($i < $detail_perjalanan['kelas_hotel']) {
                                            $is_cheked = 'checked-star';
                                        }
                                        ?>
                                        <span class="fa fa-star <?php echo $is_cheked ?>"></span>
                                    <?php } ?>
                                </div>
                                <div class="col-md-4">
                                    <span style="color: #5a6268">Durasi: </span><span><?php echo $detail_perjalanan['durasi_perjalanan'] ?> Hari</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12" style="padding-bottom: 10px;">
            <h5>Data Pemesanan</h5>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3"><span class="fa fa-users fa-4x"></span></div>
                        <div class="col-md-3">
                            <div class="row">
                                <div class="col-md-12" style="color: #5a6268">Nama User : </div>
                                <div class="col-md-12" style="color: #5a6268">Email User :</div>
                                <div class="col-md-12" style="color: #5a6268">No. Telepon : </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12"><?php echo $detail_user['full_name']?></div>
                                <div class="col-md-12"><?php echo $detail_user['email']?></div>
                                <div class="col-md-12"><?php echo $detail_user['no_telp']?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="padding-bottom: 10px;">
            <h5>Data Jamaah</h5>

            <?php echo \yii\helpers\Html::beginForm(\yii\helpers\Url::to(['/front/default/dobooking'],true),'POST') ?>
            <input type="hidden" name="id_user" value="<?php echo $detail_user['id'] ?>">
            <input type="hidden" name="id_perjalanan" value="<?php echo $detail_perjalanan['id'] ?>">
            <div class="row">
                <div class="col-md-12 forcopy" style="padding-bottom: 10px;">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12" style="padding-bottom: 10px;">
                                    <label class="float-right hapusjamaah btn btn-danger btn-sm"><span
                                                class="fa fa-trash"></span></label>
                                </div>
                                <div class="col-md-12" style="padding-bottom: 10px;">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="gelartitle">Title</label>
                                            <select id="gelartitle" required class="form-control" name="user[gelar][]">
                                                <option value="Tuan">Tuan</option>
                                                <option value="Nyonya">Nyonya</option>
                                            </select>
                                        </div>
                                        <div class="col-md-8">
                                            <label for="namalengkap">Nama Lengkap <span style="color: #8c939d">(Sesuai dengan KTP/Paspor/SIM)</span></label>
                                            <input type="text" required id="namalengkap" name="user[nama_lengkap][]"
                                                   placeholder="Nama Lengkap"
                                                   class="isian form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="padding-bottom: 10px;">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label for="notelp">No. Telepon</label>
                                            <input type="text" required id="notelp" class="isian form-control" name="user[no_telp][]"
                                                   placeholder="No. Telepon">

                                        </div>
                                        <div class="col-md-8">
                                            <label for="alamatemail">Alamat Email</label>
                                            <input type="email" required name="user[email][]" id="alamatemail"
                                                   placeholder="Alamat Email"
                                                   class="isian form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12" style="padding-bottom: 10px;">
                                    <label> Tanggal Lahir</label>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select required class="form-control" name="user[tgl_lahir][]">
                                                <option    value="">Tanggal Lahir</option>
                                                <?php for ($i = 1; $i <= 31; $i++) { ?>
                                                    <option value="<?php echo $i ?>"> <?php echo $i ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select required class="form-control" name="user[bln_lahir][]">
                                                <option value="">Bulan</option>
                                                <option value="01">Januari</option>
                                                <option value="02">Februari</option>
                                                <option value="03">Maret</option>
                                                <option value="04">April</option>
                                                <option value="05">Mei</option>
                                                <option value="06">Juni</option>
                                                <option value="07">Juli</option>
                                                <option value="08">Agustus</option>
                                                <option value="09">September</option>
                                                <option value="10">Oktober</option>
                                                <option value="11">November</option>
                                                <option value="12">Desember</option>
                                            </select>
                                        </div>

                                        <div class="col-md-4">
                                            <select required class="form-control" name="user[thn_lahir][]">
                                                <option value="">Tahun Lahir</option>
                                                <?php for ($i = date('Y'); $i >= 1930; $i--) { ?>
                                                    <option value="<?php echo $i ?>"> <?php echo $i ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12" style="padding-bottom: 10px;">
                                    <label><input type="checkbox" value="1" name="user[is_buat_paspor][]"> Daftar Untuk
                                        Membuat Paspor</label><br/>
                                    <label><input type="checkbox" value="1" name="user[is_pernah_umroh][]"> Saya Sudah Pernah
                                        Umroh dalam 3 Tahun Terakhir</label><br/>
                                </div>
                                <div class="col-md-12" style="padding-bottom: 10px;">
                                    <label>Kategori Kamar</label>
                                    <select required class="form-control hargaperjalanan" name="user[id_perjalanan_harga][]">
                                        <?php foreach ($detail_perjalanan['package'] as $r): ?>
                                            <option data-harga="<?php echo (float)$r['harga'] ?>"
                                                    value="<?php echo $r['id'] ?>"><?php echo $r['jenis_paket'] ?> /
                                                Rp. <?php echo number_format($r['harga'], 0, ',', '.') ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="forpaste"></div>
            </div>
            <label style="margin-top: 10px;" id="addjamaah" class="btn btn-primary float-right">Tambah Jamaah</label>
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Rincian Harga</h5>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <b>Paket Umroh : </b><br/>
                            <span style="color: #8c939d"> x<span id="totaljamaah">1</span> Jamaah </span>
                        </div>
                        <div class="col-md-6 text-right">Rp. <span id="hargajamaah"></span></div>
                    </div>
                </div>

                <div class="col-md-12"><hr/></div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <b>Total Harga : </b>
                        </div>
                        <div class="col-md-6 text-right">Rp. <span id="hargatotaljamaah"></span></div>
                    </div>
                </div>
            </div>
            <button style="margin-top: 10px;" class="form-control btn btn-outline-success">Lanjut Ke Pembayaran</button>
        </div>
    </div>
</div>
<?php echo \yii\helpers\Html::endForm(); ?>
<script>
    $(document).on('click', '#addjamaah', function () {
        var jmlcls = $('.forcopy').length + 1;
        var clonvar = $('.forcopy').clone();
        clonvar.attr('data-idx', jmlcls);
        clonvar.addClass('forcopy' + jmlcls);
        clonvar.find('.hapusjamaah').attr('data-idx', jmlcls);
        clonvar.find('.isian ').val('');
        clonvar.appendTo('.forpaste');
        hitungharga();
    });
    $(document).on('click', '.hapusjamaah', function () {
        var idx = $(this).attr('data-idx');
        $(this).parents('.forcopy' + idx).remove();
        hitungharga();
    });
    hitungharga();

    function hitungharga() {
        var hargatotal = 0;
        $('.hargaperjalanan').each(function () {
            var initharga = parseFloat($('option:selected', this).attr('data-harga'));
            hargatotal += initharga;
        });
        $("#totaljamaah").html($('.forcopy').length);
        $("#hargajamaah").html(hargatotal).number( true, 0 );
        $("#hargatotaljamaah").html(hargatotal).number( true, 0 );
        console.log(hargatotal);
    }
</script>