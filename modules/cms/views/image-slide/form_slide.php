<?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\Url;
?>
<?php

$config = [
    'action' =>['image-slide/new'],
    'method' => 'post', 
    'options' => [ 
        'id' => 'updateProfileForm', 
        // 'onsubmit' => 'return false;', 
        'enctype' => 'multipart/form-data', 
    ] 
];

if(!empty($id)){
    $config['enableClientScript'] = false;
    $config['action'] = ['image-slide/save'];
}

$form = ActiveForm::begin($config); 

?>
    <input type="hidden" name="id" value="<?= $id ?? ''; ?>">
    <div class="content-header">
        <div class="container-fluid">
            <h3 class="table-title">
            <?php echo empty($id)?'Create':'Edit'; ?>
            Slide
            </h3>
            <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                <a href="<?php echo \yii\helpers\Url::to(['/cms'],true) ?>">Home</a>
                </li>
                <li class="breadcrumb-item">
                <a href="<?php echo \yii\helpers\Url::to(['/cms/artikel'],true) ?>"
                    >List of Slides</a
                >
                </li>
                <li class="breadcrumb-item active">
                <a href="#"
                    ><?php echo empty($id)?'Create':'Edit'; ?>
                    Create Slide</a
                >
                </li>
            </ol>
            </nav>
        </div>
        </div>
        <div class="card">
        <div class="card-body">
            <div class="my-form">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Image Slide</label>
                            <div class="customUpload">
                                <div class="fill">
                                <?= Html::img((!empty($slideModel->url_link) ? $slideModel->url_link : ''), ['alt'=>'slider', 'id'=>'pictureProfile']);?>
                                </div>
                                <div class="fill-btn mt-2">
                                <label class="btn btn-primary btn-verifikasi">
                                    <i class="fa fa-images"></i>&nbsp;&nbsp;ADD IMAGE
                                    <?php 
                                            echo $form->field($slideModel, 'hiddenAttr')
                                                    ->hiddenInput(['value'=> $slideModel->hiddenAttr ?? ''])
                                                    ->label(false); 
                                            echo $form->field($slideModel, 'picture')
                                                    ->fileInput(['id'=>'pictureProfileFile', 'onchange' =>'loadFile(event,"pictureProfile")', ])
                                                    ->label(false); 
                                    ?>
                                </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Tagline</label>
                                    <?php echo $form->field($slideModel, 'judul')
                                                    ->textInput([
                                                            'placeholder' => 'Example tagline here...', 
                                                            'value' => $slideModel->judul ?? '', 
                                                            'maxlength' => true
                                                        ])->label(false);
                                    ?>
                                </div>    
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <?php echo $form->field($slideModel, 'description')
                                                    ->textArea([
                                                            'placeholder' => 'Example description here...', 
                                                            'value' => $slideModel->description ?? '', 
                                                            'maxlength' => true
                                                        ])->label(false);
                                    ?>
                                </div>    
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Action link</label>
                                    <?php echo $form->field($slideModel, 'action_link')
                                                    ->textInput([
                                                            'placeholder' => 'Example action link here...', 
                                                            'value' => $slideModel->action_link ?? '', 
                                                            'maxlength' => true
                                                        ])->label(false);
                                    ?>
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <a
            href="<?php echo  \yii\helpers\Url::to(['/cms/image-slide/index'])?>"
            class="btn btn-outline-danger"
            ><span class="fa fa-arrow-alt-circle-left"></span> Back</a
            >
            <button type="submit" class="btn btn-outline-info">
            <span class="fa fa-save"></span> Submit
            </button>
        </div>
    </div>
<?php ActiveForm::end(); ?>
