<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 09/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 23.32
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
 ?>

<div class="card">
 <div class="card-body">
  <div class="row">
   <div class="col">
    <div class="form-group mb-3">
     <span id="inputGroup-sizing-sm">Konten</span>
     <textarea name="artikel[desc]" placeholder="silakan isi konten"  class="form-control" id="kontendt">
         <?php echo empty($detail_artikel['desc'])?'':$detail_artikel['desc'] ?>
     </textarea>
    </div>
   </div>
  </div>
 </div>
</div>
<script src="https://cdn.ckeditor.com/ckeditor5/17.0.0/classic/ckeditor.js"></script>
<script>
    ClassicEditor
        .create( document.querySelector( '#kontendt' ) )
        .catch( error => {
            console.error( error );
        } );
</script>