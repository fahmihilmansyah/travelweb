<?php 

namespace app\modules\cms\libs;
use yii\helpers\Url;

class SimpleDataTableInit{
    private $_result = [];

    function __construct() {
        $this->_result['draw'] = 1;
        $this->_result['recordsTotal'] = 0;
        $this->_result['recordsFiltered'] = 0;
        $this->_result['start'] = 0;
        $this->_result['length'] = 0;
        $this->_result['data'] = [];
    }

    public function draw($draw){
        if(!empty($draw)) $this->_result['draw'] = $draw;
    }

    public function recordsTotal($recordTotal){
        if(!empty($recordTotal)) $this->_result['recordsTotal'] = $recordTotal;
    }

    public function recordsFiltered($recordFiltered){
        if(!empty($recordFiltered)) $this->_result['recordsFiltered'] = $recordFiltered;
    }

    public function start($start){
        if(!empty($start)) $this->_result['start'] = $start;
    }

    public function length($length){
        if(!empty($length)) $this->_result['length'] = $length;
    }
    
    public function data($data){
        if(!empty($data) && is_array($data)) $this->_result['data'] = $data;
    }
    
    public function results(){
        return $this->_result;
    }

    public function getAction($data = []){
        $button = '<div class="btn-group">';

        if(array_key_exists("edit", $data)){
            $icon = ($data['edit']['icon'] ?? 'fa-edit');
            $button .= '<a href="'.$data['edit']['url'].'" class="btn btn-warning"><i class="fa '.$icon.'"></i></a>';
        }

        if(array_key_exists("delete", $data)){
            $icon = ($data['delete']['icon'] ?? 'fa-trash');
            $button .= '<a href="'.$data['delete']['url'].'" class="btn btn-danger"><i class="fa '.$icon.'"></i></a>';
        }

        $button .= '</div>';

        return $button;
    }

    public function displayImage($url){
        $urlImage = (empty($url) ? Url::to(['/img/no-image.svg'],true) : $url);
        $image = '<div style="background: #ccc url('.$urlImage.') no-repeat center / cover; height:350px;"></div>';
        return $image;
    }
}