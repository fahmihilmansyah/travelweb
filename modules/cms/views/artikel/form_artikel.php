<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 09/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 23.00
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
 ?>
<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 08.05
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
//echo "<pre>";print_r($detailperjalanan);exit;
$urllink = ['/cms/artikel/docreate'];
if(!empty($detailperjalanan['data']['perjalanan'])){
 $urllink = ['/cms/artikel/doupdate','id'=>$detailperjalanan['data']['perjalanan']['id'] ];
}
?>
<div class="content-header">
  <div class="container-fluid">
  <h3 class="table-title"><?php echo empty($detail_artikel['id'])?'Create':'Edit'; ?> Artikel</h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo \yii\helpers\Url::to(['/cms'],true) ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?php echo \yii\helpers\Url::to(['/cms/artikel'],true) ?>">List of Artikel</a></li>
            <li class="breadcrumb-item active"><a href="#"><?php echo empty($detail_artikel['id'])?'Create':'Edit'; ?> Artikel</a></li>
        </ol>
    </nav>
  </div>
</div>
<?php if(empty($detail_artikel) && !empty($_GET['id'])): ?>
<div class="alert alert-danger" role="alert">
    Artikel tidak ditemukan
</div>
<?php endif; ?>
<?php echo \yii\helpers\Html::beginForm(\yii\helpers\Url::to($urllink,true),'POST',['enctype'=>'multipart/form-data']); ?>
<?php if(!empty($detail_artikel)): ?>
 <input type="hidden" name="id_artikel" value="<?php echo $detail_artikel['id'] ?>">
<?php endif; ?>
<div class="card">
 <div class="card-body">
  <nav>
   <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Display</a>
    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-fasilitas" role="tab" aria-controls="nav-profile" aria-selected="false">Konten</a>
    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-gambar" role="tab" aria-controls="nav-contact" aria-selected="false">Gambar Artikel</a>
    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-maskapai" role="tab" aria-controls="nav-contact" aria-selected="false">Meta Tag</a>
   </div>
  </nav>
  <div class="tab-content" id="nav-tabContent">
   <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
       <?php  echo $this->render('_display',['kategori_artikel'=>$kategori_artikel,'detail_artikel'=>$detail_artikel]); ?>
   </div>
   <div class="tab-pane fade" id="nav-fasilitas" role="tabpanel" aria-labelledby="nav-profile-tab">
       <?php echo $this->render('_konten',['detail_artikel'=>$detail_artikel]); ?>
   </div>
   <div class="tab-pane fade" id="nav-gambar" role="tabpanel" aria-labelledby="nav-contact-tab">
       <?php echo $this->render('_gambar',['detail_artikel'=>$detail_artikel]); ?>
   </div>
   <div class="tab-pane fade" id="nav-maskapai" role="tabpanel" aria-labelledby="nav-contact-tab">
       <?php echo $this->render('_metatag',['detail_artikel'=>$detail_artikel]); ?>
   </div>
  </div>
 </div>
 <div class="card-footer text-right">
  <a href="<?php echo  \yii\helpers\Url::to(['/cms/default/perjalanan'])?>" class=" btn btn-outline-danger"><span class="fa fa-arrow-alt-circle-left"></span> Back</a>
  <button type="submit" class=" btn btn-outline-info"><span class="fa fa-save"></span> Submit</button>
 </div>
</div>
<?php echo \yii\helpers\Html::endForm();?>

<script>

 jQuery(document).on('click', '.removetr', function () {
  $(this).closest('tr').remove();
 });
</script>
