<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 08.52
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">URL Gambar</span>
<!--                    <input type="text" name="artikel[img_link]" id="urlgambar" value="" class="form-control resimg">-->
                    <input type="file" name="imagefiles" class="form-control" accept="image/*">
                </div>
                <div>
                    <img src="<?php echo empty($detail_artikel['img_link'])?:$detail_artikel['img_link'] ?>">

                </div>
            </div>
        </div>
    </div>
</div>
 