<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 01/12/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 21.39
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
//print_r($list['url_img'][0]);exit;

$session = Yii::$app->session;
?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="mb-25 bold judul-donasi-big">
                    <?php echo $list['judul'] ?>
                </div>
                <div id="primary-slider" class="splide">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <?php foreach($list['url_img'] as $r ): ?>
                                <li class="splide__slide">
                                    <img src="<?php echo $r ?>">
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div id="secondary-slider" class="splide mt-15 mb-15">
                    <div class="splide__track">
                        <ul class="splide__list">
                            <?php foreach($list['url_img'] as $r ): ?>
                            <li class="splide__slide">
                                <img src="<?php echo $r ?>">
                            </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="row mt-20">
                    <div class="col-md-4">
                        <table class="table table-sm table-borderless">
                            <tr>
                                <td style="width: 30px;"><i class="la la-calendar"></i></td>
                                <td>
                                    <div class="text-muted text-smaller">Waktu Keberangkatan</div>
                                    <div class="semibold"><?php echo date("d M Y", strtotime($list['waktu_berangkat'])) ?></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <table class="table table-sm table-borderless">
                            <tr>
                                <td style="width: 30px;"><i class="la la-map-marker"></i></td>
                                <td>
                                    <div class="text-muted text-smaller">Berangkat Dari</div>
                                    <div class="semibold"><?php echo $list['berangkat_dari'] ?></div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <table class="table table-sm table-borderless">
                            <tr>
                                <td style="width: 30px;"><i class="la la-clock"></i></td>
                                <td>
                                    <div class="text-muted text-smaller">Durasi Perjalanan</div>
                                    <div class="semibold"><?php echo $list['durasi_perjalanan'] ?> Hari</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <table class="table table-sm table-borderless">
                            <tr>
                                <td style="width: 30px;"><i class="la la-plane"></i></td>
                                <td>
                                    <div class="text-muted text-smaller">Maskapai</div>
                                    <div class="semibold">Saudi Airlines</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-4">
                        <table class="table table-sm table-borderless">
                            <tr>
                                <td style="width: 30px;"><i class="la la-hotel"></i></td>
                                <td>
                                    <div class="text-muted text-smaller">Kelas Hotel</div>
                                    <div class="harga">
                                        <?php for ($i = 0; $i < 5; $i++) {
                                            $is_cheked = '';
                                            if ($i < $list['kelas_hotel']) {
                                                $is_cheked = 'la-star';
                                            }
                                            ?>

                                            <i class="la <?php echo $is_cheked ?>"></i>
                                        <?php } ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <ul class="nav nav-pills mt-15 mb-30 paket-tab" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-fasilitas-tab" data-toggle="pill" href="#pills-deskripsi" role="tab" aria-controls="pills-deskripsi" aria-selected="true">Deskripsi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " id="pills-fasilitas-tab" data-toggle="pill" href="#pills-fasilitas" role="tab" aria-controls="pills-fasilitas" aria-selected="true">Fasilitas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-hotel-tab" data-toggle="pill" href="#pills-hotel" role="tab" aria-controls="pills-hotel" aria-selected="false">Hotel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-maskapai-tab" data-toggle="pill" href="#pills-maskapai" role="tab" aria-controls="pills-maskapai" aria-selected="false">Maskapai</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-rencana-tab" data-toggle="pill" href="#pills-rencana" role="tab" aria-controls="pills-rencana" aria-selected="false">Rencana Perjalanan</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-deskripsi" role="tabpanel" aria-labelledby="pills-deskripsi-tab">
                        <div class="pendana-wrapper mb-20">
                            <div class="pendana-box">
                                <div class="pendana-img title-font" style="background-image:url(https://upload.wikimedia.org/wikipedia/id/9/95/Logo_Institut_Teknologi_Bandung.png);"></div>
                                <div class="pendana-text">
                                    <div class="text-small">Agen</div>
                                    <div class="title-font semibold lh-n"><?php echo $list['nama_agen'] ?></div>
                                    <div><i class="icofont-check verif-icon"></i> <span  class="color-second text-small medium">Terverifikasi</span></div>
                                </div>
                            </div>
                        </div>
                        <?php echo $list['description'] ?>
                    </div>
                    <div class="tab-pane fade " id="pills-fasilitas" role="tabpanel" aria-labelledby="pills-fasilitas-tab">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                $includ = '';
                                foreach ($list['fasilitas'] as $r):
                                    if ($r['jenis'] == 'include') {
                                        $includ .= '<li>' . $r['nama_fasilitas'] . '</li>';
                                    }
                                endforeach; ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <b><h6>Include</h6></b>
                                        <br/>
                                        <ul>
                                            <?php echo $includ ?>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <b><h6>Exclude</h6></b>
                                        <br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-hotel" role="tabpanel" aria-labelledby="pills-hotel-tab">
                        Hotel
                    </div>
                    <div class="tab-pane fade" id="pills-maskapai" role="tabpanel" aria-labelledby="pills-maskapai-tab">
                        Maskapai
                    </div>
                    <div class="tab-pane fade" id="pills-rencana" role="tabpanel" aria-labelledby="pills-rencana-tab">
                        Rencana Perjalanan
                    </div>
                </div>
                <div class="mt-30 mb-30">
                    <div class="mb-5">Beritahukan Paket ini kepada Teman Lewat :</div>
                    <div>
                        <a href="danai.html" class="btn btn-facebook"><span><i class="la la-facebook"></i> Facebook</span></a>
                        <a href="danai.html" class="btn btn-whatsapp"><span><i class="la la-whatsapp"></i> WhatsApp</span></a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="paket-konten">
                    <div class="mb-25 bold judul-donasi-big harga mt-0">
                        Rp. <?php echo number_format($list['package'][0]['harga'], 0, ',', '.') ?>
                    </div>
                    <div><i class="la la-info-circle color-main"></i> <?php echo $list['kuota'] ?> Kuota tersedia</div>
                    <div class="mb-10 text-small text-muted">Amankan kuota dengan uang muka 10jt/jamaah.</div>
                    <hr class="mt-15 mb-15">
                    <?php echo \yii\helpers\Html::beginForm(\yii\helpers\Url::to(['/front/default/booking'],true),'GET',['class'=>'form-daftar-paket'])?>
                    <input type="hidden" name="id" value="<?php echo $id ?>">
                    <div class="form-group">
                            <label class="text-smaller text-muted mb-0">Jamaah</label>
                            <div class="relative-box">
                                <i class="la la-user"></i>
                                <select id='jamaahls' class="custom-select">
                                    <option value="1" selected>1 Jamaah</option>
                                    <option value="2">2 Jamaah</option>
                                    <option value="3">3 Jamaah</option>
                                    <option value="4">4 Jamaah</option>
                                    <option value="5">5 Jamaah</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="text-smaller text-muted mb-0">Tipe Kamar</label>
                            <div class="relative-box">
                                <i class="la la-hotel"></i>
                                <select id='tipekamars' class="custom-select">
                                    <?php foreach ($list['package'] as  $x): ?>
                                        <option value="<?php echo $x['id'] ?>"> <?php echo $x['jenis_paket'] ?> - Rp <?php echo number_format($x['harga'],0,',','.') ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="row mt-25">
                            <div class="col-6">
                                <a href="#" class="btn btn-outline btn-block"><i class="la la-share"></i> Bagikan</a>
                            </div>
                            <div class="col-6">
                                <a href="#" class="btn btn-outline btn-block btn-favorit" onclick="favorit()"><i class="la la-heart favorit"></i> Favorit</a>
                            </div>
                        </div>
                            <button type="submit" class="btn btn-main btn-block mt-20"><span>Lanjutkan Pembayaran</span></button>
                            <a href="<?php echo \yii\helpers\Url::to(['/front/default/register'],true) ?>" class="btn btn-main btn-block mt-20"><span>Daftar Sekarang</span></a>
                    <?php echo \yii\helpers\Html::endForm(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    document.addEventListener( 'DOMContentLoaded', function () {
        var secondarySlider = new Splide( '#secondary-slider', {
            fixedWidth  : 150,
            rewind      : true,
            height      : 95,
            gap         : 10,
            cover       : true,
            isNavigation: true,
            pagination  : false,
            focus       : 'center',
            autoplay    : true,
            interval    : 6000,
            breakpoints : {
                '600': {
                    fixedWidth: 100,
                    height    : 60,
                    gap       : 5,
                }
            },
        } ).mount();

        var primarySlider = new Splide( '#primary-slider', {
            type       : 'fade',
            heightRatio: 0.45,
            pagination : false,
            arrows     : false,
            cover      : true,
        } ); // do not call mount() here.

        primarySlider.sync( secondarySlider ).mount();
    } );

    function favorit() {
        $('.btn-favorit').toggleClass('active');
    }
</script>