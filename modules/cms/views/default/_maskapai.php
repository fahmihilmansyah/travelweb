<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 08.56
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
$perjalananpenerbangan = !empty($detailperjalanan['data']['perjalanan_penerbangan']) ? $detailperjalanan['data']['perjalanan_penerbangan'] : [];

?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-3">
                        <div class="form-group mb-3">
                            <span id="inputGroup-sizing-sm">Pesawat</span>
                            <select class="form-control" id="maskapaipesawat" >
                                <?php foreach ($infopesawat['data'] as $r): ?>
                                    <option value="<?php echo $r['id'] ?>"><?php echo $r['nama_maskapai'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group mb-3">
                            <span id="inputGroup-sizing-sm">Berangkat</span>
                            <input id="maskapaiberangkat" type="text" class="form-control resmaskapai">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group mb-3">
                            <span id="inputGroup-sizing-sm">Tujuan</span>
                            <input id="maskapaitujuan" type="text" class="form-control resmaskapai">
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group mb-3">
                            <span id="inputGroup-sizing-sm">Nama Penerbangan</span>
                            <input id="maskapainamapenerbangan" type="text" class="form-control resmaskapai">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 ">
                        <label class="btn btn-outline-primary float-right" id="bttnaddmaskapai">Add Maskapai</label>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Maskapai</th>
                            <th>Berangkat</th>
                            <th>Tujuan</th>
                            <th>Nama Penerbangan</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody id="datamaskapai">
                        <?php foreach ($perjalananpenerbangan as $r): ?>
                            <tr>
                                <td>
                                    <input type="hidden" name="perjalanan_penerbangan[id][]" value="<?php echo $r['id'] ?>">
                                    <?php echo $r['nama_maskapai'] ?></td>
                                <td><?php echo $r['berangkat'] ?></td>
                                <td><?php echo $r['tujuan'] ?></td>
                                <td><?php echo $r['nama_penerbangan'] ?></td>
                                <td><label class="btn btn-outline-danger btn-sm removetr"><span class="fa fa-trash"></span></label></td>
                                </tr>
                        <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
jQuery(document).on('click','#bttnaddmaskapai',function(){
    var maskapaipesawatid = $("#maskapaipesawat").val();
    var maskapaipesawatname = $("#maskapaipesawat option:selected").html();
    var maskapaiberangkat = $("#maskapaiberangkat").val();
    var maskapaitujuan = $("#maskapaitujuan").val();
    var maskapainamapenerbangan = $("#maskapainamapenerbangan").val();
    console.log()
    $("#datamaskapai").append('<tr>'+
            '<td>'+maskapaipesawatname+'<input type="hidden" name="perjalanan_penerbangan[id_pesawat][]" value="'+maskapaipesawatid+'"></td>'+
            '<td>'+maskapaiberangkat+'<input type="hidden" name="perjalanan_penerbangan[berangkat][]" value="'+maskapaiberangkat+'"></td>'+
            '<td>'+maskapaitujuan+'<input type="hidden" name="perjalanan_penerbangan[tujuan][]" value="'+maskapaitujuan+'"></td>'+
            '<td>'+maskapainamapenerbangan+'<input type="hidden" name="perjalanan_penerbangan[nama_penerbangan][]" value="'+maskapainamapenerbangan+'"></td>'+
            '<td><label class="btn btn-outline-danger btn-sm removetr"><span class="fa fa-trash"></span></label></td>'+
        '</tr>');
    $(".resmaskapai").val('');
});
</script>