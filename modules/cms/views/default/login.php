<?php
    /* @var $this yii\web\View */
    /* @var $form yii\bootstrap\ActiveForm */
    /* @var $model app\models\LoginForm */

    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;

    $this->title = 'Login'; 
?>

<div class="extra-admin-login">
    <div class="extra-admin-card">
        <div class="extra-admin-body">
            <div class="row">
                <div class="col-lg-6 d-none d-sm-none  d-md-none  d-lg-block">
                <div class="text-center">
                    <img
                    src="<?php echo \yii\helpers\Url::to(['/img/img-login.png']) ?>"
                    class="extra-admin-login__hero"
                    alt="Image"
                    />
                </div>
                </div>
                <div class="col-lg-6 col-sm-12 col-md-12 col-xs-12">
                    <div class="extra-admin-form">
                        <div class="text-center mb-15">
                        <?php if (Yii::$app->session->hasFlash('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button
                            aria-hidden="true"
                            data-dismiss="alert"
                            class="close"
                            type="button"
                            >
                            ×
                            </button>
                            <h4><i class="icon fa fa-check"></i>Saved!</h4>
                            <?= Yii::$app->session->getFlash('success') ?>
                        </div>
                        <?php endif; ?>

                        <?php if (Yii::$app->session->hasFlash('error')): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button
                            aria-hidden="true"
                            data-dismiss="alert"
                            class="close"
                            type="button"
                            >
                            ×
                            </button>
                            <h4><i class="icon fa fa-check"></i>Oopps, Sory!</h4>
                            <?= Yii::$app->session->getFlash('error') ?>
                        </div>
                        <?php endif; ?>
                        <div class="h3 colo-second title-font bold lh-1">
                            <img
                            src="<?php echo \yii\helpers\Url::to(['/img/logo.png']) ?>"
                            alt="Raudha Tour"
                            />
                        </div>
                        <div>
                            Selamat Datang, <br/><span style="font-size:11px;">Silahkan login untuk mengakses cms kami.</span>
                        </div>
                        </div>
                        <br/>
                        <?php echo Html::beginForm(\yii\helpers\Url::to(['/cms/default/login'],true),'POST',['class'=>'reg-box'])
                        ?>
                        <label>Email</label>
                        <div class="form-group relative-box">
                        <i class="la la-envelope"></i>
                        <input
                            name="username"
                            type="text"
                            class="form-control"
                            placeholder="admin@example.com"
                        />
                        </div>
                        <label>Password</label>
                        <div class="form-group relative-box">
                        <i class="la la-lock"></i>
                        <input
                            name="password"
                            type="password"
                            class="form-control"
                            placeholder="yoursecretwords"
                        />
                        </div>
                        <div class="mt-15">
                        <button type="submit" class="btn btn-main btn-reg btn-full">
                            <i class="icofont-login"></i> Login
                        </button>
                        </div>
                        <?php echo Html::endForm() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
