<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 08.47
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
$perjalanandetail = !empty($detailperjalanan['data']['perjalanan'])?$detailperjalanan['data']['perjalanan']:[];
 ?>

<div class="card">
 <div class="card-body">
  <div class="row">
   <div class="col-md-12">
    <div class="row">
     <div class="col-6">
      <div class="form-group mb-3">
       <span id="inputGroup-sizing-sm">Judul</span>
       <input type="text" name="perjalanan[judul]" value="<?php echo $perjalanandetail['judul']??"" ?>" class="form-control">
      </div>
     </div>
     <div class="col-6">
      <div class="row">
       <div class="col-4">
        <div class="form-group mb-3">
                                        <span
                                            id="inputGroup-sizing-sm">Tanggal Berangkat</span>
         <input name="perjalanan[waktu_berangkat]" value="<?php echo $perjalanandetail['waktu_berangkat']??'' ?>" type="date" class="form-control">
        </div>
       </div>
       <div class="col-4">
        <div class="form-group mb-3">
         <span id="inputGroup-sizing-sm">Durasi Perjalanan</span>
         <input name="perjalanan[durasi_perjalanan]" value="<?php echo $perjalanandetail['durasi_perjalanan']??'' ?>" type="number" class="form-control">
        </div>
       </div>
       <div class="col-4">
        <div class="form-group mb-3">
         <span id="inputGroup-sizing-sm">Kuota</span>
         <input name="perjalanan[kuota]" type="number" value="<?php echo $perjalanandetail['kuota']??'' ?>" class="form-control">
        </div>
       </div>
      </div>
     </div>
     <div class="col-6">
      <div class="form-group mb-3">
       <span id="inputGroup-sizing-sm">Deskripsi</span>
       <textarea type="text" name="perjalanan[description]"  class="form-control"><?php echo $perjalanandetail['description']??'' ?></textarea>
      </div>
     </div>
     <div class="col-6">
      <div class="row">
       <div class="col-4">
        <div class="form-group mb-3">
         <span id="inputGroup-sizing-sm">Berangkat Dari</span>
         <input name="perjalanan[berangkat_dari]" type="text" value="<?php echo $perjalanandetail['berangkat_dari']??'' ?>" class="form-control">
        </div>
       </div>
       <div class="col-4">
        <div class="form-group mb-3">
         <span id="inputGroup-sizing-sm">Pesawat</span>
            <select class="form-control" name="perjalanan[id_pesawat]">
                <?php
                $idpesawat = $perjalanandetail['id_pesawat']??'';
                foreach ($infopesawat['data'] as $r): ?>
                    <option value="<?php echo $r['id'] ?>" <?php echo $idpesawat == $r['id'] ?'selected':''  ?> > <?php echo $r['nama_maskapai'] ?></option>
                <?php endforeach; ?>
            </select>
        </div>
       </div>
       <div class="col-4">
        <div class="form-group mb-3">
         <span id="inputGroup-sizing-sm">Kelas Hotel</span>
         <input name="perjalanan[kelas_hotel]" value="<?php echo $perjalanandetail['kelas_hotel']??'' ?>" type="number" class="form-control">
        </div>
       </div>
       <div class="col-4">
        <div class="form-group mb-3">
         <span id="inputGroup-sizing-sm">Kategori Perjalanan</span>
            <select class="form-control" name="perjalanan[id_kategori]">
                <?php
                $idkat = $perjalanandetail['id_kategori']??'';
                foreach ($infokategori['data'] as $r):
                    ?>
                <option value="<?php echo $r['id'] ?>" <?php echo $idkat == $r['id']?'selected':''; ?> > <?php echo $r['nama'] ?> </option>
                <?php endforeach; ?>
            </select>
        </div>
       </div></div>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
