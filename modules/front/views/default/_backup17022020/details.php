<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 01/12/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 21.39
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
//print_r($list['url_img'][0]);exit;
?>
<div class="col-md-8">
    <div class="row">
        <div class="col-md-12" style="padding-bottom: 10px;">
            <div class="card">

                <h5 class="card-title" style="padding: 10px"><?php echo $list['judul'] ?></h5>
                <img src="<?php echo $list['url_img'][0] ?>"
                     class="card-img-top" alt="...">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4" >
                            <span style="color: #aaabaf">Waktu Keberangkatan</span>
                            <br/>
                            <?php echo date("d M Y", strtotime($list['waktu_berangkat'])) ?>
                        </div>
                        <div class="col-md-4" >
                            <span style="color: #aaabaf">Berangkat Dari</span>
                            <br/>
                            <?php echo $list['berangkat_dari'] ?>
                        </div>
                        <div class="col-md-4" >
                            <span style="color: #aaabaf">Durasi Perjalanan</span>
                            <br/>
                            <?php echo $list['durasi_perjalanan'] ?> Hari
                        </div>
                        <div class="col-md-4" >
                            <span style="color: #aaabaf">Kelas Hotel</span>
                            <br/>
                            <?php for ($i = 0; $i < 5; $i++) {
                                $is_cheked = '';
                                if ($i < $list['kelas_hotel']) {
                                    $is_cheked = 'checked-star';
                                }
                                ?>
                                <span class="fa fa-star <?php echo $is_cheked ?>"></span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Fasilitas</h5>
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            $includ = '';
                            foreach ($list['fasilitas'] as $r):
                                if ($r['jenis'] == 'include') {
                                    $includ .= '<li>' . $r['nama_fasilitas'] . '</li>';
                                }
                            endforeach; ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <b><h6>Include</h6></b>
                                    <br/>
                                    <ul>
                                        <?php echo $includ ?>
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <b><h6>Exclude</h6></b>
                                    <br/>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="col-md-4">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title text-right" style=" font-size: 24px; color: #00a25d">
                <?php echo \yii\helpers\Html::beginForm(\yii\helpers\Url::to(['/front/default/booking'],true),'POST')?>
                <input type="hidden" name="id" value="<?php echo $id ?>">
                <b>Rp <?php echo number_format($list['package'][0]['harga'], 0, ',', '.') ?></b></h5>
            <label for="jamaahls"> Jamaah</label>
            <select id='jamaahls' class="form-control">
                <?php for ($ix = 1; $ix <= 10; $ix++): ?>
                    <option value="<?php echo $ix ?>"> <?php echo $ix ?> Jamaah</option>
                <?php endfor; ?>
            </select>
            <label for="tipekamars">Tipe Kamar</label>
            <select id='tipekamars' class="form-control">
                <?php foreach ($list['package'] as  $x): ?>
                    <option value="<?php echo $x['id'] ?>"> <?php echo $x['jenis_paket'] ?> / Rp <?php echo number_format($x['harga'],0,',','.') ?></option>
                <?php endforeach; ?>
            </select>
            <br/>
            <button type="submit" class="btn form-control btn-outline-success">Lanjut Pembayaran</button>
            <?php echo \yii\helpers\Html::endForm(); ?>
        </div>
    </div>
</div>