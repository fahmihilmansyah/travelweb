<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login'; ?>
<div class="valign-box">
  <div class="container">
    <div class="card no-margin">
      <div class="card-body pt-30 pr-30 pb-30 pl-30">
        <div class="row">
          <div class="col-md-8 valign-wrapper">
            <div class="valign-box text-center">
              <img
                src="<?php echo \yii\helpers\Url::to(['/img/img-login.png']) ?>"
                class="img-reg img-login"
                alt="Image"
              />
            </div>
          </div>
          <div class="col-md-4 valign-wrapper">
            <div class="valign-box">
              <div class="text-center mb-15">
                <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                  <button
                    aria-hidden="true"
                    data-dismiss="alert"
                    class="close"
                    type="button"
                  >
                    ×
                  </button>
                  <h4><i class="icon fa fa-check"></i>Saved!</h4>
                  <?= Yii::$app->session->getFlash('success') ?>
                </div>
                <?php endif; ?>

                <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-danger alert-dismissable">
                  <button
                    aria-hidden="true"
                    data-dismiss="alert"
                    class="close"
                    type="button"
                  >
                    ×
                  </button>
                  <h4><i class="icon fa fa-check"></i>Saved!</h4>
                  <?= Yii::$app->session->getFlash('error') ?>
                </div>
                <?php endif; ?>
                <div class="h3 colo-second title-font bold lh-1">
                  <img
                    src="<?php echo \yii\helpers\Url::to(['/img/logo.png']) ?>"
                    alt="Raudha Tour"
                  />
                </div>
              </div>
              <?php echo Html::beginForm(\yii\helpers\Url::to(['/front/default/login'],true),'POST',['class'=>'reg-box'])
              ?>
              <div class="form-group relative-box">
                <i class="la la-envelope"></i>
                <input
                  name="username"
                  type="text"
                  class="form-control"
                  placeholder="Email"
                />
              </div>
              <div class="form-group relative-box">
                <i class="la la-lock"></i>
                <input
                  name="password"
                  type="password"
                  class="form-control"
                  placeholder="Password"
                />
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-6">
                    <div class="custom-control custom-checkbox">
                      <input
                        type="checkbox"
                        class="custom-control-input"
                        id="customCheck1"
                      />
                      <label
                        class="custom-control-label text-small"
                        style="padding-top: 2px"
                        for="customCheck1"
                        >Ingat saya</label
                      >
                    </div>
                  </div>
                  <div class="col-6 text-right">
                    <a href="lupa-password.html" class="text-small"
                      >Lupa Password?</a
                    >
                  </div>
                </div>
              </div>
              <div class="text-center mt-15">
                <button type="submit" class="btn btn-main btn-reg">
                  <i class="icofont-login"></i> Login
                </button>
              </div>
              <?php echo Html::endForm() ?>

              <div class="text-center mt-30">
                Belum punya akun?
                <a
                  href="<?php echo \yii\helpers\Url::to(['/front/default/register']) ?>"
                  class="colcor-main"
                  >Daftar</a
                >
              </div>
              <?php if(!empty($kosng)): ?>
              <div class="mt-20">
                <div class="text-atau in-bglight mb-25">
                  <span>Atau</span>
                </div>
                <a href="#" class="btn btn-facebook btn-block"
                  ><i class="icofont-facebook mr-20"></i> Login dengan
                  Facebook</a
                >
                <a href="#" class="btn btn-google btn-block mt-15"
                  ><i class="icofont-google-plus mr-20"></i> Login dengan
                  Google</a
                >
              </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
