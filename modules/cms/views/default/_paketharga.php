<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 08.59
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
$perjalananpaket = !empty($detailperjalanan['data']['perjalanan_harga'])?$detailperjalanan['data']['perjalanan_harga']:[];

 ?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-3">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Nama Paket</span>
                    <input type="text" id="namapaket" class="form-control resharga">
                </div>
            </div>
            <div class="col-3">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Harga Paket</span>
                    <input type="text" id="hargapaket" class="form-control resharga">
                </div>
            </div>
            <div class="col-3">
                <br>
                <label id="btnaddharga" class="btn btn-outline-primary">Tambah Paket</label>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Nama Paket</th>
                            <th>Harga Paket</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody id="resharga">
                        <?php foreach ($perjalananpaket as $r): ?>
                        <tr>
                            <td><?php echo $r['jenis_paket'] ?><input type="hidden" name="perjalanan_harga[id][]" value="<?php echo $r['id'] ?>"></td>
                            <td><?php echo number_format($r['harga']) ?></td>
                            <td><label class="btn btn-outline-danger btn-sm removetr"><span class="fa fa-trash"></span></label></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    jQuery(document).on('click','#btnaddharga',function(){
        var namapaket = $("#namapaket").val();
        var hargapaket = $("#hargapaket").val();
        $("#resharga").append('<tr>'+
            '<td>'+namapaket+'<input type="hidden" name="perjalanan_harga[jenis_paket][]" value="'+namapaket+'"></td>'+
            '<td>'+hargapaket+'<input type="hidden" name="perjalanan_harga[harga][]" value="'+hargapaket+'"></td>'+
            '<td><label class="btn btn-outline-danger btn-sm removetr"><span class="fa fa-trash"></span></label></td>'+
            '</tr>');
        $('.resharga').val("");
    });
</script>