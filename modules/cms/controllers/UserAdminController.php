<?php

namespace app\modules\cms\controllers;

use app\fhhlib\ApiAdm;
use yii\helpers\Url;
use yii\web\Controller;
use app\modules\cms\libs\SimpleDataTableInit;
use app\modules\cms\models\AdminFormModel;
use yii\web\UploadedFile;

/**
 * Default controller for the `cms` module
 */
class UserAdminController extends Controller
{
    public $layout = '@app/views/layouts/backmain.php';

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    function actionLists(){
        $request = \Yii::$app->request->get();
        
        $response = ApiAdm::getListsAdmin($request['start'] ?? 0,$request['length'] ?? 2);
        $response = json_decode($response,true);
       
        $dataTable = new SimpleDataTableInit;

        if(!empty($response['data']['list'])){
            $dataTable->draw($request['draw']);
            $dataTable->recordsTotal($response['data']['recordsTotal']);
            $dataTable->recordsFiltered($response['data']['recordsFiltered']);

            $list = [];
            foreach ($response['data']['list'] as $r) {
                $list[] = array(
                    $r['name'] ?? '-',
                    $r['uname'] ?? '-',
                    $r['email'] ?? '-',
                    $r['created_ad'] ?? '-',
                    $dataTable->getAction([
                        'edit' => [
                            'url' => Url::to(['/cms/user-admin/update','id'=>$r['id']])
                        ],
                        'delete' => [
                            'url' => Url::to(['/cms/user-admin/delete','id'=>$r['id']])
                        ]
                    ])
                );
            }

            $dataTable->data($list);
        }

        return json_encode($dataTable->results());
    }

    public function actionCreate(){
        $data['adminModel'] = new AdminFormModel();

        return $this->render('form',$data);
    }

    public function actionNew(){
        $request = \Yii::$app->request->post()['AdminFormModel'];

        try {
            $data = [
                'name' => $request['name'],
                'uname' => $request['uname'],
                'email' => $request['email'],
                'password' => $request['password']
            ];

            $response = ApiAdm::insertAdmin($data);
            $response = json_decode($response,true);    
        } catch (\Throwable $th) {
            //throw $th;response,true);    
        }
        return $this->redirect(['user-admin/index']);
    }

    public function actionUpdate(){
        $request = \Yii::$app->request->get();
        $response = ApiAdm::getSingleAdmin($request['id']);
        $response = json_decode($response,true);
        
        $model = new AdminFormModel();
        $d = $response['data'];
        if(!empty($d)){
            $model->uname = $d['uname'];
            $model->name = $d['name'];
            $model->email = $d['email'];
        }

        $data['id'] = $request['id'];
        $data['adminModel'] = $model;

        return $this->render('form',$data);
    }

    public function actionSave(){
        $request = \Yii::$app->request->post();

        $response = ApiAdm::getSingleAdmin($request['id']);
        $response = json_decode($response,true)['data'];

        try {
            $data = [
                'id' => $request['id'],
                'uname' => $request['AdminFormModel']['uname'],
                'name' => $request['AdminFormModel']['name'],
                'email' => $request['AdminFormModel']['email'],
                'updated_at' => date('Y-m-d H:i:s')
            ];

            if(!empty($request['AdminFormModel']['password'])){
                $data['password'] = $request['AdminFormModel']['password'];
            }
            
            $response = ApiAdm::updateSingleAdmin($data);
            $response = json_decode($response,true);    
        } catch (\Throwable $th) {
            //throw $th;
        }

        return $this->redirect(['user-admin/index']);
    }

    public function actionDelete(){
        $request = \Yii::$app->request->get();

        try {
            // DELETE DATA
            $response = ApiAdm::deleteSingleAdmin($request['id']);
            $response = json_decode($response,true);
        } catch (\Throwable $th) {
            //throw $th;
        }

        return $this->redirect(['user-admin/index']);
    }
}
