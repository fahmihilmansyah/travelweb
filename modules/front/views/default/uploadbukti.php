<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 09/12/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 18.25
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>
<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <table class="table">
                <tr>
                    <td colspan="2" class="text-center"><b>FORM BUKTI TRANSFER</b></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center">&nbsp;</td>
                </tr>
                <tr>
                    <td>Kode Booking:</td>
                    <td><input type="hidden" name="kode_booking" value=""></td>
                </tr>
                <tr>
                    <td>Bank Tujuan :</td>
                    <td><input type="text" name="bank_tujuan"></td>
                </tr>
                <tr>
                    <td>Bank Pengirim :</td>
                    <td><input type="text" name="bank_pengirim"></td>
                </tr>
                <tr>
                    <td>Nama Pengirim:</td>
                    <td><input type="text" name="nama_pengirim"></td>
                </tr>
                <tr>
                    <td>Tanggal Transfer:</td>
                    <td><input type="text" name="tgl_transfer"></td>
                </tr>
                <tr>
                    <td>Jumlah Transfer:</td>
                    <td><input type="text" name="nominal"></td>
                </tr>
                <tr>
                    <td>Upload Bukti Pembayaran:</td>
                    <td><input type="file" name="uplfile"></td>
                </tr>
            </table>
        </div>
    </div>
</div>
