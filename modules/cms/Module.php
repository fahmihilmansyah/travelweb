<?php

namespace app\modules\cms;

/**
 * cms module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\cms\controllers';

    /**
     * {@inheritdoc}
     */
//    public function behaviors()
//    {
        /*return \yii\helpers\ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'index','create', 'view', 'update', 'delete', 'profile', 'togglestatus'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    return Yii::$app->response->redirect(['/admin/auth/login']);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ]);*/
//    }

    public function beforeAction($action)
    {
        $controller = $action->controller->id;
        $actionid = $action->id;
        $actionctrol = $controller.'/'.$actionid;
        $session = \Yii::$app->session;
        $arrlistlog = ['default/login','default/logout'];
        if(!in_array($actionctrol,$arrlistlog)) {
            if (empty($session['login_admin'])) {
                return \Yii::$app->response->redirect(['/cms/default/login']);
//            Logic::getAccess($action->controller->module->id, $action->controller->id, $action->id);
            }
        }

        \Yii::$app->params['adm_token']=($session['login_admin']['data']['login_token']['token']) ?? null;
        return parent::beforeAction($action);
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
