<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 09/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 23.26
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
$perjalanangambar = !empty($detailperjalanan['data']['perjalanan_image']) ? $detailperjalanan['data']['perjalanan_image'] : [];

?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Title</span>
                    <input type="text" value="<?php echo empty($detail_artikel['title'])?'':$detail_artikel['title'] ?>" name="artikel[title]" class="form-control resimg">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Kategori</span>
                    <select name="artikel[categories]" class="form-control resimg">
                        <?php  foreach ($kategori_artikel as $r):
                            $selected = '';
                            if(!empty($detail_artikel['categories'])){
                                if($detail_artikel['categories'] == $r['id']){
                                    $selected = 'selected';
                                }
                            }
                            ?>
                        <option <?php echo $selected ?> value="<?php echo $r['id'] ?>"><?php echo $r['name_kategori'] ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Konten Singkat</span>
                    <textarea name="artikel[desc_short]" class="form-control resimg"><?php echo empty($detail_artikel['desc_short'])?'':$detail_artikel['desc_short']?></textarea>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Link Terkait</span>
                    <input type="text" name="artikel[link_terkait]" value="<?php echo empty($detail_artikel['link_terkait'])?'':$detail_artikel['link_terkait'] ?>" class="form-control resimg">
                </div>
            </div>
        </div>
    </div>
</div>

