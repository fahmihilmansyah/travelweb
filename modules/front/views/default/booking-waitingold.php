<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 02/12/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 09.51
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
//print_r($detail_perjalanan);exit;
?>
<section>
    <div class="container">
        <div class="text-big mb-15"><span class="badge badge-pill badge-info pt-5 pr-5 pb-5 pl-5"><i class="la la-info-circle"></i> Menunggu Pembayaran</span></div>
        <div class="table-responsive">
            <table class="table table-striped table-borderless">
                <tr>
                    <td>Nama Paket</td>
                    <td class="bold text-right"><?php echo $result['data']['paket']?></td>
                </tr>
                <tr>
                    <td>Kode Booking</td>
                    <td class="bold text-right"><?php echo $result['data']['kode_booking']?></td>
                </tr>
                <tr>
                    <td>Jumlah Jamaah</td>
                    <td class="bold text-right"><?php echo $result['data']['jumlah']?></td>
                </tr>
                <tr>
                    <td>bayar</td>
                    <td class="bold text-right"><?php echo number_format($result['data']['total_bayar'],0,'.',',')?></td>
                </tr>
                <tr>
                    <td>DP</td>
                    <td class="bold text-right"><?php echo number_format(1000000,0,'.',',')?></td>
                </tr>
                <tr>
                    <td>Total Bayar</td>
                    <td class="bold text-right"><?php echo number_format($result['data']['total_bayar'] - 1000000,0,'.',',')?></td>
                </tr>
            </table>
        </div>
        <div class="bold mt-20 mb-15 text-bigger"><?php echo $result['data']['pesan']?></div>
        <div id="resform">
        <?php echo \yii\helpers\Html::beginForm(\yii\helpers\Url::to(['/front/default/do-pembayaran'],true),'POST',['id'=>'formindp']) ?>
        <input type="hidden" name="invoice" value="<?php echo $result['data']['kode_booking']?>">
        <input type="hidden" name="amount" value="1000000">
        <div class="row" id="accordion">
            <div class="col-md-4">
                <div class="bold mb-15">E-Money / QRIS</div>
                <?php foreach ($listbayar['data'] as $r):
                    if($r['type'] == 'EMONEY' || $r['type'] == 'QRIS'):
                    ?>
                <label class="custom-label-radio">
                    <input type="radio" name="metodebayar" value="<?php echo $r['kode_produk'] ?>" id="va-1" class="d-none">
                    <div class="custom-radio-box">
                        <?php if(empty($r['url_img'])): ?>
                        <div class="img-bank" style="background:rgba(0,0,0,.15) url()no-repeat center / contain;"></div>
                        <?php else: ?>
                            <div class="img-bank" style="background:rgba(0,0,0,.15) url(<?php echo $r['url_img'] ?>)no-repeat center / contain;"></div>
                        <?php endif; ?>
                        <span><?php echo $r['namaPembayaran'] ?></span>
                    </div>
                </label>
                <?php endif; endforeach; ?>
            </div>
            <div class="col-md-4">
                <div class="bold mb-15">Transfer Bank (Virtual Account)</div>
                <?php foreach ($listbayar['data'] as $r):
                    if($r['type'] == 'VA'):
                    ?>
                <label class="custom-label-radio">
                    <input type="radio" name="metodebayar" value="<?php echo $r['kode_produk'] ?>" id="va-1" class="d-none">
                    <div class="custom-radio-box">
                        <?php if(empty($r['url_img'])): ?>
                        <div class="img-bank" style="background:rgba(0,0,0,.15) url()no-repeat center / contain;"></div>
                        <?php else: ?>
                            <div class="img-bank" style="background:rgba(0,0,0,.15) url(<?php echo $r['url_img'] ?>)no-repeat center / contain;"></div>
                        <?php endif; ?>
                        <span><?php echo $r['namaPembayaran'] ?></span>
                    </div>
                </label>
                <?php endif; endforeach; ?>
            </div>
            <div class="col-md-4">
                <div class="bold mb-15">Setor Tunai</div>
                <?php foreach ($listbayar['data'] as $r):
                    if($r['type'] == 'SETORTUNAI'):
                    ?>
                <label class="custom-label-radio">
                    <input type="radio" name="metodebayar" value="<?php echo $r['kode_produk'] ?>" id="va-1" class="d-none">
                    <div class="custom-radio-box">
                        <?php if(empty($r['url_img'])): ?>
                        <div class="img-bank" style="background:rgba(0,0,0,.15) url()no-repeat center / contain;"></div>
                        <?php else: ?>
                            <div class="img-bank" style="background:rgba(0,0,0,.15) url(<?php echo $r['url_img'] ?>)no-repeat center / contain;"></div>
                        <?php endif; ?>
                        <span><?php echo $r['namaPembayaran'] ?></span>
                    </div>
                </label>
                <?php endif; endforeach; ?>
            </div>
            <div class="col-md-12">
                <label class="float-right btn btn-success" id="bayardp">Bayar DP <i class="la la-arrow-right"></i></label>
            </div>
        </div>
        <?php echo \yii\helpers\Html::endForm() ?>
        </div>
    </div>
</section>
<script>
    jQuery(document).on('click','#bayardp',function (){
        var form = $('#formindp');
        var fattr = form.attr('action');
        var formdt = new FormData(form[0]);
        $.ajax({
            url : fattr,
            data : formdt,
            type : 'POST',
            processData: false,
            contentType: false,
            beforeSend:function (){
                // $("#resform").empty();
            },
            success : function(data){

                $("#resform").html(data);
            }
        });
        // console.log(formdt);
        // alert('test');
    })

</script>