<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 27/11/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 10.01
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */

namespace app\fhhlib;

class ApiAdm{
    private $URL_API = 'http://localhost:8000/';
//    const URL_API='http://localhost/travel/public/';
//    const URL_API='http://194.31.53.26/ddtravel/public/';
    static function getListPerjalanan($offset=0,$limit=5,$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/getlistperjalanan/'.$offset.'/'.$limit,"GET",[],$token);
        return $result;
    }
    static function getListArtikel($offset=0,$limit=5,$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('front/listartikel/'.$offset.'/'.$limit,"GET",[],$token);
        return $result;
    }
    static function getListImageSlide($offset=0,$limit=5,$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('front/listartikel/'.$offset.'/'.$limit,"GET",[],$token);
        return $result;
    }
    static function getinfopesawat(){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('front/infopesawat',"GET");
        return $result;
    }
    static function getinfokategori(){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('front/infokategoriperjalanan',"GET");
        return $result;
    }
    static function getartikelkatogori(){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('front/listartikelkategori',"GET");
        return $result;
    }
    static function getdetailartikel($id=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('front/artikeldetail/'.$id,"GET");
        return $result;
    }
    static function getdetailperjalanan($id='',$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/getperjalanan/'.$id,"GET",[],$token);
        return $result;
    }
    static function getinfohotel(){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('front/infohotel',"GET");
        return $result;
    }
    static function insertperjalanan($data=array(),$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/insertperjalan',"POST",http_build_query($data),$token);
        return $result;
    }
    static function insertartikel($data=array(),$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/insertartikel',"POST",http_build_query($data),$token);
        return $result;
    }
    static function editartikel($data=array(),$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/editartikel',"POST",http_build_query($data),$token);
        return $result;
    }
    static function updateperjalanan($data=array(),$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/updateperjalan',"POST",http_build_query($data),$token);
        return $result;
    }

    public function _exec_data($slug='',$method="GET",$query_str='',$token=''){
        //
// A very simple PHP example that sends a HTTP POST to a remote site
//
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$this->URL_API.$slug);
        // curl_setopt($ch, CURLOPT_URL,\Yii::$app->params['host'].$slug);
        if(!empty($token)){
            $customHeaders = array(
                'X-TOKEN-LOGIN: '.$token
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $customHeaders);
        }
        if($method != "GET") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $query_str);
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

// In real life you should use something like:
// curl_setopt($ch, CURLOPT_POSTFIELDS,
//          http_build_query(array('postvar1' => 'value1')));

// Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);
        curl_close ($ch);

        return $server_output;
    }

    /*===========================*/
    static function postLoginAdmin($data){
        $mylib = new Mylibs();
        $result = $mylib->_exec_data('auth/login/default',"POST",http_build_query($data));
        return $result;
    }
    /*===========================*/
    public function _exec_data_file($slug,$file,$body,$token){
        $localFile = $file['uplfile']['tmp_name'];
        $uploadfile = Yii::getAlias('@webroot').'/tmpfile/'.$file['uplfile']['name'];
        move_uploaded_file($file['uplfile']['tmp_name'], $uploadfile);
        $ch = curl_init();
        $curlFile = curl_file_create($uploadfile);
        $post['bukti_file'] =  $curlFile ;
        array_push($post,$body);
        $ch = curl_init();
        $customHeaders = array(
            'X-TOKEN-LOGIN: '.$token
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $customHeaders);
        curl_setopt($ch, CURLOPT_URL,$this->URL_API.$slug);
        // curl_setopt($ch, CURLOPT_URL,\Yii::$app->params['host'].$slug);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $result=curl_exec ($ch);
        curl_close ($ch);
        unlink($uploadfile);
    }

    // NEW ENDPOINT SLIDE
    static function getListsAdmin($offset=0,$limit=5,$token=''){
        $mylib = new ApiAdm();
        $endpoint = 'adm/admins/'.$offset.'/'.$limit;
        $result = $mylib->_exec_data($endpoint,"GET",[],$token);
       
        return $result;
    }
    static function insertAdmin($data=array(),$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/admins',"POST",http_build_query($data),$token);
        return $result;
    }
    static function getSingleAdmin($id=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/admins/'.$id,"GET");
        return $result;
    }
    static function updateSingleAdmin($data=array(),$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/admins',"PUT",http_build_query($data),$token);
        return $result;
    }
    static function deleteSingleAdmin($id='',$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/admins/'.$id,"DELETE",[],$token);
        return $result;
    }

    //  // NEW ENDPOINT SLIDE
     static function getListsSlide($offset=0,$limit=5,$token=''){
        $mylib = new ApiAdm();
        $endpoint = 'adm/slides/'.$offset.'/'.$limit;
        $result = $mylib->_exec_data($endpoint,"GET",[],$token);
       
        return $result;
    }
    static function insertSlide($data=array(),$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/slides',"POST",http_build_query($data),$token);
        return $result;
    }
    static function getSingleSlide($id=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/slides/'.$id,"GET");
        return $result;
    }
    static function updateSingleSlide($data=array(),$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/slides',"PUT",http_build_query($data),$token);
        return $result;
    }
    static function deleteSingleSlide($id='',$token=''){
        $mylib = new ApiAdm();
        $result = $mylib->_exec_data('adm/slides/'.$id,"DELETE",[],$token);
        return $result;
    }
}
