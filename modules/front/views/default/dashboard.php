<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 27/11/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 10.17
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>

<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 27/11/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 06.15
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
//echo "<pre>"; //print_r($list);exit; ?>

<?php echo $this->render('@app/modules/front/views/default/_header.php',['slide'=>$slide]);
?>
<section>
  <div class="container">
    <div class="row mb-25 umroh-package">
      <div class="col-md-12">
        <div class="color-main section-title-2nd mt-4 mb-3">
            <h3>Paket Umroh</h3>
            <p>Kami menyediakan layanan dan fasilitas <br/> terbaik untuk anda.</p>
        </div>
      </div>
      <!-- <div class="col-md-3">
        <form>
          <select class="custom-select">
            <option value="0" selected>Segera Berangkat</option>
            <option value="1">Paling Murah</option>
            <option value="2">Paling Mahal</option>
            <option value="3">Durasi Paling Cepat</option>
            <option value="4">Durasi Paling Lama</option>
            <option value="5">Popularitas</option>
          </select>
        </form>
      </div> -->
    </div>
    <div>
    <div class="row">
        <div class="col-lg-6">
            <h5>Rekomendasi dari kami</h5>
        </div>
        <div class="col-lg-6">
            <h5 class="float-right">Lihat lainnya</h5>
        </div>
    </div>
    </div>
    <div class="row">
      <?php foreach ($list['list_item'] as $r): ?>
      <div class="col-lg-4 col-md-6">
        <a
          href="<?php echo \yii\helpers\Url::to(['/front/default/details','id'=>$r['id']],true)  ?>"
          class="card hoverable overflow-hidden"
        >
          <div
            class="paket-img"
            style="background: #ccc url(<?= empty($r[ 'url_img' ][0])?\yii\helpers\Url::to(['/img/no-image.svg'],true):$r['url_img'][0] ?>) no-repeat center / cover;">
            <div class="paket-judul">
              <?php echo $r['judul'] ?>
            </div>
          </div>
          <div class="card-body">
            <table
              class="table table-sm table-borderless no-margin"
            >
              <tbody>
                <tr>
                  <td class="text-muted">
                    <i class="la la-calendar"></i> Jadwal Berangkat
                  </td>
                  <td class="text-right">
                    <?php echo date("d M Y", strtotime($r['waktu_berangkat'])) ?>
                  </td>
                </tr>
                <tr>
                  <td class="text-muted">
                    <i class="la la-clock"></i> Durasi Perjalanan
                  </td>
                  <td class="text-right">
                    <?php echo $r['durasi_perjalanan'] ?>
                    Hari
                  </td>
                </tr>
                <tr>
                  <td class="text-muted">
                    <i class="la la-hotel"></i> Kelas Hotel
                  </td>
                  <td class="text-right">
                    <?php for ($i = 0; $i < 5; $i++) {
                                    $is_cheked = '';
                                    $is_color = '';
                                    if ($i < $r['kelas_hotel']) {
                                        $is_cheked = 'checked-star';
                                        $is_color = 'color:#FCA418;';
                                    }
                                    ?>
                    <i class="la la-star" style="<?php echo $is_color ?>"></i>
                    <?php } ?>
                  </td>
                </tr>
                <tr>
                  <td class="text-muted">
                    <i class="la la-map-marker"></i> Berangkat Dari
                  </td>
                  <td class="text-right">Jakarta</td>
                </tr>
                <tr>
                  <td class="text-muted">
                    <i class="la la-plane"></i> Maskapai
                  </td>
                  <td class="text-right">Saudi Airlines</td>
                </tr>
              </tbody>
            </table>
            <hr/>
            <div class="row mt-3">
              <div class="col-4">
                <div class="text-from">Mulai Dari</div>
              </div>
              <div class="col-8 text-right">
                <div class="text-price">
                  Rp. <?php echo empty($r['package'][0]['harga'])?0:number_format($r['package'][0]['harga'], 0, ',', '.') ?>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
      <?php endforeach; ?>
    </div>
    <!-- <div class="mb-5">
      Menampilkan 1 - 6 dari
      <?php //echo $list['total_record'] ?>
      hasil.
    </div> -->
  </div>
</section>

<section class="bg-grey">
  <div class="container">
    <div class="row mb-25">
        <div class="col-md-12">
            <div class="color-main section-title-2nd mt-4 mb-3">
                <h3>Artikel Kami</h3>
                <p>Informasi terkini seputar Haji & Umrah</p>
            </div>
        </div>
      <!-- <div class="col-md-3 text-right d-none d-md-block">
        <a
          href="<?php //echo \yii\helpers\Url::to(['/front/default/artikel']) ?>"
          class="btn btn-main"
          >Lihat Semua</a
        >
      </div> -->
    </div>
    <div class="row">
      <?php foreach ($listartikel['data']['list'] as $r): ?>
      <div class="col-4">
        <!-- ARTICLE CARD -->
        <div class="card">
          <div
            class="paket-img"    
            style="background: #ccc url(<?= empty($r[ 'img_link' ])?\yii\helpers\Url::to(['/img/no-image.svg'],true):$r['img_link'] ?>) no-repeat center / cover;"
          >
            <div class="paket-judul">
              <?php echo $r['title'] ?>
            </div>
          </div>
          <div class="card-body">
            <div class="mb-15 text-smaller">
              <div>
                <i class="text-muted la la-user mr-5"></i>
                <?php echo $r['name_author'] ?? 'Author' ?>
              </div>
              <div>
                <i class="text-muted la la-calendar mr-5"></i>
                <?php echo $r['post_date'] ?? '<i style="color:#ccc">( not set )</i>'; ?>
              </div>
            </div>
            <div class="article-description">
                <p class="block-with-text">
                  <?php echo $r['desc_short'] ?? 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.' ?>
                </p>
            </div>
            <div class="mt-20">
              <a
                href="<?php echo \yii\helpers\Url::to(['/front/default/artikeldetail','id'=>$r['id']]) ?>"
                class="btn btn-block btn-main"
                >
                <span>Selengkapnya</span>
                </a>
            </div>
          </div>
        </div>
      </div>
      <?php endforeach; ?>
      <br/>
      <div class="col-md-12 mt-30">
        <div class="text-center">
            <a
            href="<?php echo \yii\helpers\Url::to(['/front/default/artikel']) ?>"
            class="btn btn-main"
            >
                <span>Tampilkan Semua</span>
            </a>
        </div>
      </div>
    </div>
  </div>
</section>
