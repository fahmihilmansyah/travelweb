<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 08.52
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-12">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Meta Title</span>
                    <input type="text" value="<?php echo empty($detail_artikel['meta_title'])?'':$detail_artikel['meta_title'] ?>" name="artikel[meta_title]"  class="form-control resimg">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Meta Description</span>
                    <textarea name="artikel[meta_description]"  class="form-control resimg"><?php echo empty($detail_artikel['meta_description'])?'':$detail_artikel['meta_description'] ?></textarea>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Meta Keyword</span>
                    <input value="<?php echo empty($detail_artikel['meta_keyword'])?'':$detail_artikel['meta_keyword'] ?>" type="text" name="artikel[meta_keyword]" id="urlgambar" class="form-control resimg">
                </div>
            </div>
        </div>
    </div>
</div>
 