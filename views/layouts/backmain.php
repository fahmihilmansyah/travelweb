<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 19/02/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 13.39
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>
<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

$session = Yii::$app->session; ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
  <head>
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>CMS - Welcome to Raudha Tour</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/fontawesome-free/css/all.min.css'], true) ?>"
    />
    <!-- Ionicons -->
    <link
      rel="stylesheet"
      href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
    />

    <!-- DataTables -->
    <link
      rel="stylesheet"
      href="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.css'], true) ?>"
    />
    <!-- Tempusdominus Bbootstrap 4 -->
    <link
      rel="stylesheet"
      href="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'], true) ?>"
    />
    <!-- iCheck -->
    <link
      rel="stylesheet"
      href="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css'], true) ?>"
    />
    <!-- JQVMap -->
    <link
      rel="stylesheet"
      href="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/jqvmap/jqvmap.min.css'], true) ?>"
    />
    <!-- Theme style -->
    <link
      rel="stylesheet"
      href="<?php echo \yii\helpers\Url::to(['/adminlte/dist/css/adminlte.min.css'], true) ?>"
    />
    <!-- overlayScrollbars -->
    <link
      rel="stylesheet"
      href="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css'], true) ?>"
    />
    <!-- Daterange picker -->
    <link
      rel="stylesheet"
      href="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/daterangepicker/daterangepicker.css'], true) ?>"
    />
    <!-- summernote -->
    <link
      rel="stylesheet"
      href="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/summernote/summernote-bs4.css'], true) ?>"
    />
    <!-- Google Font: Source Sans Pro -->
    <link
      href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700"
      rel="stylesheet"
    />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link
      rel="stylesheet"
      href="<?php echo \yii\helpers\Url::to(['/css/extra/admin.css'], true) ?>"
    />

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <!-- jQuery -->
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/jquery/jquery.min.js'], true) ?>"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/jquery-ui/jquery-ui.min.js'], true) ?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js'], true) ?>"></script>
    <!-- ChartJS -->
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/chart.js/Chart.min.js'], true) ?>"></script>
    <!-- Sparkline -->
    <!-- <script src="-->
    <?php //echo \yii\helpers\Url::to(['/adminlte/plugins/sparklines/sparkline.js'],true) ?>
    <!--"></script>-->
    <!-- JQVMap -->
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/jqvmap/jquery.vmap.min.js'], true) ?>"></script>
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/jqvmap/maps/jquery.vmap.usa.js'], true) ?>"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/jquery-knob/jquery.knob.min.js'], true) ?>"></script>
    <!-- daterangepicker -->
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/moment/moment.min.js'], true) ?>"></script>
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/daterangepicker/daterangepicker.js'], true) ?>"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js'], true) ?>"></script>
    <!-- Summernote -->
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/summernote/summernote-bs4.min.js'], true) ?>"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js'], true) ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/dist/js/adminlte.js'], true) ?>"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!--    <script src="-->
    <?php //echo \yii\helpers\Url::to(['/adminlte/dist/js/pages/dashboard.js'], true) ?>
    <!--"></script>-->
    <!-- AdminLTE for demo purposes -->
    <!--    <script src="-->
    <?php //echo \yii\helpers\Url::to(['/adminlte/dist/js/demo.js'], true) ?>
    <!--"></script>-->

    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/datatables/jquery.dataTables.js'], true) ?>"></script>
    <script src="<?php echo \yii\helpers\Url::to(['/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js'], true) ?>"></script>

    <?php $this->
    head() ?>
  </head>
  <body id="page-top" class="hold-transition sidebar-mini layout-fixed">
    <?php $this->beginBody() ?>
    <div class="wrapper">
      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"
              ><i class="fas fa-bars"></i
            ></a>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item d-none d-sm-inline-block">
            <a
              href="<?php echo \yii\helpers\Url::to(['/cms/default/logout']) ?>"
              class="nav-link"
              >Logout</a
            >
          </li>
        </ul>
      </nav>
      <!-- /.navbar -->

      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a
          href="index3.html"
          class="brand-link"
          style="overflow: hidden; background: #fff"
        >
          <img
            src="<?php echo \yii\helpers\Url::to(['/img/logo.png']) ?>"
            alt="AdminLTE Logo"
            class="brand-image"
            style="opacity: 0.8"
          />
          <span class="brand-text font-weight-light"> </span>
        </a>

        <div class="main-menu mt-3 pb-3 d-flex">
          <div class="info">
            <a href="#">MAIN MENU</a>
          </div>
        </div>

          <!-- Sidebar Menu -->
          <nav>
            <ul
              class="nav nav-pills nav-sidebar flex-column"
              data-widget="treeview"
              role="menu"
              data-accordion="false"
            >
              <!-- Add icons to the links using the .nav-icon class
                         with font-awesome or any other icon font library -->

              <li class="nav-item has-treeview">
                <a href="#" class="nav-link white">
                  <i class="nav-icon fas fa-user"></i>
                  <p>
                    User Admin
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a
                      href="<?php echo \yii\helpers\Url::to(['/cms/user-admin/index'],true) ?>"
                      class="nav-link"
                    >
                      <i class="far fa-circle child"></i>
                      <p class="white child">List Data</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a
                      href="<?php echo \yii\helpers\Url::to(['/cms/user-admin/create'],true) ?>"
                      class="nav-link"
                    >
                      <i class="far fa-circle child"></i>
                      <p class="white child">Create New</p>
                    </a>
                  </li>
                </ul>
              </li>

              <li class="nav-item has-treeview">
                <a href="#" class="nav-link white">
                  <i class="nav-icon fas fa-images"></i>
                  <p>
                    Slider
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a
                      href="<?php echo \yii\helpers\Url::to(['/cms/image-slide/index'],true) ?>"
                      class="nav-link"
                    >
                      <i class="far fa-circle child"></i>
                      <p class="white child">List Data</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a
                      href="<?php echo \yii\helpers\Url::to(['/cms/image-slide/create'],true) ?>"
                      class="nav-link"
                    >
                      <i class="far fa-circle child"></i>
                      <p class="white child">Create New</p>
                    </a>
                  </li>
                </ul>
              </li>
              
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link white">
                  <i class="nav-icon fas fa-plane"></i>
                  <p>
                    Perjalanan
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a
                      href="<?php echo \yii\helpers\Url::to(['/cms/default/perjalanan'],true) ?>"
                      class="nav-link"
                    >
                      <i class="far fa-circle child"></i>
                      <p class="white child">List Data</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a
                      href="<?php echo \yii\helpers\Url::to(['/cms/default/create'],true) ?>"
                      class="nav-link"
                    >
                      <i class="far fa-circle child"></i>
                      <p class="white child">Create New</p>
                    </a>
                  </li>
                </ul>
              </li>

              <li class="nav-item has-treeview">
                <a href="#" class="nav-link white">
                  <i class="nav-icon fas fa-book-open"></i>
                  <p>
                    Artikel
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a
                      href="<?php echo \yii\helpers\Url::to(['/cms/artikel/index'],true) ?>"
                      class="nav-link"
                    >
                      <i class="far fa-circle child"></i>
                      <p class="white child">List Data</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a
                      href="<?php echo \yii\helpers\Url::to(['/cms/artikel/create'],true) ?>"
                      class="nav-link"
                    >
                      <i class="far fa-circle child"></i>
                      <p class="white child">Create New</p>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
          <div class="container-fluid">
            <?php echo $content ?>
          </div>
          <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <strong
          >Copyright &copy; 2014-2019
          <a href="http://adminlte.io">AdminLTE.io</a>.</strong
        >
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
          <b>Version</b> 3.0.2
        </div>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>

<script>
    var loadFile = function(event, idTarget) {
        console.log(event)
        var output = document.getElementById(idTarget);
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src);
        }
    };
</script>