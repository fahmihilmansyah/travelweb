<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="canonical" href="https://umroh.com">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        a, a:hover {
            text-decoration: none !important;
            color: black;
        }

        .text-ellipses {
            display: block;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .text-sm-grey {
            font-size: 12px;
            color: #a0a0a0;
        }

        footer#footer {
            background: #067141;
            color: #fff;
            padding: 50px 0;

        }

        .wrapper {
            /*height: calc(115vh - 60px);*/
            min-height: 720px;
            background: #f7f7f7;
        }

        .checked-star {
            color: orange;
        }
    </style>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<nav class="navbar navbar-expand-lg navbar-light" style="
    box-shadow: 0px 1px 4px #057141;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <a class="navbar-brand" href="<?php echo \yii\helpers\Url::to(['/']) ?>">DDTravel</a>
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo \yii\helpers\Url::to(['/']) ?>">Home <span class="sr-only">(current)</span></a>
            </li>
            <!--            <li class="nav-item">-->
            <!--                <a class="nav-link" href="#">Link</a>-->
            <!--            </li>-->
        </ul>
        <div class="form-inline my-2 my-lg-0">
            <?php
            $session = Yii::$app->session;
            if (empty($session['login_member'])) {
                ?>
                <a href="<?php echo \yii\helpers\Url::to(['/front/default/login']) ?>" class="btn btn-outline-success">Login</a>
            <?php } else {
                ?>
                <a href="<?php echo \yii\helpers\Url::to(['/front/default/memberorderlist']) ?>"
                   class="btn btn-outline-danger">Order</a>
                <a href="<?php echo \yii\helpers\Url::to(['/front/default/logout']) ?>" class="btn btn-outline-danger">Logout</a>
            <?php } ?>
        </div>
    </div>
</nav>
<div class="wrapper">
    <div style="padding-top: 10px;" class="row">
        <!--<div class="col-md-12">
            <div class="jumbotron">
                <h1>Congratulations!</h1>

                <p class="lead">You have successfully created your Yii-powered application.</p>

                <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
            </div>
        </div>-->
        <div class="col-md-12">
            <div class="container">
                <div class="row">
                    <?= $content ?>
                </div>
            </div>
        </div>
        <div class="col-md-12">&nbsp;</div>
    </div>
</div>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="row">
                    <div class="col-xs-12">
                        <img src="https://www.dompetdhuafa.org/asset/logo/Dompet_Dhuafa_White.png" alt="DD Logo"
                             width="180">
                        <hr>
                    </div>
                </div>
                <div id="footer-body" class="row">
                    <div class="col-sm-4">
                        <h4>Dompet Dhuafa Republika</h4>
                        <p>
                            Philanthropy Building
                            <br><br>
                            Jl. Warung Jati Barat No.14<br>
                            Jakarta Selatan 12540, Indonesia
                            <br><br>
                            Ph : +62 21 7821292<br>
                            Fax : +62 21 7821333
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <h4>Kantor Layanan Ziswaf</h4>
                        <p>
                            Perkantoran Ciputat Indah Permai Blok C 28-29
                            <br><br>
                            Jl. Ir. H. Juanda No. 50 Ciputat - 15419<br>
                            Tangerang Selatan, Banten, Indonesia<br>
                            Phone : +62 21 7416040 (Hunting)<br>
                            Fax : +62 21 7416070<br>
                            Call Center : +62 21 7416050<br>
                            Email : layandonatur@dompetdhuafa.org
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <h4>Social Media</h4>
                        <ul id="social-media-footer" class="list-unstyled">
                            <li><a href="https://www.facebook.com/pages/Dompet-Dhuafa/188412341173718"><i
                                            class="fa fa-facebook fa-fw"></i> Facebook</a></li>
                            <li><a href="https://twitter.com/Dompet_Dhuafa"><i class="fa fa-twitter fa-fw"></i> Twitter</a>
                            </li>
                            <li><a href="https://instagram.com/dompet_dhuafa/"><i class="fa fa-instagram fa-fw"></i>
                                    Instagram</a></li>
                            <li><a href="https://plus.google.com/s/dompet%20dhuafa"><i class="fa fa-google fa-fw"></i>
                                    Google+</a></li>
                            <li><a href="https://www.youtube.com/user/DhuafaDompet?feature=watch"><i
                                            class="fa fa-youtube-play fa-fw"></i> Youtube</a></li>
                            <li><a href="https://id.wikipedia.org/wiki/Dompet_Dhuafa_Republika"><i
                                            class="fa fa-wikipedia-w fa-fw"></i> Wikipedia</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="text-center">
            Copyright © Dompet Dhuafa 2019
        </div>

        <table style="font-size:5px" align="center">
            <tbody>
            <tr>
                <td>
                    <a href="http://www.republika.co.id/berita/dunia-islam/wakaf/16/07/21/oan7vs-dompet-dhuafa-gencarkan-kampanye-wakaf-produktif"
                       target="_blank" style="text-decoration: none;color:grey">Wakaf Produktif</a></td>
                <td>--</td>
                <td><a href="http://tabungwakaf.com" target="_blank" style="text-decoration: none;color:grey">Wakaf
                        Produktif</a></td>
            </tr>
            </tbody>
        </table>
    </div>
</footer>
<script type="text/javascript">
    $(document).ready(function () {
        // executes when HTML-Document is loaded and DOM is ready
        console.log("document is ready");


        $(".cardf").hover(
            function () {
                $(this).addClass('shadow-lg').css('cursor', 'pointer');
            }, function () {
                $(this).removeClass('shadow-lg');
            }
        );

// document ready
    });


</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
