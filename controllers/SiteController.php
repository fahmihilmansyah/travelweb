<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    function actionTest(){
        $this->layout = '@app/views/layouts/fmain.php';;
        return $this->render('test');
    }

    function actionTestcurl(){
        if($_FILES) {
//            print_r($_FILES);
//            exit;
            // This is the entire file that was uploaded to a temp location.
            $localFile = $_FILES['uplfile']['tmp_name'];
            $uploadfile = Yii::getAlias('@webroot').'/tmpfile/'.$_FILES['uplfile']['name'];
            move_uploaded_file($_FILES['uplfile']['tmp_name'], $uploadfile);

// Connecting to website.
            $ch = curl_init();
            $curlFile = curl_file_create($uploadfile);
            $post = array('val1' => 'value','val2' => 'value','bukti_file'=> $curlFile );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,'http://localhost/travel/public/front/testupload');
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            $result=curl_exec ($ch);
            curl_close ($ch);
            unlink($uploadfile);
            print_r($result);exit;
        }
        return $this->render('testupload');
    }
    function actionTest1(){
        $session = Yii::$app->session;
//        print_r($session['login_admin']);
        echo "Asda";exit;
        Yii::$app->params = array('test'=>'dada');
        return $this->render('test1');
    }
}
