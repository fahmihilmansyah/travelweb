<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/10/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 03.57
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>

<section>
    <div class="row">
        <div class="col-3">&nbsp;</div>
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 text-center">
                            <p>Nomor Virtual Account</p>
                            <p><b><?php echo $list ?></b></p>
                        </div>
                    </div>
                </div>
            </div>

            <div id="accordion">
            <?php $i=0; foreach ($guide as $r): ?>
                <div class="card">
                    <div class="card-header" id="headingOne"  data-toggle="collapse" data-target="#collapseOne<?php echo $i ?>" aria-expanded="true" aria-controls="collapseOne">
                        <h5 class="mb-0">
                                <?php echo $r['title'] ?>
                        </h5>
                    </div>
                    <div id="collapseOne<?php echo $i ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            <?php echo $r['desc'] ?>
                        </div>
                    </div>
                </div>
            <?php $i++; endforeach; ?>
            </div>
        </div>
        <div class="col-3">&nbsp;</div>
    </div>
</section>