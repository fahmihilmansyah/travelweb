<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 17/07/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 09.13
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
 ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="title-font judul-donasi-big bold mb-15"><?php echo $list['data']['title'] ?></div>
<!--                <img src="https://images.pexels.com/photos/2989625/pexels-photo-2989625.jpeg" alt="Image">-->
                <div class="mt-15 mr-15 text-smaller">
                    <span>
                        <i class="text-muted la la-user mr-5"></i> <?php echo $list['data']['author'] ?>
                    </span>
                    <span class="ml-15">
                        <i class="text-muted la la-calendar mr-5"></i> <?php echo $list['data']['created_ad'] ?>
                    </span>
                </div>
                <div class="mt-25 mb-40">
                    <?php echo $list['data']['desc'] ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="right-content">
                    <div class="title-font bold color-second mb-10">Artikel Populer</div>
                    <div class="pendana-wrapper">
                        <a href="<?php echo \yii\helpers\Url::to(['/front/default/artikeldetail']) ?>" class="pendana-box blog-link hoverable">
                            <div class="blog-img" style="background-image:url(https://images.unsplash.com/photo-1469571486292-0ba58a3f068b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80);"></div>
                            <div class="blog-text">
                                <div class="title-font semibold">
                                    ITB Berikan Bantuan untuk Masyarakat dan Mahasis...
                                </div>
                                <div class="text-muted text-small mt-5">2 Desember 2019</div>
                            </div>
                        </a>
                        <a href="<?php echo \yii\helpers\Url::to(['/front/default/artikeldetail']) ?>" class="pendana-box blog-link hoverable">
                            <div class="blog-img" style="background-image:url(https://images.unsplash.com/photo-1469571486292-0ba58a3f068b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80);"></div>
                            <div class="blog-text">
                                <div class="title-font semibold">
                                    ITB Berikan Bantuan untuk Masyarakat dan Mahasis...
                                </div>
                                <div class="text-muted text-small mt-5">2 Desember 2019</div>
                            </div>
                        </a>
                        <a href="<?php echo \yii\helpers\Url::to(['/front/default/artikeldetail']) ?>" class="pendana-box blog-link hoverable">
                            <div class="blog-img" style="background-image:url(https://images.unsplash.com/photo-1469571486292-0ba58a3f068b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80);"></div>
                            <div class="blog-text">
                                <div class="title-font semibold">
                                    ITB Berikan Bantuan untuk Masyarakat dan Mahasis...
                                </div>
                                <div class="text-muted text-small mt-5">2 Desember 2019</div>
                            </div>
                        </a>
                        <a href="<?php echo \yii\helpers\Url::to(['/front/default/artikeldetail']) ?>" class="pendana-box blog-link hoverable">
                            <div class="blog-img" style="background-image:url(https://images.unsplash.com/photo-1469571486292-0ba58a3f068b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80);"></div>
                            <div class="blog-text">
                                <div class="title-font semibold">
                                    ITB Berikan Bantuan untuk Masyarakat dan Mahasis...
                                </div>
                                <div class="text-muted text-small mt-5">2 Desember 2019</div>
                            </div>
                        </a>
                        <a href="<?php echo \yii\helpers\Url::to(['/front/default/artikeldetail']) ?>" class="pendana-box blog-link hoverable">
                            <div class="blog-img" style="background-image:url(https://images.unsplash.com/photo-1469571486292-0ba58a3f068b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80);"></div>
                            <div class="blog-text">
                                <div class="title-font semibold">
                                    ITB Berikan Bantuan untuk Masyarakat dan Mahasis...
                                </div>
                                <div class="text-muted text-small mt-5">2 Desember 2019</div>
                            </div>
                        </a>
                    </div>
                    <a href="artikel.html" class="btn btn-main btn-block mt-15"><span>Lihat Artikel Lainnnya</span></a>
                </div>
            </div>
        </div>
    </div>
</section>