<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

$session = Yii::$app->session;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Raudha Tour</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo \yii\helpers\Url::to(['/vendor/bootstrap/css/bootstrap.min.css'],true) ?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo \yii\helpers\Url::to(['/css/scrolling-nav.css'],true) ?>" rel="stylesheet">
    <link href="<?php echo \yii\helpers\Url::to(['/css/rjcustoms.css'],true) ?>" rel="stylesheet">
    <link href="<?php echo \yii\helpers\Url::to(['/vendor/line-awesome/css/line-awesome.min.css'],true) ?>" rel="stylesheet">

    <link href="<?php echo \yii\helpers\Url::to(['/vendor/splide/css/splide.min.css'],true) ?>" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap" rel="stylesheet">
    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo \yii\helpers\Url::to(['/vendor/jquery/jquery.min.js'],true)?>"></script>
    <script src="<?php echo \yii\helpers\Url::to(['/vendor/splide/js/splide.min.js'],true)?>"></script>
    <script src="<?php echo \yii\helpers\Url::to(['/vendor/bootstrap/js/bootstrap.bundle.min.js'],true) ?>"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo \yii\helpers\Url::to(['/vendor/jquery-easing/jquery.easing.min.js'],true)?>"></script>

    <!-- Custom JavaScript for this theme -->
    <script src="<?php echo \yii\helpers\Url::to(['/js/scrolling-nav.js'],true) ?> "></script>

    <link href="<?php echo \yii\helpers\Url::to(['/css/extra/index.css'],true) ?>" rel="stylesheet">

    <?php $this->head() ?>
</head>
<body id="page-top" class="reg-page full-height valign-wrapper">
<?php $this->beginBody() ?>

<?= $content ?>

<script type="text/javascript">
    $(document).ready(function () {
        // executes when HTML-Document is loaded and DOM is ready
        console.log("document is ready");


        $(".cardf").hover(
            function () {
                $(this).addClass('shadow-lg').css('cursor', 'pointer');
            }, function () {
                $(this).removeClass('shadow-lg');
            }
        );

// document ready
    });


</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
