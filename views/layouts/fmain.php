<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

$session = Yii::$app->session; ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
  <head>
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Raudha Tour</title>

    <!-- Bootstrap core CSS -->
    <link
      href="<?php echo \yii\helpers\Url::to(['/vendor/bootstrap/css/bootstrap.min.css'],true) ?>"
      rel="stylesheet"
    />

    <!-- Custom styles for this template -->
    <link
      href="<?php echo \yii\helpers\Url::to(['/css/scrolling-nav.css'],true) ?>"
      rel="stylesheet"
    />
    <link
      href="<?php echo \yii\helpers\Url::to(['/css/rjcustoms.css'],true) ?>"
      rel="stylesheet"
    />
    <link
      href="<?php echo \yii\helpers\Url::to(['/vendor/line-awesome/css/line-awesome.min.css'],true) ?>"
      rel="stylesheet"
    />

    <link
      href="<?php echo \yii\helpers\Url::to(['/vendor/splide/css/splide.min.css'],true) ?>"
      rel="stylesheet"
    />

    <link
      href="https://fonts.googleapis.com/css2?family=Ubuntu:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap"
      rel="stylesheet"
    />

    <link
      href="<?php echo \yii\helpers\Url::to(['/css/extra/front.css'],true) ?>"
      rel="stylesheet"
    />

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo \yii\helpers\Url::to(['/vendor/jquery/jquery.min.js'],true)?>"></script>
    <script src="<?php echo \yii\helpers\Url::to(['/vendor/splide/js/splide.min.js'],true)?>"></script>
    <script src="<?php echo \yii\helpers\Url::to(['/vendor/bootstrap/js/bootstrap.bundle.min.js'],true) ?>"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo \yii\helpers\Url::to(['/vendor/jquery-easing/jquery.easing.min.js'],true)?>"></script>

    <!-- Custom JavaScript for this theme -->
    <script src="<?php echo \yii\helpers\Url::to(['/js/scrolling-nav.js'],true) ?> "></script>
    <?php $this->
    head() ?>
  </head>
  <body id="page-top">
    <?php $this->beginBody() ?>

    <!-- Navigation -->
    <nav
      class="navbar navbar-expand-lg navbar-light bg-white fixed-top"
      id="mainNav"
    >
      <div class="container">
        <a
          class="navbar-brand js-scroll-trigger title-font bold color-main"
          href="<?php echo \yii\helpers\Url::to(['/'],true) ?>"
          ><img
            src="<?php echo \yii\helpers\Url::to(['/img/logo.png']) ?>"
            alt="Raudha Tour"
        /></a>
        <button
          class="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarResponsive"
          aria-controls="navbarResponsive"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="tentang-kami.html">Tentang Kami</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="program.html">Paket Umroh</a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                href="<?php echo \yii\helpers\Url::to(['/front/default/artikel']) ?>"
                >Artikel</a
              >
            </li>

            <?php //if (empty($session['login_member'])) { ?>
            <!-- <li class="nav-item dropdown no-caret">
              <a
                class="nav-link dropdown-toggle"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                href="#"
                >Masuk/Daftar</a
              >
              <div class="dropdown-menu dropdown-menu-right login-dropdown">
                <div class="text-center mb-15">Silahkan Login</div>
                <?php //echo Html::beginForm(\yii\helpers\Url::to(['/front/default/login'],true),'POST',['class'=>'login-form'])
                ?>
                <div class="form-group relative-box">
                  <i class="icofont la la-envelope"></i>
                  <input type="text" class="form-control" placeholder="Email" />
                </div>
                <div class="form-group relative-box">
                  <i class="icofont la la-lock"></i>
                  <input
                    type="text"
                    class="form-control"
                    placeholder="Password"
                  />
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-6">
                      <div class="custom-control custom-checkbox">
                        <input
                          type="checkbox"
                          class="custom-control-input"
                          id="customCheck1"
                        />
                        <label
                          class="custom-control-label text-small"
                          style="padding-top: 2px"
                          for="customCheck1"
                          >Ingat saya</label
                        >
                      </div>
                    </div>
                    <div class="col-6 text-right">
                      <a href="#" class="text-small">Lupa Password?</a>
                    </div>
                  </div>
                </div>
                <button type="submit" class="btn btn-main btn-block btn-login">
                  <i class="icofont-login"></i> Login
                </button>
                <?php //echo Html::endForm() ?>
                <div class="text-center mt-20 text-smaller color-main">
                  Belum punya akun?
                  <a href="register.html" class="text-muted">Daftar</a>
                </div> -->
                <!--                        <div class="mt-30">-->
                <!--                            <div class="text-atau mb-25">-->
                <!--                                <span>Atau</span>-->
                <!--                            </div>-->
                <!--                            <a href="#" class="btn btn-facebook btn-block"><i class="la la-facebook mr-20"></i> Login dengan Facebook</a>-->
                <!--                            <a href="#" class="btn btn-google btn-block mt-15"><i class="la la-google-plus mr-20"></i> Login dengan Google</a>-->
                <!--                        </div>-->
              <!-- </div> -->
            </li>
            <?php //}else{ ?>
            <!-- <li class="nav-item">
              <a
                class="nav-link"
                href="<?php //echo \yii\helpers\Url::to(['front/default/logout'],true)?>"
                >Logout</a
              >
            </li> -->
            <?php //}?>
          </ul>
        </div>
      </div>
    </nav>
    <?= $content ?>
    <div class="bg-main footer-info">
      <div class="container">
        <div class="row">
          <div class="col-md-5 footer-info-box">
            <div class="h3 title-font bold">
              <img
                src="<?php echo \yii\helpers\Url::to(['/img/logo-white.png']) ?>"
                class="logo-footer"
                alt="Raudha Tour"
              />
            </div>
            <div class="mb-20">
              PT. Raudha Rahma Abadi Adalah penyelenggara ibadah haji khusus,
              umrah dan wisata halal sejak tahun 1997. Merupakan anak usaha
              sosial dibawah Yayasan Dompet Dhuafa Republika.
              <br />
              <br />
              Izin Kementerian Agama Republik Indonesia
              <br />
              Haji/ PIHK: 508/ 2017
              <br />
              Umrah/ PPIU: No.903/ 2017
            </div>
            <div class="mb-20">
              <a
                href="#"
                class="sos-link"
                title="Berteman dengan Kami di Facebook"
                ><i class="la la-facebook-square"></i
              ></a>
              <a href="#" class="sos-link" title="Ikuti Kami di Twitter"
                ><i class="la la-twitter-square"></i
              ></a>
              <a href="#" class="sos-link" title="Ikuti Kami di Instagram"
                ><i class="la la-instagram"></i
              ></a>
            </div>
          </div>
          <div class="col-md-3 footer-info-box">
            <div class="h5 mb-25 title-font medium">Pelajari Lebih Lanjut</div>
            <ul class="inline">
              <li><a href="#">FAQ</a></li>
              <li><a href="#">Syarat dan Ketentuan</a></li>
              <li><a href="#">Kebijakan Privasi</a></li>
              <li><a href="#">Tentang Kami</a></li>
            </ul>
          </div>
          <div class="col-md-4 footer-info-box">
            <div class="h5 mb-25 title-font medium">Kontak</div>
            <div class="semibold title-font mb-5">Raudha Tour</div>
            <table class="table table-borderless table-sm no-padding">
              <tbody>
                <tr>
                  <!-- <td style="width: 30px"><i class="la la-building"></i></td> -->
                  <td>
                    Perkantoran Ciputat Indah Permai (Blok C 18) <br />
                    Jl. Ir. H. Juanda No. 50 Ciputat, Tangerang Selatan, 15419.
                    Banten, Indonesia
                  </td>
                </tr>
                <tr>
                  <!-- <td><i class="la la-phone"></i></td> -->
                  <td>021 7418624</td>
                </tr>
                <tr>
                  <!-- <td><i class="la la-envelope"></i></td> -->
                  <td>mail@raudhatour.com</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Footer -->
    <footer class="bg-second">
      <div class="container">
        <p class="m-0 text-center regular">
          Copyright &copy; 2020 - Raudha Tour
        </p>
      </div>
      <!-- /.container -->
    </footer>
    <script type="text/javascript">
      $(document).ready(function () {
        // executes when HTML-Document is loaded and DOM is ready
        console.log('document is ready');

        $('.cardf').hover(
          function () {
            $(this).addClass('shadow-lg').css('cursor', 'pointer');
          },
          function () {
            $(this).removeClass('shadow-lg');
          }
        );

        // document ready
      });
    </script>
    <?php $this->endBody() ?>
  </body>
</html>
<?php $this->endPage() ?>
