<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 02/12/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 09.51
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
//print_r($detail_perjalanan);exit;
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/df-number-format/2.1.6/jquery.number.min.js"></script>

<section style="margin-top: 35px;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 ">
                <div class="mb-25 bold judul-donasi-big"><?php echo $detail_perjalanan['judul'] ?></div>
                <div class="row">
                    <div class="col-md-2 d-none d-sm-flex">
                        <span class="la la-plane la-5x text-muted"></span>
                    </div>
                    <div class="col-md-10">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <div>Berangkat</div>
                                    <div class="bold"><?php echo $detail_perjalanan['berangkat_dari'] ?>, <?php echo date("d M Y", strtotime($detail_perjalanan['waktu_berangkat'])); ?></div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <div>Pulang</div>
                                    <div class="bold"><?php echo $detail_perjalanan['berangkat_dari'] ?>, <?php echo date("d M Y", strtotime($detail_perjalanan['waktu_pulang'])); ?></div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <div>Kelas Hotel</div>
                                    <div class="bold">
                                        <?php for ($i = 0; $i < 5; $i++) {
                                            $is_cheked = '';
                                            if ($i < $detail_perjalanan['kelas_hotel']) {
                                                $is_cheked = 'checked-star';
                                            }
                                            ?>
                                            <span class="fa fa-star <?php echo $is_cheked ?>"></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <div>Duraasi</div>
                                    <div class="bold"><?php echo $detail_perjalanan['durasi_perjalanan'] ?> Hari</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo \yii\helpers\Html::beginForm(\yii\helpers\Url::to(['/front/default/dobooking'], true), 'POST',['id'=>'formbookingdo']) ?>
                <div class="text-bigger bold mt-10 mb-15">Data Pemesanan</div>
                <div class="row">
                    <div class="col-md-2 d-none d-sm-flex">
                        <span class="la la-user-circle la-5x text-muted"></span>
                    </div>
                    <div class="col-md-10">
                        <?php if(!empty($detail_user)): ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nama Lengkap</label>
                                        <?php echo $detail_user['full_name'] ?? '';?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <?php echo $detail_user['email'] ?? ''; ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>No. Tlp</label>
                                        <?php echo $detail_user['no_telp'] ?? ''; ?>
                                    </div>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Nama Lengkap</label>
                                        <input type="text" name="namapemesan" class="form-control" placeholder="Nama Pemesan">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="emailpemesan" class="form-control" placeholder="Email Pemesan">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>No. Tlp</label>
                                        <input type="text" name="telppemesan" class="form-control" placeholder="No. Telp Pemesan">
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="text-bigger bold mt-30">Data Jamaah</div>
                <input type="hidden" name="id_user" value="<?php echo $detail_user['id'] ?? ''; ?>">
                <input type="hidden" name="id_perjalanan" value="<?php echo $detail_perjalanan['id'] ?>">
                <div class="forcopy">
                    <fieldset class="mt-15">
                        <div class="text-right">
                            <label class="hapusjamaah btn btn-danger btn-sm"><span class="fa fa-trash la la-trash"></span></label>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="gelartitle">Title</label>
                                    <select id="gelartitle" required class="custom-select"
                                            name="user[gelar][]">
                                        <option value="Tuan">Tuan</option>
                                        <option value="Nyonya">Nyonya</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="namalengkap">Nama Lengkap <span class="text-muted">(Sesuai dengan KTP/Paspor/SIM)</span></label>
                                    <input type="text" required id="namalengkap" name="user[nama_lengkap][]" placeholder="Nama Lengkap" class="isian form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="notelp">No. Telepon</label>
                                    <input type="text" required id="notelp" class="isian form-control" name="user[no_telp][]" placeholder="No. Telepon">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for="alamatemail">Alamat Email</label>
                                    <input type="email" required name="user[email][]" id="alamatemail" placeholder="Alamat Email" class="isian form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Lahir</label>
                            <div class="row">
                                <div class="col-md-4">
                                    <select required class="custom-select" name="user[tgl_lahir][]">
                                        <option value="">Tanggal Lahir</option>
                                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                                            <option value="<?php echo $i ?>"> <?php echo $i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select required class="custom-select" name="user[bln_lahir][]">
                                        <option value="">Bulan</option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select required class="custom-select" name="user[thn_lahir][]">
                                        <option value="">Tahun Lahir</option>
                                        <?php for ($i = date('Y'); $i >= 1930; $i--) { ?>
                                            <option value="<?php echo $i ?>"> <?php echo $i ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Kategori Kamar</label>
                            <select required class="custom-select hargaperjalanan"
                                    name="user[id_perjalanan_harga][]">
                                <?php foreach ($detail_perjalanan['package'] as $r): ?>
                                    <option data-harga="<?php echo (float)$r['harga'] ?>"
                                            value="<?php echo $r['id'] ?>"><?php echo $r['jenis_paket'] ?>
                                        /
                                        Rp. <?php echo number_format($r['harga'], 0, ',', '.') ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="user[is_buat_paspor][]" id="user[is_buat_paspor][]">
                                <label class="custom-control-label" for="user[is_buat_paspor][]">Daftar untuk membuat paspor</label>
                            </div>
                        </div>
                        <div>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" value="1" name="user[is_pernah_umroh][]" id="user[is_pernah_umroh][]">
                                <label class="custom-control-label" for="user[is_pernah_umroh][]">Saya Sudah Pernah Umroh dalam 3 Tahun Terakhir</label>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="forpaste"></div>
                <label id="addjamaah" class="btn btn-main mt-15">Tambah Jamaah</label>
            </div>
            <div class="col-md-4">
                <div class="card bg-second card-rincian">
                    <div class="card-body">
                        <div class="card-title">Rincian Harga</div>
                        <table class="table table-borderless no-padding">
                            <tbody>
                                <tr>
                                    <td>
                                        Paket Umroh :<br>
                                        <span class="text-muted"> x<span id="totaljamaah">1</span> Jamaah </span>
                                    </td>
                                    <td class="text-right bold">Rp. <span id="hargajamaah"></span></td>
                                </tr>
                            </tbody>
                        </table>
                        <hr>
                        <table class="table table-borderless no-padding">
                            <tbody>
                                <tr>
                                    <td class="bold">Total Harga</td>
                                    <td class="text-right bold">Rp. <span id="hargatotaljamaah"></span></td>
                                </tr>
                            </tbody>
                        </table>
                        <button class="btn btn-main mt-15 btn-block">Lanjut Ke Pembayaran</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php echo \yii\helpers\Html::endForm(); ?>
<script>
    $(document).on('click', '#addjamaah', function () {
        var jmlcls = $('.forcopy').length + 1;
        var clonvar = $('.forcopy').clone();
        clonvar.attr('data-idx', jmlcls);
        clonvar.addClass('forcopy' + jmlcls);
        clonvar.find('.hapusjamaah').attr('data-idx', jmlcls);
        clonvar.find('.isian ').val('');
        clonvar.appendTo('.forpaste');
        hitungharga();
    });
    $(document).on('click', '.hapusjamaah', function () {
        var idx = $(this).attr('data-idx');
        $(this).parents('.forcopy' + idx).remove();
        hitungharga();
    });
    hitungharga();

    function hitungharga() {
        var hargatotal = 0;
        $('.hargaperjalanan').each(function () {
            var initharga = parseFloat($('option:selected', this).attr('data-harga'));
            hargatotal += initharga;
        });
        $("#totaljamaah").html($('.forcopy').length);
        $("#hargajamaah").html(hargatotal).number(true, 0);
        $("#hargatotaljamaah").html(hargatotal).number(true, 0);
        console.log(hargatotal);
    }
    $("#formbookingdo").submit(function (event){
        var aksi = $("#formbookingdo").attr('action');
        var dataisi = $("#formbookingdo").serialize();
        console.log(aksi);
        $.ajax({
            url:aksi,
            method:'POST',
            data:dataisi,
            dataType:'JSON',
            success:function (msg){
                window.location = msg.link;
                console.log(msg);
            }
        });
        event.preventDefault();
    });
</script>