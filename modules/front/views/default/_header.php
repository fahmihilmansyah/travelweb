<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 17/02/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 13.12
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>
<header>
    <div id="main-slider" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php $i = 0;
            foreach ($slide as $r): ?>
                <li data-target="#main-slider" data-slide-to="<?php echo $i ?>"
                    class="<?php echo $i == 0 ? 'active' : '' ?>"></li>
                <?php $i++; endforeach; ?>
        </ol>
        <div class="carousel-inner">
            <?php $i = 0;
            foreach ($slide['list'] as $r): ?>
                <div class="carousel-item <?php echo $i == 0 ?  'active' : '' ?>" data-interval="9000">
                    <div class="slider-img"
                        style="background:#999999 url(<?php echo $r['url_link'] ?>)no-repeat center / cover;"></div>
                    <div class="carousel-caption">
                        <?php echo empty($r['judul']) ? '' : '<h4>' . $r['judul'] . '</h4>' ?>
                        <?php echo empty($r['description']) ? '' : '<p>' . $r['description'] . '</p>' ?>
                        <?php echo empty($r['action_link']) ? '' : '<a href="'.$r['action_link'].'" class="btn btn-main mb-20">Click Me</a>' ?>

                    </div>
                </div>
            <?php $i++; endforeach; ?>
        </div>
        <a class="carousel-control-prev" href="#main-slider" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#main-slider" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- <div class="search-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 mx-auto search-umroh shadow">
                    <div class="row">
                        <div class="col-lg-12">
                            <h5>Jadwal Keberangkatan</h5>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">@</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">@</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <h5>Keberangakatan dari</h5>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">@</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <h5>Maskapai</h5>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">@</span>
                                </div>
                                <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12" style="text-align:center">
                            <button class="btn btn-primary mx-auto">Cari</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
</header>
