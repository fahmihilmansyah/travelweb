<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 08.57
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
$perjalananrencana = !empty($detailperjalanan['data']['perjalanan_rincian'])?$detailperjalanan['data']['perjalanan_rincian']:[];

 ?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-3">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Title</span>
                    <input type="text" class="form-control resrencana" id="titlerencana">
                </div>
            </div>
            <div class="col-3">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Description</span>
                    <textarea class="form-control resrencana" id="descrencana"></textarea>
                </div>
            </div>
            <div class="col-3">
                <br>
                <label id="btnaddrencana" class="btn btn-outline-primary">Tambah Hotel</label>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Description</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody id="resrencana">
                        <?php foreach ($perjalananrencana as $r):?>
                            <tr>
                                <td><?php echo $r['title'] ?><input type="hidden" name="perjalanan_rincian[id][]" value="<?php echo $r['id'] ?>"></td>
                                <td><?php echo $r['description'] ?></td>
                                <td><label class="btn btn-outline-danger btn-sm removetr"><span class="fa fa-trash"></span></label></td>
                                </tr>'
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    jQuery(document).on('click','#btnaddrencana',function(){
        var titlerencana = $("#titlerencana").val();
        var descrencana = $("#descrencana").val();
        $("#resrencana").append('<tr>'+
            '<td>'+titlerencana+'<input type="hidden" name="perjalanan_rincian[title][]" value="'+titlerencana+'"></td>'+
            '<td>'+descrencana+'<input type="hidden" name="perjalanan_rincian[description][]" value="'+descrencana+'"></td>'+
            '<td><label class="btn btn-outline-danger btn-sm removetr"><span class="fa fa-trash"></span></label></td>'+
            '</tr>');
        $('.resrencana').val('');
    });
</script>
