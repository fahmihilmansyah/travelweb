<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/10/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 03.57
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>

<section>
    <div class="row">
        <div class="col-4">&nbsp;</div>
        <div class="col-4">
            <div class="card">
                <div class="card-body">
                    <img src="<?php echo $list['url_img'] ?>" alt="..." class="img-thumbnail">
                    <p>Silakan scan QRIS dengan menggunakan ewallet anda atau
                        <a href="<?php echo $list['url_img'] ?>" download> download </a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-4">&nbsp;</div>
    </div>
</section>