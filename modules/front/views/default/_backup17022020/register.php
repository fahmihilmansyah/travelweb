<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Register</h5>
                    <?php echo Html::beginForm(\yii\helpers\Url::to(['/front/default/register'],true),'POST') ?>
                    <div class="form-label-group">
                            <label for="inputUsername">Username</label>
                            <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username" required>
                        </div>
                        <div class="form-label-group">
                            <label for="inputPassword">Password</label>
                            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
                        </div>
                        <div class="form-label-group">
                            <label for="inputrePassword">Re-Password</label>
                            <input type="password" name="repassword" id="inputrePassword" class="form-control" placeholder="Re-Password" required>
                        </div>
                        <div class="form-label-group">
                            <label for="inputFullname">Full Name</label>
                            <input type="text" name="fullname" id="inputFullname" class="form-control" placeholder="Full Name" required>
                        </div>
                        <div class="form-label-group">
                            <label for="inputNotlp">No Telp</label>
                            <input type="text" name="notlp" id="inputNotlp" class="form-control" placeholder="No Telp" required>
                        </div>
                        <div class="form-label-group">
                            <label for="inputEmail">Email address</label>
                            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                        </div>
                        <br/>
                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Register</button>
                    <?php echo Html::endForm() ?>
                </div>
            </div>
        </div>