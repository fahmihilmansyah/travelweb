<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 17/07/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 09.13
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
 ?>
<section>
    <div class="container">
        <div class="mb-25">
            <div class="h3 bold title-font lh-1 color-second">Artikel</div>
            <div class="text-muted mb-10">Berita terkini seputar Haji & Umrah.</div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="card overflow-hidden">
                    <div class="bg-donasi" style="background-image:url(https://images.pexels.com/photos/2219024/pexels-photo-2219024.jpeg);"></div>
                    <div class="card-body">
                        <div class="mb-25 font-title bold judul-donasi">
                            ITB Berikan Bantuan untuk Masyarakat dan Mahasiswa Korban Bencana Alam Palu
                        </div>
                        <div class="mb-15 text-smaller">
                            <div>
                                <i class="text-muted la la-user mr-5"></i> Adi Permana
                            </div>
                            <div>
                                <i class="text-muted la la-calendar mr-5"></i> Rabu, 3 Oktober 2019, 11:15
                            </div>
                        </div>
                        <div>
                            BANDUNG, itb.ac.id -- Seluruh keluarga besar Institut Teknologi Bandung menyampaikan rasa prihatin dan belasungkawa yang sedalam-dalamnya...
                        </div>
                        <div class="mt-20">
                            <a href="<?php echo \yii\helpers\Url::to(['/front/default/artikeldetail']) ?>" class="btn btn-block btn-main"><span>Selengkapnya</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card overflow-hidden">
                    <div class="bg-donasi" style="background-image:url(https://images.pexels.com/photos/2219024/pexels-photo-2219024.jpeg);"></div>
                    <div class="card-body">
                        <div class="mb-25 font-title bold judul-donasi">
                            ITB Berikan Bantuan untuk Masyarakat dan Mahasiswa Korban Bencana Alam Palu
                        </div>
                        <div class="mb-15 text-smaller">
                            <div>
                                <i class="text-muted la la-user mr-5"></i> Adi Permana
                            </div>
                            <div>
                                <i class="text-muted la la-calendar mr-5"></i> Rabu, 3 Oktober 2019, 11:15
                            </div>
                        </div>
                        <div>
                            BANDUNG, itb.ac.id -- Seluruh keluarga besar Institut Teknologi Bandung menyampaikan rasa prihatin dan belasungkawa yang sedalam-dalamnya...
                        </div>
                        <div class="mt-20">
                            <a href="<?php echo \yii\helpers\Url::to(['/front/default/artikeldetail']) ?>" class="btn btn-block btn-main"><span>Selengkapnya</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card overflow-hidden">
                    <div class="bg-donasi" style="background-image:url(https://images.pexels.com/photos/2219024/pexels-photo-2219024.jpeg);"></div>
                    <div class="card-body">
                        <div class="mb-25 font-title bold judul-donasi">
                            ITB Berikan Bantuan untuk Masyarakat dan Mahasiswa Korban Bencana Alam Palu
                        </div>
                        <div class="mb-15 text-smaller">
                            <div>
                                <i class="text-muted la la-user mr-5"></i> Adi Permana
                            </div>
                            <div>
                                <i class="text-muted la la-calendar mr-5"></i> Rabu, 3 Oktober 2019, 11:15
                            </div>
                        </div>
                        <div>
                            BANDUNG, itb.ac.id -- Seluruh keluarga besar Institut Teknologi Bandung menyampaikan rasa prihatin dan belasungkawa yang sedalam-dalamnya...
                        </div>
                        <div class="mt-20">
                            <a href="<?php echo \yii\helpers\Url::to(['/front/default/artikeldetail']) ?>" class="btn btn-block btn-main"><span>Selengkapnya</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card overflow-hidden">
                    <div class="bg-donasi" style="background-image:url(https://images.pexels.com/photos/2219024/pexels-photo-2219024.jpeg);"></div>
                    <div class="card-body">
                        <div class="mb-25 font-title bold judul-donasi">
                            ITB Berikan Bantuan untuk Masyarakat dan Mahasiswa Korban Bencana Alam Palu
                        </div>
                        <div class="mb-15 text-smaller">
                            <div>
                                <i class="text-muted la la-user mr-5"></i> Adi Permana
                            </div>
                            <div>
                                <i class="text-muted la la-calendar mr-5"></i> Rabu, 3 Oktober 2019, 11:15
                            </div>
                        </div>
                        <div>
                            BANDUNG, itb.ac.id -- Seluruh keluarga besar Institut Teknologi Bandung menyampaikan rasa prihatin dan belasungkawa yang sedalam-dalamnya...
                        </div>
                        <div class="mt-20">
                            <a href="<?php echo \yii\helpers\Url::to(['/front/default/artikeldetail']) ?>" class="btn btn-block btn-main"><span>Selengkapnya</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card overflow-hidden">
                    <div class="bg-donasi" style="background-image:url(https://images.pexels.com/photos/2219024/pexels-photo-2219024.jpeg);"></div>
                    <div class="card-body">
                        <div class="mb-25 font-title bold judul-donasi">
                            ITB Berikan Bantuan untuk Masyarakat dan Mahasiswa Korban Bencana Alam Palu
                        </div>
                        <div class="mb-15 text-smaller">
                            <div>
                                <i class="text-muted la la-user mr-5"></i> Adi Permana
                            </div>
                            <div>
                                <i class="text-muted la la-calendar mr-5"></i> Rabu, 3 Oktober 2019, 11:15
                            </div>
                        </div>
                        <div>
                            BANDUNG, itb.ac.id -- Seluruh keluarga besar Institut Teknologi Bandung menyampaikan rasa prihatin dan belasungkawa yang sedalam-dalamnya...
                        </div>
                        <div class="mt-20">
                            <a href="<?php echo \yii\helpers\Url::to(['/front/default/artikeldetail']) ?>" class="btn btn-block btn-main"><span>Selengkapnya</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card overflow-hidden">
                    <div class="bg-donasi" style="background-image:url(https://images.pexels.com/photos/2219024/pexels-photo-2219024.jpeg);"></div>
                    <div class="card-body">
                        <div class="mb-25 font-title bold judul-donasi">
                            ITB Berikan Bantuan untuk Masyarakat dan Mahasiswa Korban Bencana Alam Palu
                        </div>
                        <div class="mb-15 text-smaller">
                            <div>
                                <i class="text-muted la la-user mr-5"></i> Adi Permana
                            </div>
                            <div>
                                <i class="text-muted la la-calendar mr-5"></i> Rabu, 3 Oktober 2019, 11:15
                            </div>
                        </div>
                        <div>
                            BANDUNG, itb.ac.id -- Seluruh keluarga besar Institut Teknologi Bandung menyampaikan rasa prihatin dan belasungkawa yang sedalam-dalamnya...
                        </div>
                        <div class="mt-20">
                            <a href="<?php echo \yii\helpers\Url::to(['/front/default/artikeldetail']) ?>" class="btn btn-block btn-main"><span>Selengkapnya</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mt-20">
            <nav aria-label="...">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1" aria-disabled="true"><i class="la la-angle-left"></i></a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item active" aria-current="page">
                        <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#"><i class="la la-angle-right"></i></a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</section>