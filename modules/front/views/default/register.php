<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<div class="valign-box">
    <div class="container">
        <div class="card no-margin">
            <div class="card-body pt-30 pr-30 pb-30 pl-30">
                <div class="row">
                    <div class="col-md-6 valign-wrapper">
                        <div class="valign-box text-center">
                            <img src="<?php echo \yii\helpers\Url::to(['/img/img-reg.png']) ?>" class="img-reg img-login" alt="Image">
                        </div>
                    </div>
                    <div class="col-md-6 valign-wrapper">
                        <div class="valign-box">
                            <div class="h3 colo-second title-font bold lh-1 text-center mb-15">
                                <img src="<?php echo \yii\helpers\Url::to(['/img/logo.png']) ?>" alt="Raudha Tour">
                            </div>
                            <?php echo Html::beginForm(\yii\helpers\Url::to(['/front/default/register'],true),'POST') ?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputFullname">Full Name</label>
                                            <input type="text" name="fullname" id="inputFullname" class="form-control" placeholder="Full Name" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputUsername">Username</label>
                                            <input type="text" name="username" id="inputUsername" class="form-control" placeholder="Username" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputPassword">Password</label>
                                            <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputrePassword">Re-Password</label>
                                            <input type="password" name="repassword" id="inputrePassword" class="form-control" placeholder="Re-Password" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputNotlp">No Telp</label>
                                            <input type="text" name="notlp" id="inputNotlp" class="form-control" placeholder="No Telp" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail">Email address</label>
                                            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center mt-15">
                                    <button class="btn btn-main btn-reg" type="submit"><i class="icofont-long-arrow-right"></i> Register</button>
                                </div>
                            <?php echo Html::endForm() ?>
                            <div class="text-center mt-30">
                                Suda punya akun? <a href="<?php echo \yii\helpers\Url::to(['/front/default/login']) ?>" class="colcor-main">Login</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>