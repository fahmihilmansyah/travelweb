<?php

namespace app\modules\cms\models;

use yii\base\Model;
use yii\web\UploadedFile;

class AdminFormModel extends Model
{
    /**
     * @var UploadedFile
     */
    public $uname;
    public $upass;
    public $email;
    public $name;

    public function rules()
    {
        return [
            [['uname','upass','email','name','password'], 'required'],
        ];
    }
}