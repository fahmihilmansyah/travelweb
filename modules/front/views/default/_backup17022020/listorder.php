<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 05/12/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 06.55
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
 ?>
<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 02/12/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 09.51
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
//print_r($detail_perjalanan);exit;
?>
<div class="col-md-12">
 <div class="card" >
  <div class="card-body">
      <h5 class="card-title">List Order</h5>
   <table class="table table-bordered table-striped">
    <thead>
     <tr>
      <th>Pemesan</th>
      <th>Kode Booking</th>
      <th>Status Pemesanan</th>
      <th>Harga</th>
      <th>Tgl Pesan</th>
      <th>Action</th>
     </tr>
    </thead>
    <tbody>
     <?php foreach ($list as $r): ?>
     <tr>
      <td><?php echo $r['full_name']?></td>
      <td><?php echo $r['kode_booking']?></td>
      <td><?php echo $r['status_pemesanan']?></td>
      <td><?php echo number_format($r['harga'],0,'.',',')?></td>
      <td><?php echo $r['created_ad']?></td>
      <td><?php if($r['status_pemesanan'] == 'WAITING'){ ?>
       <a href="#" class="btn btn-outline-primary btn-sm"> <span class="fa fa-money"></span></a>
       <?php }?></td>
     </tr>
    <?php endforeach;?>
    </tbody>
   </table>
  </div>
 </div>
</div>
