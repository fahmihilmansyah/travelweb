<?php

namespace app\modules\front\controllers;

use Yii;
use app\fhhlib\Mylibs;
use yii\helpers\Url;
use yii\web\Controller;


/**
 * Default controller for the `front` module
 */
class DefaultController extends Controller
{
    public $layout = '@app/views/layouts/fmain.php';
    public $enableCsrfValidation = false;
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $data = Mylibs::getListPerjalanan(0,6);
        $artikel =json_decode( Mylibs::getListArtikel(0,3),true);
        $slide = json_decode(Mylibs::getListSlide(),true);
        $data = json_decode($data, true);
        return $this->render('dashboard', ['list' => $data['data'],'listartikel'=>$artikel,'slide'=>$slide['data']]);
    }

    function actionLogin()
    {
        $this->layout = '//main-login';
        if ($_POST) {
            $req = \Yii::$app->request->post();
            $username = $req['username'];
            $password = $req['password'];
            $data = Mylibs::postLogin($req);
            $datajson = json_decode($data, true);
            if ($datajson['rc'] == '00') {
                $session = Yii::$app->session;
                $session['login_member'] = $datajson;
                Yii::$app->session->setFlash('success', $datajson['msg']);

                return $this->redirect(Url::to(['/'], true));
            }
            Yii::$app->session->setFlash('error', $datajson['msg']);
        }
        return $this->render('login');
    }

    function actionRegister()
    {
        $this->layout = '//main-login';
        if ($_POST) {
            $req = \Yii::$app->request->post();
            $username = $req['username'];
            $password = $req['password'];
            $data = Mylibs::postRegister($req);
            print_r($data);
            exit;
        }
        return $this->render('register');
    }

    function actionDetails($id)
    {
        $data = Mylibs::getDetailPerjalanan($id);
        $data = json_decode($data, true);
        return $this->render('details', ['list' => $data['data'], 'id' => $id]);
    }

    function actionLogout()
    {
        $session = Yii::$app->session;
        $session->destroy();
        return $this->redirect(Url::to(["/"], true));
    }

    function actionBooking()
    {
        $session = Yii::$app->session;
//        if (empty($session['login_member'])) {
//            return $this->redirect(Url::to(['/front/default/login']));
//        }
        if ($_POST) {
            $req = Yii::$app->request->post();
            $data = Mylibs::getDetailPerjalanan($req['id']);
            $user = $session['login_member']['data']['detail_user'];
//            print_r($user);exit;
            $data = json_decode($data, true);
            return $this->render('booking', ['detail_perjalanan' => $data['data'], 'detail_user' => $user]);
        }
        $req = Yii::$app->request->get();
        $data = Mylibs::getDetailPerjalanan($req['id']);
        $user = $session['login_member']['data']['detail_user'] ?? [];
//            print_r($user);exit;
        $data = json_decode($data, true);
        return $this->render('booking', ['detail_perjalanan' => $data['data'], 'detail_user' => $user]);
    }

    function actionDobooking()
    {
        $this->enableCsrfValidation = false;
        $listpembayran = Mylibs::getListPembayaran();
        if ($_POST) {
            $req = Yii::$app->request->post();
            $id_user = $req['id_user'];
            $id_perjalanan = $req['id_perjalanan'];
            $namapemesan = $req['namapemesan'];
            $emailpemesan = $req['emailpemesan'];
            $telppemesan = $req['telppemesan'];
            $userdata = $req['user'];
            $getbooking = Mylibs::postBooking(['full_name'=>$namapemesan,'email'=>$emailpemesan,'no_hp'=>$telppemesan,'id_perjalanan' => $id_perjalanan, 'member_id' => $id_user]);

            $kodebooking = (json_decode($getbooking, true))['data']['kode_booking'];
            $jmlusr = count($userdata['gelar']);
            $arrtmp = [];
            for ($i = 0; $i < $jmlusr; $i++) {
                foreach ($userdata as $k => $v) {
                    $arrtmp[$i][$k] = $v[$i];
                }
            }

            $userjson = json_encode($arrtmp);
            $postjamaah = Mylibs::postJamaah($kodebooking, $userjson);

            $sendMail = Mylibs::sendBookingMail([
                'booking_kode' => $kodebooking,
                'emailTo' => $emailpemesan,
                'urlRedirect' => \Yii::$app->params['image_host'].'front/default/bookingwaiting?bookingid='.$kodebooking
            ]);

            return json_encode(['link'=>Url::to(['/front/default/bookingwaiting','bookingid'=>$kodebooking],true)]);
//            print_r($listpembayran);exit;
//            return $this->render('booking-waiting',['listbayar'=>$listpembayran,'result'=>json_decode($postjamaah,true)]);
        }
    }
    function actionBookingwaiting($bookingid=''){
        $listpembayran = Mylibs::getListPembayaran();
        $postjamaah = Mylibs::getBookingdetail($bookingid);
//        print_r($postjamaah);exit;
        return $this->render('booking-waiting',['listbayar'=>$listpembayran,'result'=>json_decode($postjamaah,true)]);
    }
    function actionMemberorderlist(){
        $session = Yii::$app->session;
        if (empty($session['login_member'])) {
            return $this->redirect(Url::to(['/front/default/login']));
        }
        if($session['login_member']['data']['detail_user']['jenis'] != 'MEMBER'){
            return $this->redirect(Url::to(['/'],true));
        }
        $token = $session['login_member']['data']['login_token']['token'];
        $list = Mylibs::getListorder($session['login_member']['data']['detail_user']['id'],$token);
        return $this->render('listorder',['list'=>(json_decode($list,true))['data']]);
    }
    function actionUploadbukti($kode_booking){
        $session = Yii::$app->session;
        if (empty($session['login_member'])) {
            return $this->redirect(Url::to(['/front/default/login']));
        }
        if($session['login_member']['data']['detail_user']['jenis'] != 'MEMBER'){
            return $this->redirect(Url::to(['/'],true));
        }
        if($_POST){

        }
        return $this->render('uploadbukti');
    }
    function actionArtikel(){
        return $this->render('artikel/index');
    }
    function actionArtikeldetail(){

        $req = Yii::$app->request->get();
        $data = json_decode(Mylibs::getDetailArtikel($req['id']),true);
//        print_r($data);exit;
        return $this->render('artikel/detail',['list'=>$data]);
    }
    function actionDoPembayaran(){
        $req = Yii::$app->request;
        if($req->isPost){
            $bayar = json_decode(Mylibs::getBayar($req->post()),true);

            if($bayar['rc'] == '0000'){
                if(!empty($bayar['data']['qris'])) {
                    $qrisdata = $bayar['data']['qris'];
                    return $this->renderPartial('res_pembayaran_qris', ['list' => $qrisdata]);
                }
                if(!empty($bayar['data']['no_va'])) {
                    $no_va = $bayar['data']['no_va'];
                    $html_guide = $bayar['data']['html_guide'];
                    return $this->renderPartial('res_pembayaran', ['list' => $no_va,'guide'=>$html_guide]);
                }
                if(!empty($bayar['data']['landing_page'])) {
                    $landingpage = $bayar['data']['landing_page'];
                    return $this->redirect($landingpage);
                    print_r($bayar);exit;
                    return $this->renderPartial('res_pembayaran_qris', ['list' => $qrisdata]);
                }
            }
            print_r($bayar);exit;
        }
    }
    function actionTest(){
        return $this->render('res_pembayaran_qris');
    }
}
