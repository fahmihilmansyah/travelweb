<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 02/12/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 09.51
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
//print_r($detail_perjalanan);exit;
?>
<div class="col-md-12">
    <div class="card" >
        <div class="card-body">
            <table class="table">
                <tr>
                    <td colspan="2" class="text-center"><b>Menununggu Pembayaran</b></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center">&nbsp;</td>
                </tr>
                <tr>
                    <td>Nama Paket:</td>
                    <td><?php echo $result['data']['paket']?></td>
                </tr>
                <tr>
                    <td>Kode Booking:</td>
                    <td><?php echo $result['data']['kode_booking']?></td>
                </tr>
                <tr>
                    <td>Jumlah Jamaah:</td>
                    <td><?php echo $result['data']['jumlah']?></td>
                </tr>
                <tr>
                    <td>Total bayar:</td>
                    <td><?php echo number_format($result['data']['total_bayar'],0,'.',',')?></td>
                </tr>
                <tr>
                    <td colspan="2" class="text-center"><b><?php echo $result['data']['pesan']?></b></td>
                </tr>
            </table>
        </div>
    </div>
</div>