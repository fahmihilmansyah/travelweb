<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 08.52
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
$perjalanangambar = !empty($detailperjalanan['data']['perjalanan_image']) ? $detailperjalanan['data']['perjalanan_image'] : [];

?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-3">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Gambar</span>
<!--                    <input type="text" id="urlgambar" class="form-control resimg">-->
                    <input type="file" name="imagefiles" class="form-control" accept="image/*">
                </div>
            </div>
            <div class="col-3">
                <br>
                <label id="btnaddgambar" style="display: none;" class="btn btn-outline-primary">Tambah Gambar</label>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>URL Gambar</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody id="resgambar">
                        <?php foreach ($perjalanangambar as $r): ?>
                            <tr>
                                <td>
                                    <?php echo $r['url_link'] ?>
                                    <input type="hidden" name="perjalanan_image[id][]" value="<?php echo $r['id'] ?>">
                                </td>
                                <td>
                                    <label class="btn btn-outline-danger btn-sm removetr"><span class="fa fa-trash"></span></label>
                                </td>
                                </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    jQuery(document).on('click','#btnaddgambar',function(){
        var urlgambar = $("#urlgambar").val();
        $("#resgambar").append('<tr>'+
            '<td>'+urlgambar+'<input type="hidden" name="perjalanan_image[url_link][]" value="'+urlgambar+'"></td>'+
            '<td><label class="btn btn-outline-danger btn-sm removetr"><span class="fa fa-trash"></span></label></td>'+
            '</tr>');
        $('.resimg').val('');
    });
</script>
 