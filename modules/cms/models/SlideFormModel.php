<?php

namespace app\modules\cms\models;

use yii\base\Model;
use yii\web\UploadedFile;

class SlideFormModel extends Model
{
    /**
     * @var UploadedFile
     */
    public $picture;
    public $hiddenAttr;
    public $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    public $url_link;
    public $action_link;
    public $slide_type;
    public $judul;
    public $description;

    public function rules()
    {
        return [
            [['picture'], 'required'],
            [['action_link','judul','description'], 'string'],
            [['hiddenAttr'], 'string'],
        ];
    }
    
    public function upload()
    {
        if ($this->validate()) {
            $locationName = 'uploads/slides/'. $this->generate_string($this->permitted_chars, 20).'_'.$this->picture->name;
            $this->picture->saveAs($locationName);
            return \Yii::$app->params['image_host'].$locationName;
        } else {
            return false;
        }
    }

    private function generate_string($input, $strength = 16) {
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }
     
        return $random_string;
    }
}