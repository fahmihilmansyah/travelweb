<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 19/02/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 13.53
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>
<div class="content-header">
    <div class="container-fluid">
        <h3>List Perjalanan</h3>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <a href="<?php echo \yii\helpers\Url::to(['/cms/default/create'],true) ?>" class="btn btn-outline-primary float-right"><i class="fa fa-plus"></i> Tambah Perjalanan</a>
            </div>
            <div class="col-md-12">&nbsp;</div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="tablerekap" class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Judul</th>
                            <th>Agen</th>
                            <th>Durasi Perjalanan</th>
                            <th>Berangkat Dari</th>
                            <th>Hotel Kelas</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var table = $("#tablerekap");
    table.DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        ajax: {
            type: 'GET',
            url: '<?php echo \yii\helpers\Url::toRoute(['/cms/default/perjalananlist']); ?>',
            data: function (d) {
                return $.extend({}, d, {
                    extra_search: []//$('#advancedsearch-form').serializeArray()
                });
            }
            // ,
            // success:function (msg) {
            //     getfileclik();
            //     return msg;
            // }
        },
        ordering: false,
        columnDefs: [{
            orderable: false,
            targets: "center",
            className: 'dt-body-center'
        }],
        pageLength: 10,
        searching: false
    });
</script>
