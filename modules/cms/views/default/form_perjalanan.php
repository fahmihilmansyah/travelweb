<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 08.05
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
//echo "<pre>";print_r($detailperjalanan);exit;
$urllink = ['/cms/default/docreate'];
if(!empty($detailperjalanan['data']['perjalanan'])){
    $urllink = ['/cms/default/doupdate','id'=>$detailperjalanan['data']['perjalanan']['id'] ];
}
?>
<div class="content-header">
    <div class="container-fluid">
        <h3>Create/Edit Perjalanan</h3>
    </div>
</div>
<?php echo \yii\helpers\Html::beginForm(\yii\helpers\Url::to($urllink,true),'POST',['enctype'=>'multipart/form-data']); ?>
<?php if(!empty($detailperjalanan)): ?>
<input type="hidden" name="id_perjalanan" value="<?php echo $detailperjalanan['data']['perjalanan']['id'] ?>">
<?php endif; ?>
<div class="card">
    <div class="card-body">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Perjalanan</a>
                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-fasilitas" role="tab" aria-controls="nav-profile" aria-selected="false">Fasilitas</a>
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-gambar" role="tab" aria-controls="nav-contact" aria-selected="false">Gambar</a>
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-maskapai" role="tab" aria-controls="nav-contact" aria-selected="false">Maskapai</a>
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-rencanaperjalanan" role="tab" aria-controls="nav-contact" aria-selected="false">Rencana Perjalanan</a>
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-harga" role="tab" aria-controls="nav-contact" aria-selected="false">Paket Harga</a>
                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-hotel" role="tab" aria-controls="nav-contact" aria-selected="false">Hotel</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                <?php echo $this->render('_perjalanan',['infokategori'=>$infokategori,'infopesawat'=>$infopesawat,'detailperjalanan'=>$detailperjalanan]); ?>
            </div>
            <div class="tab-pane fade" id="nav-fasilitas" role="tabpanel" aria-labelledby="nav-profile-tab">
                <?php echo $this->render('_fasilitas',['detailperjalanan'=>$detailperjalanan]); ?>
            </div>
            <div class="tab-pane fade" id="nav-gambar" role="tabpanel" aria-labelledby="nav-contact-tab">
                <?php echo $this->render('_gambar',['detailperjalanan'=>$detailperjalanan]); ?>
            </div>
            <div class="tab-pane fade" id="nav-maskapai" role="tabpanel" aria-labelledby="nav-contact-tab">
                <?php echo $this->render('_maskapai',['infopesawat'=>$infopesawat,'detailperjalanan'=>$detailperjalanan]); ?>
            </div>
            <div class="tab-pane fade" id="nav-rencanaperjalanan" role="tabpanel" aria-labelledby="nav-contact-tab">
                <?php echo $this->render('_rencanaperjalanan',['detailperjalanan'=>$detailperjalanan]); ?>
            </div>
            <div class="tab-pane fade" id="nav-harga" role="tabpanel" aria-labelledby="nav-contact-tab">
                <?php echo $this->render('_paketharga',['detailperjalanan'=>$detailperjalanan]); ?>
            </div>
            <div class="tab-pane fade" id="nav-hotel" role="tabpanel" aria-labelledby="nav-contact-tab">
                <?php echo $this->render('_hotel',['infohotel'=>$infohotel,'detailperjalanan'=>$detailperjalanan]); ?>
            </div>
        </div>
    </div>
    <div class="card-footer text-right">
        <a href="<?php echo  \yii\helpers\Url::to(['/cms/default/perjalanan'])?>" class=" btn btn-outline-danger"><span class="fa fa-arrow-alt-circle-left"></span> Back</a>
        <button type="submit" class=" btn btn-outline-info"><span class="fa fa-save"></span> Submit</button>
    </div>
</div>
<?php echo \yii\helpers\Html::endForm();?>

<script>

    jQuery(document).on('click', '.removetr', function () {
        $(this).closest('tr').remove();
    });
</script>