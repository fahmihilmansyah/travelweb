<?php

namespace app\modules\cms\controllers;

use app\fhhlib\Mylibs;
use Yii;
use app\fhhlib\ApiAdm;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Default controller for the `cms` module
 */
class DefaultController extends Controller
{

    public $layout = '@app/views/layouts/backmain.php';

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPerjalanan()
    {
        return $this->render('perjalanan');
    }

    function actionPerjalananlist()
    {
        $req = \Yii::$app->request->get();
        $length = $req['length'] == NULL ? 10 : $req['length'];
        $start = $req['start'] == NULL ? 0 : $req['start'];
        $listperjalanan = ApiAdm::getListPerjalanan($start, $length,\Yii::$app->params['adm_token']);
        $listperjalanan = json_decode($listperjalanan, true);
        if ($listperjalanan['rc'] != '00' && empty($listperjalanan['data']['list'])) {

            $data['draw'] = 1;
            $data['recordsTotal'] = 0;
            $data['recordsFiltered'] = 0;
            $data['start'] = 0;
            $data['length'] = 0;
            $data['data'] = array();
        } else {

            $data['draw'] = $req['draw'];
            $data['recordsTotal'] = $listperjalanan['data']['recordsTotal'];
            $data['recordsFiltered'] = $listperjalanan['data']['recordsFiltered'];
            foreach ($listperjalanan['data']['list'] as $r) {
                $data['data'][] = array($r['judul'],
                    $r['nama_agen'],
                    $r['durasi_perjalanan'],
                    $r['berangkat_dari'], $r['kelas_hotel'],
                    '<a href="' . Url::to(['/cms/default/editperjalanan', 'id' => $r['id']]) . '" class="btn btn-outline-warning"><i class="fa fa-edit"></i></a>'
                );
            }
            $data['start'] = 0;
            $data['length'] = 0;
        }
        echo json_encode($data);
        exit;
    }

    function actionCreate()
    {
        $infokategori = json_decode(ApiAdm::getinfokategori(), true);
        $infopesawat = json_decode(ApiAdm::getinfopesawat(), true);
        $infohotel = json_decode(ApiAdm::getinfohotel(), true);
        return $this->render('form_perjalanan', ['infokategori' => $infokategori, 'infohotel' => $infohotel, 'infopesawat' => $infopesawat, 'detailperjalanan' => []]);
    }

    function actionDocreate()
    {
        $target_file = '';
        $filenames = '';
        if (!empty($_FILES['imagefiles']['tmp_name'])) {
            $imgname = $_FILES['imagefiles'];
            $exts = pathinfo($imgname['name'], PATHINFO_EXTENSION);
            $filenames = time() . rand(0, 999) . "." . $exts;
            $target_file = \Yii::getAlias('@webroot') . "/uploads/" . $filenames;
            $check = getimagesize($imgname["tmp_name"]);
            $uploadOk = 0;
            if ($check !== false) {
                $uploadOk = 1;
            }
            try {

                if ($uploadOk == 0) {
                    echo "Sorry, your file was not uploaded.";
                } else {
                    if (!move_uploaded_file($imgname["tmp_name"], $target_file)) {
                        echo "Sorry, there was an error uploading your file.";
                        exit;
                    }
                }
            } catch (\Exception $e) {
                print_r($e->getMessage());
                exit;
            }
        }
        if (!empty($target_file)) {
            $_POST['perjalanan_image']['url_link'][] = Url::to(['/uploads/' . $filenames], true);
        }
        $insertdata = ApiAdm::insertperjalanan($_POST, \Yii::$app->params['adm_token']);
        return $this->redirect(Url::to(['/cms/default/perjalanan'], true));
        echo $insertdata;
        echo "<br>";
//        print_r(http_build_query($_POST));
        exit;
    }

    function actionEditperjalanan($id = null)
    {
        $detailperjalanan = json_decode(ApiAdm::getdetailperjalanan($id,\Yii::$app->params['adm_token']), true);
        $infokategori = json_decode(ApiAdm::getinfokategori(), true);
        $infopesawat = json_decode(ApiAdm::getinfopesawat(), true);
        $infohotel = json_decode(ApiAdm::getinfohotel(), true);
        return $this->render('form_perjalanan', ['infokategori' => $infokategori, 'infohotel' => $infohotel, 'infopesawat' => $infopesawat, 'detailperjalanan' => $detailperjalanan]);
        print_r($detailperjalanan);
        exit;
    }

    function actionDoupdate($id = '')
    {

        $target_file = '';
        $filenames = '';
        if (!empty($_FILES['imagefiles']['tmp_name'])) {
            $imgname = $_FILES['imagefiles'];
            $exts = pathinfo($imgname['name'], PATHINFO_EXTENSION);
            $filenames = time() . rand(0, 999) . "." . $exts;
            $target_file = \Yii::getAlias('@webroot') . "/uploads/" . $filenames;
            $check = getimagesize($imgname["tmp_name"]);
            $uploadOk = 0;
            if ($check !== false) {
                $uploadOk = 1;
            }
            try {

                if ($uploadOk == 0) {
                    echo "Sorry, your file was not uploaded.";
                } else {
                    if (!move_uploaded_file($imgname["tmp_name"], $target_file)) {
                        echo "Sorry, there was an error uploading your file.";
                        exit;
                    }
                }
            } catch (\Exception $e) {
                print_r($e->getMessage());
                exit;
            }
        }
        if (!empty($target_file)) {
            $_POST['perjalanan_image']['url_link'][] = Url::to(['/uploads/' . $filenames], true);
        }
        $updateperjalan = ApiAdm::updateperjalanan($_POST, \Yii::$app->params['adm_token']);
        return $this->redirect(Url::to(['/cms/default/editperjalanan', 'id' => $id], true));
    }

    function actionLogin()
    {
        $this->layout = '/main-login';
        if ($_POST) {
            $req = \Yii::$app->request->post();
            $username = $req['username'];
            $password = $req['password'];
            $data = ApiAdm::postLoginAdmin($req);
//            print_r($data);exit;
            $datajson = json_decode($data, true);
            if ($datajson['rc'] == '00') {
                $session = Yii::$app->session;
                $session['login_admin'] = $datajson;
                Yii::$app->session->setFlash('success', $datajson['msg']);
                return $this->redirect(Url::to(['/cms/default/index'], true));
            }
            Yii::$app->session->setFlash('error', $datajson['msg']);
        }
        return $this->render('login');
    }
    function actionLogout()
    {
        unset($_SESSION['login_admin']);
        return $this->redirect(Url::to(['/cms/default/login']));
    }
}
