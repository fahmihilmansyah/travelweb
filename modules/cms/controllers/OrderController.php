<?php

namespace app\modules\cms\controllers;

use app\fhhlib\ApiAdm;
use yii\helpers\Url;
use yii\web\Controller;

/**
 * Default controller for the `cms` module
 */
class OrderController extends Controller
{

    public $layout = '@app/views/layouts/backmain.php';
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('list_artikel');
    }
    function actionListartikel(){
        $req = \Yii::$app->request->get();
        $length = $req['length'] == NULL ? 10 : $req['length'];
        $start = $req['start'] == NULL ? 0 : $req['start'] ;
        $listperjalanan = ApiAdm::getListArtikel($start,$length);
//        print_r($listperjalanan);exit;
        $listperjalanan = json_decode($listperjalanan,true);
        if($listperjalanan['rc'] != '00' && empty($listperjalanan['data']['list'])){

            $data['draw'] = 1;
            $data['recordsTotal'] = 0;
            $data['recordsFiltered'] = 0;
            $data['start'] = 0;
            $data['length'] = 0;
            $data['data'] = array(
            );
        }else {

            $data['draw'] =  $req['draw'];
            $data['recordsTotal'] = $listperjalanan['data']['recordsTotal'];
            $data['recordsFiltered'] = $listperjalanan['data']['recordsFiltered'];
            foreach ($listperjalanan['data']['list'] as $r) {
                $data['data'][] = array($r['title'],
                    $r['desc_short'],
                    $r['post_date'],
                    $r['name_author'],
                    $r['name_kategori'],
                    '<a href="'.Url::to(['/cms/artikel/editartikel','id'=>$r['id']]).'" class="btn btn-outline-warning"><i class="fa fa-edit"></i></a>'
                );
            }
            $data['start'] = 0;
            $data['length'] = 0;
        }
        echo json_encode($data);
        exit;
    }
    public function actionCreate()
    {
        $data['kategori_artikel'] = json_decode(ApiAdm::getartikelkatogori(), true)['data'];

        return $this->render('form_artikel',$data);
    }
    public function actionEditartikel()
    {
        $req = \Yii::$app->request->get();
        $data['kategori_artikel'] = json_decode(ApiAdm::getartikelkatogori(), true)['data'];
        $data['detail_artikel'] = json_decode(ApiAdm::getdetailartikel($req['id']), true)['data'];
        return $this->render('form_artikel',$data);
    }
    function actionDocreate(){
        if($_POST){
            if(empty($_POST['id_artikel'])){
                $insertdata = ApiAdm::insertartikel($_POST,'');
            }else{
                $updatedata = ApiAdm::editartikel($_POST,'');
            }
            return $this->redirect(Url::to(['/cms/artikel/index'],true));
        }
        exit;
    }
}
