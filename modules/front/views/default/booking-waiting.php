<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 02/12/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 09.51
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
//print_r($detail_perjalanan);exit;
?>
<section>
    <div class="container">
        <div class="text-big mb-15"><span class="badge badge-pill badge-info pt-5 pr-5 pb-5 pl-5"><i class="la la-info-circle"></i> Menunggu Pembayaran</span></div>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nama Paket</th>
                        <th>Kode Booking</th>
                        <th>Jumlah Jamaah</th>
                        <th class="text-right">Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="bold"><?php echo $result['data']['judul']?></td>
                        <td><?php echo $result['data']['kode_booking']?></td>
                        <td><?php echo $result['data']['jumlah_jamaah']?></td>
                        <td class="text-right"><?php echo number_format($result['data']['harga'],0,'.',',')?></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="bold text-right pt-30"><span class="h5 mb-0">DP</span></td>
                        <td class="text-right" style="vertical-align: bottom;"><span class="h5 mb-0"><?php echo number_format(1000000,0,'.',',')?></span></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="bold text-right"><span class="h4 mb-0 text-muted">Sisa Bayar</span></td>
                        <td class="bold text-right"><span class="h4 mb-0"><?php echo number_format($result['data']['harga'] - 1000000,0,'.',',')?></span></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="bold mt-20 mb-15 text-bigger"><?php echo $result['data']['pesan']?></div>
        <div id="resform">
        <?php echo \yii\helpers\Html::beginForm(\yii\helpers\Url::to(['/front/default/do-pembayaran'],true),'POST',['id'=>'formindp']) ?>
            <input type="hidden" name="invoice" value="<?php echo $result['data']['kode_booking']?>">
            <input type="hidden" name="amount" value="1000000">
            <ul class="nav nav-pills mb-20" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="transfer-tab" data-toggle="tab" href="#transfer" role="tab" aria-controls="transfer" aria-selected="false">Transfer Bank (VA)</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="setor-tab" data-toggle="tab" href="#setor" role="tab" aria-controls="setor" aria-selected="false">Setor Tunai</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade" id="transfer" role="tabpanel" aria-labelledby="transfer-tab">
                    <div class="row">
                        <?php foreach ($listbayar['data'] as $r):
                        if($r['type'] == 'VA'):
                        ?>
                            <div class="col-md-4">
                                <label class="custom-label-radio">
                                    <input type="radio" name="metodebayar" value="<?php echo $r['kode_produk'] ?>" id="va-1" class="d-none">
                                    <div class="custom-radio-box">
                                        <?php if(empty($r['url_img'])): ?>
                                        <div class="img-bank" style="background:rgba(0,0,0,.15) url()no-repeat center / contain;"></div>
                                        <?php else: ?>
                                            <div class="img-bank" style="background:rgba(0,0,0,.15) url(<?php echo $r['url_img'] ?>)no-repeat center / contain;"></div>
                                        <?php endif; ?>
                                        <span><?php echo $r['namaPembayaran'] ?></span>
                                    </div>
                                </label>
                            </div>
                        <?php endif; endforeach; ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="setor" role="tabpanel" aria-labelledby="setor-tab">
                    <div class="row">
                        <?php foreach ($listbayar['data'] as $r):
                        if($r['type'] == 'SETORTUNAI'):
                        ?>
                            <div class="col-md-4">
                                <label class="custom-label-radio">
                                    <input type="radio" name="metodebayar" value="<?php echo $r['kode_produk'] ?>" id="va-1" class="d-none">
                                    <div class="custom-radio-box">
                                        <?php if(empty($r['url_img'])): ?>
                                        <div class="img-bank" style="background:rgba(0,0,0,.15) url()no-repeat center / contain;"></div>
                                        <?php else: ?>
                                            <div class="img-bank" style="background:rgba(0,0,0,.15) url(<?php echo $r['url_img'] ?>)no-repeat center / contain;"></div>
                                        <?php endif; ?>
                                        <span><?php echo $r['namaPembayaran'] ?></span>
                                    </div>
                                </label>
                            </div>
                        <?php endif; endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="text-right mt-15">
                <label class="btn btn-main btn-lg" id="bayardp">Bayar DP <i class="la la-arrow-right"></i></label>
            </div>
        <?php echo \yii\helpers\Html::endForm() ?>
        </div>
    </div>
</section>
<script>
    jQuery(document).on('click','#bayardp',function (){
        var form = $('#formindp');
        var fattr = form.attr('action');
        var formdt = new FormData(form[0]);
        $.ajax({
            url : fattr,
            data : formdt,
            type : 'POST',
            processData: false,
            contentType: false,
            beforeSend:function (){
                // $("#resform").empty();
            },
            success : function(data){

                $("#resform").html(data);
            }
        });
        // console.log(formdt);
        // alert('test');
    })

</script>