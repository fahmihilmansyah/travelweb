<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 27/11/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 10.01
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */

namespace app\fhhlib;

class Mylibs{
    private $URL_API = 'http://localhost:8000/';
    // const URL_API='http://localhost/travel/public/';
    // const URL_API='http://194.31.53.26/ddtravel/public/';
    static function getListSlide(){
        $mylib = new Mylibs();
        $result = $mylib->_exec_data('front/listslide',"GET");
        return $result;
    }
    static function getListPerjalanan($offset=0,$limit=0){
        $mylib = new Mylibs();
        $othertext = '';
        if($limit > 0){
            $othertext = '/'.$offset.'/'.$limit;
        }
        $result = $mylib->_exec_data('front/listperjalanan'.$othertext,"GET");
        return $result;
    }
    static function getListArtikel($offset=0,$limit=0){
        $mylib = new Mylibs();
        $othertext = '';
        if($limit > 0){
            $othertext = '/'.$offset.'/'.$limit;
        }
        $result = $mylib->_exec_data('front/listartikel'.$othertext,"GET");
        return $result;
    }
    static function getDetailArtikel($id=''){
        $mylib = new Mylibs();
        $othertext = '';
        if(!empty($id)){
            $othertext = '/'.$id;
        }
        $result = $mylib->_exec_data('front/artikeldetail'.$othertext,"GET");
        return $result;
    }
    static function getDetailPerjalanan($id){
        $mylib = new Mylibs();
        $result = $mylib->_exec_data('front/detailperjalanan/'.$id,"GET");
        return $result;
    }
    static function getListPembayaran(){
        $mylib = new Mylibs();
        $result = $mylib->_exec_data('front/listpembayaran',"GET");
        $result = json_decode($result,true);
        return $result;
    }
    static function getListorder($idmember,$token){
        $mylib = new Mylibs();
        $result = $mylib->_exec_data('front/member/infobooking/'.$idmember,"GET",'',$token);
        return $result;
    }
    static function getBayar($data=array(),$token=''){
        $mylib = new Mylibs();
        $paramdata = http_build_query($data);
        $result = $mylib->_exec_data('trx/reqipg',"POST",$paramdata);
        return $result;
    }
    static function getBookingdetail($bookingid=[],$token=''){
        $mylib = new Mylibs();
        $paramdata = [];
        $result = $mylib->_exec_data('front/booking/'.urlencode($bookingid),"GET",$paramdata);
        return $result;
    }
    static function postBooking($data){
        $mylib = new Mylibs();
        $result = $mylib->_exec_data('front/booking',"POST",http_build_query($data));
//        print_r($result);exit;
        return $result;
    }
    static function postJamaah($kodebooking,$data){
        $mylib = new Mylibs();
        $result = $mylib->_exec_data('front/jamaah/'.$kodebooking,"POST",($data));
        return $result;
    }
    static function postLogin($data){
        $mylib = new Mylibs();
        $result = $mylib->_exec_data('front/loginmember',"POST",http_build_query($data));
        return $result;
    }
    static function postRegister($data){
        $mylib = new Mylibs();
        $result = $mylib->_exec_data('front/registermember',"POST",http_build_query($data));
        return $result;
    }
    public function _exec_data($slug='',$method="GET",$query_str='',$token=''){
        //
        // A very simple PHP example that sends a HTTP POST to a remote site
        //
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$this->URL_API.$slug);
        
        // curl_setopt($ch, CURLOPT_URL,\Yii::$app->params['host'].$slug);
        if(!empty($token)){
            $customHeaders = array(
                'X-TOKEN-LOGIN: '.$token
            );
            curl_setopt($ch, CURLOPT_HTTPHEADER, $customHeaders);
        }
        if($method != "GET") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $query_str);
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);

        // In real life you should use something like:
        // curl_setopt($ch, CURLOPT_POSTFIELDS,
        //          http_build_query(array('postvar1' => 'value1')));

        // Receive server response ...
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);
        curl_close ($ch);

        return $server_output;
    }

    public function _exec_data_file($slug,$file,$body,$token){
        $localFile = $file['uplfile']['tmp_name'];
        $uploadfile = \Yii::getAlias('@webroot').'/tmpfile/'.$file['uplfile']['name'];
        move_uploaded_file($file['uplfile']['tmp_name'], $uploadfile);
        $ch = curl_init();
        $curlFile = curl_file_create($uploadfile);
        $post['bukti_file'] =  $curlFile ;
        array_push($post,$body);
        $ch = curl_init();
        $customHeaders = array(
            'X-TOKEN-LOGIN: '.$token
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $customHeaders);
        curl_setopt($ch, CURLOPT_URL,$this->URL_API.$slug);
        // curl_setopt($ch, CURLOPT_URL,Yii::$app->params['host'].$slug);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        $result=curl_exec ($ch);
        curl_close ($ch);
        unlink($uploadfile);
    }

    static function sendBookingMail($data = array(), $token = ''){
        $mylib = new Mylibs();
        $endpoint = 'front/sendBookingMail/'.$data['booking_kode'].'?emailTo='.$data['emailTo'].'&urlRedirect='.$data['urlRedirect'];
        $result = $mylib->_exec_data($endpoint,"GET",[],$token);
       
        return $result;
    }
}
