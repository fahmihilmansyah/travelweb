<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 27/11/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 10.17
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>

<?php
/**
 * Created by PhpStorm.
 * Project : travelweb
 * User: fahmihilmansyah
 * Date: 27/11/19
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 06.15
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
?>

<?php foreach ($list as $r): ?>
    <div class="col-sm-4 col-md-4 " style="padding-bottom: 10px;">
        <a href="<?php echo \yii\helpers\Url::to(['/front/default/details','id'=>$r['id']],true)  ?>" style="">
            <div class="card cardf shadow bg-white rounded">
                <img src="<?php echo $r['url_img'][0] ?>"
                     class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title text-ellipses"
                        title="<?php echo $r['judul'] ?>"> <?php echo $r['judul'] ?> </h5>
                    <hr/>
                    <div class="card-text">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6 text-sm-grey">Mulai dari</div>
                                    <div class="col-md-6" style="color: #00a859">
                                        <b>Rp. <?php echo number_format($r['package'][0]['harga'], 0, ',', '.') ?></b>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr/>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 text-sm-grey">Jadwal Berangkat</div>
                                            <div class="col-md-6"><?php echo date("d M Y", strtotime($r['waktu_berangkat'])) ?></div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 text-sm-grey">Durasi Perjalanan</div>
                                            <div class="col-md-6"><?php echo $r['durasi_perjalanan'] ?>
                                                Hari
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 text-sm-grey">Kelas Hotel</div>
                                            <div class="col-md-6">
                                                <?php for ($i = 0; $i < 5; $i++) {
                                                    $is_cheked = '';
                                                    if ($i < $r['kelas_hotel']) {
                                                        $is_cheked = 'checked-star';
                                                    }
                                                    ?>
                                                    <span class="fa fa-star <?php echo $is_cheked ?>"></span>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <hr/>
                            </div>
                            <div class="col-md-12">
                                            <span style="color: grey"
                                                  class="float-right">Agen : <?php echo $r['nama_agen'] ?></span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </a>
    </div>
<?php endforeach; ?>