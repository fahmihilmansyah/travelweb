<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 03/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 09.16
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
$perjalananfasilitas = !empty($detailperjalanan['data']['perjalanan_fasilitas']) ? $detailperjalanan['data']['perjalanan_fasilitas'] : [];
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-3">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Nama Fasilitas</span>
                    <input type="text" id="namafasilitas" class="form-control resfasilitas">
                </div>
            </div>
            <div class="col-3">
                <div class="form-group mb-3">
                    <span id="inputGroup-sizing-sm">Jenis Fasilitas</span>
                    <select id="jenisfasilitas" class="form-control">
                        <option value="include">Include</option>
                        <option value="noinclude">No-Include</option>
                    </select>
                </div>
            </div>
            <div class="col-3">
                <br>
                <label id="btnaddfasilitas" class="btn btn-outline-primary">Tambah Fasilitas</label>
            </div>
        </div>
        <div class="row">
            <div class="col-6">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Nama Fasilitas</th>
                            <th>Jenis Fasilitas</th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody id="resfasilitas">
                        <?php foreach ($perjalananfasilitas as $r): ?>
                            <tr>
                                <td>
                                    <input type="hidden" name="perjalanan_fasilitas[id][]" value="<?php echo $r['id'] ?>">
                                    <?php echo $r['nama_fasilitas'] ?>
                                </td>
                                <td>
                                    <?php echo $r['jenis'] ?>
                                </td>
                                <td>
                                    <label class="btn btn-outline-danger btn-sm removetr"><span
                                                class="fa fa-trash"></span></label>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    jQuery(document).on('click', '#btnaddfasilitas', function () {
        var namafasilitas = $("#namafasilitas").val();
        var jenisfasilitasid = $("#jenisfasilitas").val();
        var jenisfasilitas = $("#jenisfasilitas option:selected").html();
        $("#resfasilitas").append('<tr>' +
            '<td>' + namafasilitas + '<input type="hidden" name="perjalanan_fasilitas[nama_fasilitas][]" value="' + namafasilitas + '"></td>' +
            '<td>' + jenisfasilitas + '<input type="hidden" name="perjalanan_fasilitas[jenis][]" value="' + jenisfasilitasid + '"></td>' +
            '<td><label class="btn btn-outline-danger btn-sm removetr"><span class="fa fa-trash"></span></label></td>' +
            '</tr>');
        $('.resfasilitas').val('');
    });
</script>

