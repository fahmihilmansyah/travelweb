<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<!--    <link rel="canonical" href="https://umroh.com">-->
    <link rel="stylesheet" href="<?php echo \yii\helpers\Url::to(['css/travel.css']) ?>">

    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <?php $this->head() ?>
</head>
<body class="u__user">
<?php $this->beginBody() ?>
<div data-server-rendered="true" id="__nuxt">
    <!---->
    <div id="__layout">
        <div>
            <div id="cover_all" class="cover_all" data-v-fe644ae0 data-v-99c31f42>
<!--                <div class="cover__banner" data-v-7df72d53 data-v-fe644ae0>-->
<!--                    <div id="widget-downloadapp" class="wrap_banner_download" data-v-7df72d53>-->
<!--                        <div class="container-fluid pl-0" data-v-7df72d53>-->
<!--                            <div class="row" data-v-7df72d53>-->
<!--                                <div class="col-3" data-v-7df72d53><span data-v-7df72d53><i class="el-icon-close"-->
<!--                                                                                            data-v-7df72d53></i></span>-->
<!--                                    <img src="https://res.cloudinary.com/umrohcom/image/upload/v1567409274/web/icon-umroh_2x.png"-->
<!--                                         alt="icon-umroh" data-v-7df72d53></div>-->
<!--                                <div class="title-download col-6"-->
<!--                                     data-v-7df72d53>-->
<!--                                    <h4 data-v-7df72d53>Dapatkan Tabungan Umroh</h4>-->
<!--                                    <p data-v-7df72d53>Hanya di Aplikasi Umroh.com</p>-->
<!--                                </div>-->
<!--                                <div class="col-3 btn-download" data-v-7df72d53><a href="https://bit.ly/androidumrohcom"-->
<!--                                                                                   target="_blank" class="btn-primary"-->
<!--                                                                                   data-v-7df72d53>Unduh</a></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <div class="cov_navbar" data-v-fe644ae0>
                    <div class="cov_nav" data-v-fe644ae0>
                        <div class="navbar_header" data-v-fe644ae0><a href="https://umroh.com/" class="logo"
                                                                      data-v-fe644ae0></a>
                            <menu class="marg_" data-v-fe644ae0>
                                <div class="menu" data-v-fe644ae0>
                                    <ul data-v-fe644ae0>
                                        <li data-v-fe644ae0><a href="https://umroh.com/paket" class="umroh_package"
                                                               data-v-fe644ae0>Paket Umroh</a></li>
                                        <li data-v-fe644ae0><span class="savings2" data-v-fe644ae0></span></li>
                                        <li data-v-fe644ae0><span target="_blank" class="passport_onlines"
                                                                  data-v-fe644ae0></span></li>
                                        <li class="others" data-v-fe644ae0>

                                        </li>
                                    </ul>
                                </div>
                                <div class="cov_menu" data-v-fe644ae0>
                                    <div class="menu_log" data-v-fe644ae0>
                                        <button type="button" class="el-button btn_in el-button--text" data-v-fe644ae0>
                                            <!----><!----><span>Masuk</span></button>
                                        <a href="https://umroh.com/login" data-v-fe644ae0>
                                            <button class="btn_reg" data-v-fe644ae0>Daftar</button>
                                        </a></div>
                                </div>
                            </menu>
                        </div>
                    </div>
                    <div class="cover_ham" data-v-fe644ae0>
                        <div class="ham" data-v-fe644ae0>
                            <div class="burger" data-v-fe644ae0>
                                <div data-v-fe644ae0></div>
                                <div data-v-fe644ae0></div>
                                <div data-v-fe644ae0></div>
                            </div>
                            <a href="https://umroh.com/" class="logo" data-v-fe644ae0></a>
                            <div style="margin: auto" data-v-fe644ae0><span data-v-fe644ae0><i class="icon-search"
                                                                                               data-v-fe644ae0></i></span>
                            </div>
                        </div>
                    </div>
                    <div role="presentation" class="el-dialog__wrapper cover_left_side" style="display:none;"
                         data-v-fe644ae0>
                        <div role="document" tabindex="-1" class="el-drawer__container">
                            <div aria-modal="true" aria-labelledby="el-drawer__title" role="presentation"
                                 class="el-drawer ltr" style="width:90%;">
                                <header id="el-drawer__title" class="el-drawer__header"><span role="heading"></span>
                                    <button aria-label="close drawer" type="button" class="el-drawer__close-btn"><i
                                                class="el-dialog__close el-icon el-icon-close"></i></button>
                                </header>
                                <!---->
                            </div>
                        </div>
                    </div>
                    <div class="left-search-side" data-v-fe644ae0>
                        <div role="presentation" class="el-dialog__wrapper cover_left_search" style="display:none;"
                             data-v-fe644ae0>
                            <div role="document" tabindex="-1" class="el-drawer__container">
                                <div aria-modal="true" aria-labelledby="el-drawer__title" role="presentation"
                                     class="el-drawer rtl" style="width:100%;">
                                    <header id="el-drawer__title" class="el-drawer__header"><span role="heading"></span>
                                        <button aria-label="close drawer" type="button" class="el-drawer__close-btn"><i
                                                    class="el-dialog__close el-icon el-icon-close"></i></button>
                                    </header>
                                    <!---->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="u_userLanding" data-v-99c31f42>
                <div class="cover bg" data-v-627cea1e data-v-627cea1e>
                    <div class="container_carousel" data-v-627cea1e>
                        <div class="carousel__el u_desktop_v el-carousel el-carousel--horizontal el-carousel--card"
                             data-v-627cea1e>
                            <div class="el-carousel__container">
                                <button type="button" class="el-carousel__arrow el-carousel__arrow--left"
                                        style="display:none;"><i class="el-icon-arrow-left"></i></button>
                                <button type="button" class="el-carousel__arrow el-carousel__arrow--right"
                                        style="display:none;"><i class="el-icon-arrow-right"></i></button>
                            </div>
                            <ul
                                    class="el-carousel__indicators el-carousel__indicators--horizontal el-carousel__indicators--outside"></ul>
                        </div>
                    </div>
                    <div class="carousel__el u_mobile_v el-carousel el-carousel--horizontal" data-v-627cea1e>
                        <div class="el-carousel__container">
                            <button type="button" class="el-carousel__arrow el-carousel__arrow--left"
                                    style="display:none;"><i class="el-icon-arrow-left"></i></button>
                            <button type="button" class="el-carousel__arrow el-carousel__arrow--right"
                                    style="display:none;"><i class="el-icon-arrow-right"></i></button>
                        </div>
                        <ul
                                class="el-carousel__indicators el-carousel__indicators--horizontal"></ul>
                    </div>
                </div>
                <div id="PackageFeed" class="package_feed" data-v-07e2620a>
                    <div class="mt-5" data-v-07e2620a>
                        <div class="u__container" data-v-07e2620a>
                            <div class="big_cover" data-v-07e2620a>
                                <div class="row mt-4" data-v-07e2620a>
                                    <div class="col-xl-4 col-lg-6 col-md-6" data-v-07e2620a>
                                        <a href="/tiraz-travel/umroh-bersama-kang-deden-hardiansyah"
                                           class="u_card_package w-100" data-v-7b027ed8 data-v-7b027ed8 data-v-07e2620a>
                                            <div class="u_card_img" data-v-7b027ed8><img
                                                        src="https://res.cloudinary.com/umrohcom/image/upload/c_fill,f_auto,b_rgb:000000,dpr_2.0,h_146,w_298,q_80,fl_progressive/v1/iqzziuwll565dnjtnvsy.jpg"
                                                        data-v-7b027ed8></div>
                                            <div class="u_card_info"
                                                 data-v-7b027ed8>
                                                <h5 data-v-7b027ed8>Umroh Special Bersama Kang Deden Hardiansyah</h5>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_price row" data-v-7b027ed8>
                                                    <div class="col-md-4 col-4 left" data-v-7b027ed8>
                                                        <p class="start_from" data-v-7b027ed8>Mulai dari</p>
                                                    </div>
                                                    <div class="col-md-8 col-8 right" data-v-7b027ed8>
                                                        <!---->
                                                        <div class="real_price" data-v-7b027ed8>
                                                            <p data-v-7b027ed8>Rp 23.900.000</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_detail_package row" data-v-7b027ed8>
                                                    <div class="col-md-6 col-6 static_section" data-v-7b027ed8>
                                                        <p data-v-7b027ed8><i class="icon-calendar-2"
                                                                              data-v-7b027ed8></i>Jadwal Berangkat</p>
                                                        <p data-v-7b027ed8><i class="icon-time" data-v-7b027ed8></i>Durasi
                                                            Perjalanan</p>
                                                        <p data-v-7b027ed8><i class="icon-hotel" data-v-7b027ed8></i>Kelas
                                                            Hotel</p>
                                                        <p data-v-7b027ed8><i class="icon-location" data-v-7b027ed8></i>Berangkat
                                                            dari</p>
                                                        <p data-v-7b027ed8><i class="icon-airplane" data-v-7b027ed8></i>Maskapai
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 col-6 dinamic_section" data-v-7b027ed8>
                                                        <p data-v-7b027ed8>Januari 2020</p>
                                                        <p data-v-7b027ed8>9 Hari</p>
                                                        <p data-v-7b027ed8><i class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i></p>
                                                        <p data-v-7b027ed8>Jakarta</p>
                                                        <p data-v-7b027ed8></p>
                                                        <p data-v-7b027ed8>Saudi Airlines</p>
                                                    </div>
                                                </div>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_partner" data-v-7b027ed8>
                                                    <div class="left" data-v-7b027ed8><img data-v-7b027ed8></div>
                                                    <div class="right" data-v-7b027ed8>
                                                        <p class="partner_name" data-v-7b027ed8>Tiraz Travel</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-4 col-lg-6 col-md-6" data-v-07e2620a>
                                        <a href="/annisa-travel/umroh-paket-reguler-1441-h-paket-4"
                                           class="u_card_package w-100" data-v-7b027ed8 data-v-7b027ed8 data-v-07e2620a>
                                            <div class="u_card_img" data-v-7b027ed8><img
                                                        src="https://res.cloudinary.com/umrohcom/image/upload/c_fill,f_auto,b_rgb:000000,dpr_2.0,h_146,w_298,q_80,fl_progressive/v1/idyxbqu8nrb601l5ikj0.jpg"
                                                        data-v-7b027ed8></div>
                                            <div class="u_card_info"
                                                 data-v-7b027ed8>
                                                <h5 data-v-7b027ed8>Umroh Paket Reguler 1441 H Paket 4</h5>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_price row" data-v-7b027ed8>
                                                    <div class="col-md-4 col-4 left" data-v-7b027ed8>
                                                        <p class="start_from" data-v-7b027ed8>Mulai dari</p>
                                                    </div>
                                                    <div class="col-md-8 col-8 right" data-v-7b027ed8>
                                                        <!---->
                                                        <div class="real_price" data-v-7b027ed8>
                                                            <p data-v-7b027ed8>Rp 30.500.000</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_detail_package row" data-v-7b027ed8>
                                                    <div class="col-md-6 col-6 static_section" data-v-7b027ed8>
                                                        <p data-v-7b027ed8><i class="icon-calendar-2"
                                                                              data-v-7b027ed8></i>Jadwal Berangkat</p>
                                                        <p data-v-7b027ed8><i class="icon-time" data-v-7b027ed8></i>Durasi
                                                            Perjalanan</p>
                                                        <p data-v-7b027ed8><i class="icon-hotel" data-v-7b027ed8></i>Kelas
                                                            Hotel</p>
                                                        <p data-v-7b027ed8><i class="icon-location" data-v-7b027ed8></i>Berangkat
                                                            dari</p>
                                                        <p data-v-7b027ed8><i class="icon-airplane" data-v-7b027ed8></i>Maskapai
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 col-6 dinamic_section" data-v-7b027ed8>
                                                        <p data-v-7b027ed8>Desember 2019</p>
                                                        <p data-v-7b027ed8>9 Hari</p>
                                                        <p data-v-7b027ed8><i class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i></p>
                                                        <p data-v-7b027ed8>Jakarta</p>
                                                        <p data-v-7b027ed8></p>
                                                        <p data-v-7b027ed8>Saudi Airlines</p>
                                                    </div>
                                                </div>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_partner" data-v-7b027ed8>
                                                    <div class="left" data-v-7b027ed8><img data-v-7b027ed8></div>
                                                    <div class="right" data-v-7b027ed8>
                                                        <p class="partner_name" data-v-7b027ed8>Annisa Travel</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-4 col-lg-6 col-md-6" data-v-07e2620a>
                                        <a href="/annisa-travel/umroh-paket-reguler-1441-h-paket-5"
                                           class="u_card_package w-100" data-v-7b027ed8 data-v-7b027ed8 data-v-07e2620a>
                                            <div class="u_card_img" data-v-7b027ed8><img
                                                        src="https://res.cloudinary.com/umrohcom/image/upload/c_fill,f_auto,b_rgb:000000,dpr_2.0,h_146,w_298,q_80,fl_progressive/v1/k2ojokmig3fskoavdmub.jpg"
                                                        data-v-7b027ed8></div>
                                            <div class="u_card_info"
                                                 data-v-7b027ed8>
                                                <h5 data-v-7b027ed8>Umroh Paket Reguler 1441 H Paket 5</h5>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_price row" data-v-7b027ed8>
                                                    <div class="col-md-4 col-4 left" data-v-7b027ed8>
                                                        <p class="start_from" data-v-7b027ed8>Mulai dari</p>
                                                    </div>
                                                    <div class="col-md-8 col-8 right" data-v-7b027ed8>
                                                        <!---->
                                                        <div class="real_price" data-v-7b027ed8>
                                                            <p data-v-7b027ed8>Rp 36.600.000</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_detail_package row" data-v-7b027ed8>
                                                    <div class="col-md-6 col-6 static_section" data-v-7b027ed8>
                                                        <p data-v-7b027ed8><i class="icon-calendar-2"
                                                                              data-v-7b027ed8></i>Jadwal Berangkat</p>
                                                        <p data-v-7b027ed8><i class="icon-time" data-v-7b027ed8></i>Durasi
                                                            Perjalanan</p>
                                                        <p data-v-7b027ed8><i class="icon-hotel" data-v-7b027ed8></i>Kelas
                                                            Hotel</p>
                                                        <p data-v-7b027ed8><i class="icon-location" data-v-7b027ed8></i>Berangkat
                                                            dari</p>
                                                        <p data-v-7b027ed8><i class="icon-airplane" data-v-7b027ed8></i>Maskapai
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 col-6 dinamic_section" data-v-7b027ed8>
                                                        <p data-v-7b027ed8>Desember 2019</p>
                                                        <p data-v-7b027ed8>9 Hari</p>
                                                        <p data-v-7b027ed8><i class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i></p>
                                                        <p data-v-7b027ed8>Jakarta</p>
                                                        <p data-v-7b027ed8></p>
                                                        <p data-v-7b027ed8>Saudi Airlines</p>
                                                    </div>
                                                </div>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_partner" data-v-7b027ed8>
                                                    <div class="left" data-v-7b027ed8><img data-v-7b027ed8></div>
                                                    <div class="right" data-v-7b027ed8>
                                                        <p class="partner_name" data-v-7b027ed8>Annisa Travel</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-4 col-lg-6 col-md-6" data-v-07e2620a>
                                        <a href="/fio-holiday/kajian-islam-umroh-plus-thaif-10-hari-qanaah-3"
                                           class="u_card_package w-100" data-v-7b027ed8 data-v-7b027ed8 data-v-07e2620a>
                                            <div class="u_card_img" data-v-7b027ed8><img
                                                        src="https://res.cloudinary.com/umrohcom/image/upload/c_fill,f_auto,b_rgb:000000,dpr_2.0,h_146,w_298,q_80,fl_progressive/v1/iavcnxnjh0ldkz76wo8d.jpg"
                                                        data-v-7b027ed8></div>
                                            <div class="u_card_info"
                                                 data-v-7b027ed8>
                                                <h5 data-v-7b027ed8>Kajian Islam &amp; Umroh Plus Thaif 10 Hari qana'ah
                                                    *3</h5>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_price row" data-v-7b027ed8>
                                                    <div class="col-md-4 col-4 left" data-v-7b027ed8>
                                                        <p class="start_from" data-v-7b027ed8>Mulai dari</p>
                                                    </div>
                                                    <div class="col-md-8 col-8 right" data-v-7b027ed8>
                                                        <!---->
                                                        <div class="real_price" data-v-7b027ed8>
                                                            <p data-v-7b027ed8>Rp 27.400.000</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_detail_package row" data-v-7b027ed8>
                                                    <div class="col-md-6 col-6 static_section" data-v-7b027ed8>
                                                        <p data-v-7b027ed8><i class="icon-calendar-2"
                                                                              data-v-7b027ed8></i>Jadwal Berangkat</p>
                                                        <p data-v-7b027ed8><i class="icon-time" data-v-7b027ed8></i>Durasi
                                                            Perjalanan</p>
                                                        <p data-v-7b027ed8><i class="icon-hotel" data-v-7b027ed8></i>Kelas
                                                            Hotel</p>
                                                        <p data-v-7b027ed8><i class="icon-location" data-v-7b027ed8></i>Berangkat
                                                            dari</p>
                                                        <p data-v-7b027ed8><i class="icon-airplane" data-v-7b027ed8></i>Maskapai
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 col-6 dinamic_section" data-v-7b027ed8>
                                                        <p data-v-7b027ed8>Desember 2019</p>
                                                        <p data-v-7b027ed8>10 Hari</p>
                                                        <p data-v-7b027ed8><i class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i></p>
                                                        <p data-v-7b027ed8>Jakarta</p>
                                                        <p data-v-7b027ed8></p>
                                                        <p data-v-7b027ed8>Saudi Airlines</p>
                                                    </div>
                                                </div>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_partner" data-v-7b027ed8>
                                                    <div class="left" data-v-7b027ed8><img data-v-7b027ed8></div>
                                                    <div class="right" data-v-7b027ed8>
                                                        <p class="partner_name" data-v-7b027ed8>Fio Holiday</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-4 col-lg-6 col-md-6" data-v-07e2620a>
                                        <a href="/fio-holiday/kajian-islam-umroh-plus-thaif-tawadhu-5"
                                           class="u_card_package w-100" data-v-7b027ed8 data-v-7b027ed8 data-v-07e2620a>
                                            <div class="u_card_img" data-v-7b027ed8><img
                                                        src="https://res.cloudinary.com/umrohcom/image/upload/c_fill,f_auto,b_rgb:000000,dpr_2.0,h_146,w_298,q_80,fl_progressive/v1/n31wdzbqfzc29xcarwrk.jpg"
                                                        data-v-7b027ed8></div>
                                            <div class="u_card_info"
                                                 data-v-7b027ed8>
                                                <h5 data-v-7b027ed8>Kajian Islam &amp; Umroh Plus Thaif Tawadhu *5</h5>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_price row" data-v-7b027ed8>
                                                    <div class="col-md-4 col-4 left" data-v-7b027ed8>
                                                        <p class="start_from" data-v-7b027ed8>Mulai dari</p>
                                                    </div>
                                                    <div class="col-md-8 col-8 right" data-v-7b027ed8>
                                                        <!---->
                                                        <div class="real_price" data-v-7b027ed8>
                                                            <p data-v-7b027ed8>Rp 29.800.000</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_detail_package row" data-v-7b027ed8>
                                                    <div class="col-md-6 col-6 static_section" data-v-7b027ed8>
                                                        <p data-v-7b027ed8><i class="icon-calendar-2"
                                                                              data-v-7b027ed8></i>Jadwal Berangkat</p>
                                                        <p data-v-7b027ed8><i class="icon-time" data-v-7b027ed8></i>Durasi
                                                            Perjalanan</p>
                                                        <p data-v-7b027ed8><i class="icon-hotel" data-v-7b027ed8></i>Kelas
                                                            Hotel</p>
                                                        <p data-v-7b027ed8><i class="icon-location" data-v-7b027ed8></i>Berangkat
                                                            dari</p>
                                                        <p data-v-7b027ed8><i class="icon-airplane" data-v-7b027ed8></i>Maskapai
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 col-6 dinamic_section" data-v-7b027ed8>
                                                        <p data-v-7b027ed8>Desember 2019</p>
                                                        <p data-v-7b027ed8>10 Hari</p>
                                                        <p data-v-7b027ed8><i class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i></p>
                                                        <p data-v-7b027ed8>Jakarta</p>
                                                        <p data-v-7b027ed8></p>
                                                        <p data-v-7b027ed8>Saudi Airlines</p>
                                                    </div>
                                                </div>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_partner" data-v-7b027ed8>
                                                    <div class="left" data-v-7b027ed8><img data-v-7b027ed8></div>
                                                    <div class="right" data-v-7b027ed8>
                                                        <p class="partner_name" data-v-7b027ed8>Fio Holiday</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-xl-4 col-lg-6 col-md-6" data-v-07e2620a>
                                        <a href="/fio-holiday/paket-liburan-akhir-tahun-umroh-kajian-isalm-9-hari"
                                           class="u_card_package w-100" data-v-7b027ed8 data-v-7b027ed8 data-v-07e2620a>
                                            <div class="u_card_img" data-v-7b027ed8><img
                                                        src="https://res.cloudinary.com/umrohcom/image/upload/c_fill,f_auto,b_rgb:000000,dpr_2.0,h_146,w_298,q_80,fl_progressive/v1/mi7hesu60oozrewwaovs.jpg"
                                                        data-v-7b027ed8></div>
                                            <div class="u_card_info"
                                                 data-v-7b027ed8>
                                                <h5 data-v-7b027ed8>Paket Liburan Akhir Tahun Umroh &amp; Kajian Islam 9
                                                    Hari</h5>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_price row" data-v-7b027ed8>
                                                    <div class="col-md-4 col-4 left" data-v-7b027ed8>
                                                        <p class="start_from" data-v-7b027ed8>Mulai dari</p>
                                                    </div>
                                                    <div class="col-md-8 col-8 right" data-v-7b027ed8>
                                                        <!---->
                                                        <div class="real_price" data-v-7b027ed8>
                                                            <p data-v-7b027ed8>Rp 30.100.000</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_detail_package row" data-v-7b027ed8>
                                                    <div class="col-md-6 col-6 static_section" data-v-7b027ed8>
                                                        <p data-v-7b027ed8><i class="icon-calendar-2"
                                                                              data-v-7b027ed8></i>Jadwal Berangkat</p>
                                                        <p data-v-7b027ed8><i class="icon-time" data-v-7b027ed8></i>Durasi
                                                            Perjalanan</p>
                                                        <p data-v-7b027ed8><i class="icon-hotel" data-v-7b027ed8></i>Kelas
                                                            Hotel</p>
                                                        <p data-v-7b027ed8><i class="icon-location" data-v-7b027ed8></i>Berangkat
                                                            dari</p>
                                                        <p data-v-7b027ed8><i class="icon-airplane" data-v-7b027ed8></i>Maskapai
                                                        </p>
                                                    </div>
                                                    <div class="col-md-6 col-6 dinamic_section" data-v-7b027ed8>
                                                        <p data-v-7b027ed8>Desember 2019</p>
                                                        <p data-v-7b027ed8>9 Hari</p>
                                                        <p data-v-7b027ed8><i class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i><i
                                                                    class="icon-star" data-v-7b027ed8></i></p>
                                                        <p data-v-7b027ed8>Jakarta</p>
                                                        <p data-v-7b027ed8></p>
                                                        <p data-v-7b027ed8>Etihad</p>
                                                    </div>
                                                </div>
                                                <hr data-v-7b027ed8>
                                                <div class="u_card_info_partner" data-v-7b027ed8>
                                                    <div class="left" data-v-7b027ed8><img data-v-7b027ed8></div>
                                                    <div class="right" data-v-7b027ed8>
                                                        <p class="partner_name" data-v-7b027ed8>Fio Holiday</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="row mt-4" data-v-07e2620a>
                                    <div class="col-lg-4" data-v-07e2620a></div>
                                    <div class="col-lg-4 col-md-12" data-v-07e2620a>
                                        <div style="display:;" data-v-07e2620a>
                                            <div class="lds-ring" data-v-3946dea8 data-v-07e2620a>
                                                <div data-v-3946dea8></div>
                                                <div data-v-3946dea8></div>
                                                <div data-v-3946dea8></div>
                                                <div data-v-3946dea8></div>
                                            </div>
                                        </div>
                                        <div style="display:none;" data-v-07e2620a><a
                                                    href="https://umroh.com/cari-paket/regular/semua-waktu/semua-keberangkatan/popularitas"
                                                    class="m-auto pb-3 pt-3 w-100 form-controll btn btn-outline-success see_more"
                                                    data-v-07e2620a><span data-v-07e2620a>Lihat Lebih Banyak</span></a>
                                        </div>
                                    </div>
                                    <div class="col-lg-4" data-v-07e2620a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="positionLimit"></div>
                <div class="u_seo_about" data-v-4743005f data-v-4743005f>
                    <div class="u__container mt-5 mb-5" data-v-4743005f>
                        <div class="covd" data-v-4743005f>
                            <h2 data-v-4743005f>Umroh.com – Cari Paket Umroh Murah di Indonesia </h2> <br
                                    data-v-4743005f>
                            <p class="mb-4" data-v-4743005f>Umroh.com adalah marketplace yang menyediakan paket umroh
                                lebih dari 100 travel umroh terpercaya di Indonesia dengan izin resmi Kementerian Agama.
                                Ibadah terasa mudah karena tidak lagi pusing untuk mencari travel umroh
                                dan temukan paket umroh yang diinginkan. Cari paket umroh dengan mudah dan aman,
                                banyaknya pilihan paket umroh dari berbagai biro umroh terpercaya. Cek berbagai paket
                                umroh pilihamu, ingin paket umroh regular, atau
                                ingin beribadah sekaligus berwisata ke Negara islam dengan umroh plus, hingga kamu bisa
                                umroh special bersama ustadz favoritmu. Temukan berbagai paket umroh regular, umroh
                                plus, hingga umroh special. Mudahkan juga perjalanan
                                dengan layanan pembuatan paspor online, pembelian paket data, ibadah nyaman semua mudah
                                ditemukan hanya dengan Umroh.com. Saat ini sudah banyak travel terpercaya yang sudah
                                bergabung dengan kami, Umroh.com ingin memudahkan
                                umat muslim Indonesia dalam beribadah, menghilangkan kekhawatiran dikalangan masyarakat
                                dan memberikan layanan yang terpercaya dan aman. Transaksi aman, ibadah nyaman bersama
                                Umroh.com</p>
                            <p class="all" data-v-4743005f>Selengkapnya</p>
                            <!---->
                        </div>
                    </div>
                </div>
            </div>
            <footer data-v-f5d52de8 data-v-99c31f42>
                <div class="desktop-footer" data-v-f5d52de8>
                    <div class="container-fluid top_footer mb-5" data-v-f5d52de8>
                        <div class="container" data-v-f5d52de8>
                            <div class="row" data-v-f5d52de8>
                                <div class="col" data-v-f5d52de8></div>
                                <div class="col-8" data-v-f5d52de8>
                                    <div class="d-flex cov" data-v-f5d52de8>
                                        <div class="image_phone" data-v-f5d52de8></div>
                                        <div class="desc" data-v-f5d52de8>
                                            <p data-v-f5d52de8>Download Aplikasi <br data-v-f5d52de8> Umroh.com</p>
                                        </div>
                                        <a href="https://bit.ly/androidumrohcom" target="_blank" data-v-f5d52de8>
                                            <div class="img_share_android mr-3" data-v-f5d52de8></div>
                                        </a>
                                        <a href="https://bit.ly/androidumrohcom" target="_blank" data-v-f5d52de8>
                                            <div class="img_share_appstore" data-v-f5d52de8></div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col" data-v-f5d52de8></div>
                            </div>
                        </div>
                    </div>
                    <div class="u__container" data-v-f5d52de8>
                        <div data-v-f5d52de8>
                            <div data-v-f5d52de8>
                                <div class="row" data-v-f5d52de8>
                                    <div class="col" data-v-f5d52de8>
                                        <div class="logo mb-4" data-v-f5d52de8></div>
                                        <div class="social_media" data-v-f5d52de8><span data-v-f5d52de8></span>
                                            <div class="desc" data-v-f5d52de8>
                                                <p data-v-f5d52de8>Whatsapp</p>
                                                <p data-v-f5d52de8>0852 1800 5011</p>
                                            </div>
                                        </div>
                                        <div class="email" data-v-f5d52de8><span data-v-f5d52de8></span>
                                            <div class="desc" data-v-f5d52de8>
                                                <p data-v-f5d52de8>Email</p>
                                                <p data-v-f5d52de8>info@umroh.com</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col" data-v-f5d52de8>
                                        <div class="about_company" data-v-f5d52de8>
                                            <div class="title" data-v-f5d52de8>
                                                <p data-v-f5d52de8>Tentang Perusahaan</p>
                                            </div>
                                            <ul data-v-f5d52de8>
                                                <li data-v-f5d52de8><a href="https://umroh.com/tentang-kami"
                                                                       data-v-f5d52de8>Tentang Kami</a></li>
                                                <li data-v-f5d52de8><a href="https://umroh.com/payment" data-v-f5d52de8>Cara
                                                        Pembayaran</a></li>
                                                <li data-v-f5d52de8><a href="https://umroh.com/blog/" data-v-f5d52de8>Berita</a>
                                                </li>
                                                <li data-v-f5d52de8><a href="https://umroh.com/tanya-jawab"
                                                                       data-v-f5d52de8>FAQ</a></li>
                                                <li data-v-f5d52de8><a href="https://umroh.com/syarat-ketentuan"
                                                                       data-v-f5d52de8>Syarat dan Ketentuan</a></li>
                                                <li data-v-f5d52de8><a href="https://umroh.com/kebijakan-privasi"
                                                                       data-v-f5d52de8>Kebijakan Privasi</a></li>
                                                <li data-v-f5d52de8><a href="https://umroh.com/sitemap" data-v-f5d52de8>Site
                                                        Map</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col" data-v-f5d52de8>
                                        <div class="product" data-v-f5d52de8>
                                            <div class="title" data-v-f5d52de8>
                                                <p data-v-f5d52de8>Produk</p>
                                            </div>
                                            <ul data-v-f5d52de8>
                                                <li data-v-f5d52de8><a href="https://umroh.com/cari-paket"
                                                                       data-v-f5d52de8>Paket Umroh</a></li>
                                                <li data-v-f5d52de8><a target="_blank"
                                                                       href="https://umroh.com/paspor-online"
                                                                       data-v-f5d52de8>Paspor Online</a></li>
                                                <li data-v-f5d52de8><a href="https://umroh.com/tabungan"
                                                                       data-v-f5d52de8>Tabungan Umroh</a></li>
                                                <li data-v-f5d52de8><a target="_blank"
                                                                       href="https://umroh.com/jadwal-sholat"
                                                                       data-v-f5d52de8>Jadwal Sholat</a></li>
                                                <li data-v-f5d52de8><a target="_blank" href="https://umroh.com/al-quran"
                                                                       data-v-f5d52de8>Al-Quran Online</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col" data-v-f5d52de8>
                                        <div class="travel_partner" data-v-f5d52de8>
                                            <div class="title" data-v-f5d52de8>
                                                <p data-v-f5d52de8>Travel Partner</p>
                                            </div>
                                            <ul data-v-f5d52de8>
                                                <li data-v-f5d52de8><a href="https://travel.umroh.com/" target="_blank"
                                                                       data-v-f5d52de8>Gabung Travel Partner</a></li>
                                                <li data-v-f5d52de8><a href="https://umroh.com/travel-umroh"
                                                                       data-v-f5d52de8>Travel Umroh</a></li>
                                            </ul>
                                        </div>
                                        <div class="social_media_follow" data-v-f5d52de8>
                                            <div class="title" data-v-f5d52de8>
                                                <p data-v-f5d52de8>Ikuti Kami</p>
                                            </div>
                                            <ul data-v-f5d52de8>
                                                <li data-v-f5d52de8><a target="_blank"
                                                                       href="https://wwww.facebook.com/umrohcom"
                                                                       data-v-f5d52de8><span class="icon-facebook"
                                                                                             data-v-f5d52de8></span>
                                                        Facebook</a></li>
                                                <li data-v-f5d52de8><a target="_blank"
                                                                       href="https://www.twitter.com/umrohdotcom"
                                                                       data-v-f5d52de8><span class="icon-twitter"
                                                                                             data-v-f5d52de8></span>
                                                        Twitter</a></li>
                                                <li data-v-f5d52de8><a target="_blank"
                                                                       href="https://www.instagram.com/umrohcom"
                                                                       data-v-f5d52de8><span class="icon-ig"
                                                                                             data-v-f5d52de8></span>
                                                        Instagram</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mobile-footer" data-v-f5d52de8>
                    <div class="container" data-v-f5d52de8>
                        <div class="row" data-v-f5d52de8>
                            <div class="col" data-v-f5d52de8>
                                <div class="logo d-block" data-v-f5d52de8></div>
                                <div class="d-flex cov_social" data-v-f5d52de8>
                                    <div class="social_media" data-v-f5d52de8><span data-v-f5d52de8></span>
                                        <div class="desc" data-v-f5d52de8>
                                            <p data-v-f5d52de8>Whatsapp</p>
                                            <p data-v-f5d52de8>-</p>
                                        </div>
                                    </div>
                                    <div class="email" data-v-f5d52de8><span data-v-f5d52de8></span>
                                        <div class="desc" data-v-f5d52de8>
                                            <p data-v-f5d52de8>Email</p>
                                            <p data-v-f5d52de8>info@travl.com</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="about_company" data-v-f5d52de8>
                                    <ul data-v-f5d52de8>
                                        <li data-v-f5d52de8><a href="https://travel.umroh.com/" target="_blank"
                                                               data-v-f5d52de8>Travel Umroh</a></li>
                                        <li data-v-f5d52de8><a href="https://umroh.com/tentang-kami" data-v-f5d52de8>Tentang
                                                Kami</a></li>
                                        <li data-v-f5d52de8><a href="https://umroh.com/payment" data-v-f5d52de8>Cara
                                                Pembayaran</a></li>
                                        <li data-v-f5d52de8><a href="https://umroh.com/blog/" data-v-f5d52de8>Berita</a>
                                        </li>
                                        <li data-v-f5d52de8><a href="https://umroh.com/tanya-jawab"
                                                               data-v-f5d52de8>FAQ</a></li>
                                        <li data-v-f5d52de8><a href="https://umroh.com/syarat-ketentuan"
                                                               data-v-f5d52de8>Syarat dan Ketentuan</a></li>
                                        <li data-v-f5d52de8><a href="https://umroh.com/kebijakan-privasi"
                                                               data-v-f5d52de8>Kebijakan Privasi</a></li>
                                        <li data-v-f5d52de8><a href="https://umroh.com/sitemap" data-v-f5d52de8>Site
                                                Map</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid sub_footer" data-v-f5d52de8>
                    <div class="container" data-v-f5d52de8>
                        <div class="row" data-v-f5d52de8>
                            <div class="col" data-v-f5d52de8>
                                <p class="text-center" data-v-f5d52de8>Copyright © 2019 Umroh.com</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
