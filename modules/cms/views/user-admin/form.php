<?php 
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\helpers\Url;
?>
<?php

$config = [
    'action' =>['user-admin/new'],
    'method' => 'post', 
    'options' => [ 
        'id' => 'userAdminForm', 
        // 'onsubmit' => 'return false;', 
    ] 
];

if(!empty($id)){
    $config['enableClientScript'] = false;
    $config['action'] = ['user-admin/save'];
}

$form = ActiveForm::begin($config); 

?>
    <input type="hidden" name="id" value="<?= $id ?? ''; ?>">
    <div class="content-header">
        <div class="container-fluid">
            <h3 class="table-title">
            <?php echo empty($id)?'Create':'Edit'; ?>
            Slide
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                    <a href="<?php echo \yii\helpers\Url::to(['/cms'],true) ?>">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                    <a href="<?php echo \yii\helpers\Url::to(['/cms/artikel'],true) ?>"
                        >List of User Admnistrator</a
                    >
                    </li>
                    <li class="breadcrumb-item active">
                    <a href="#"
                        ><?php echo empty($id)?'Create':'Edit'; ?>
                        Create User Administrator</a
                    >
                    </li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="my-form">
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <?php echo $form->field($adminModel, 'name')
                                                        ->textInput([
                                                                'placeholder' => 'Example name here...', 
                                                                'value' => $adminModel->name ?? '', 
                                                                'maxlength' => true
                                                            ])->label(false);
                                        ?>
                                    </div>    
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <?php echo $form->field($adminModel, 'email')
                                                        ->textInput([
                                                                'placeholder' => 'Example action link here...', 
                                                                'value' => $adminModel->email ?? '', 
                                                                'maxlength' => true
                                                            ])->label(false);
                                        ?>
                                    </div>    
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <?php echo $form->field($adminModel, 'uname')
                                                        ->textInput([
                                                                'placeholder' => 'Example username here...', 
                                                                'value' => $adminModel->uname ?? '', 
                                                                'maxlength' => true
                                                            ])->label(false);
                                        ?>
                                    </div>    
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <?php echo $form->field($adminModel, 'password')
                                                        ->passwordInput([
                                                                'placeholder' => 'Example password here...', 
                                                                'value' => $adminModel->password ?? '', 
                                                                'maxlength' => true
                                                            ])->label(false);
                                        ?>
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <a
            href="<?php echo  \yii\helpers\Url::to(['/cms/image-slide/index'])?>"
            class="btn btn-outline-danger"
            ><span class="fa fa-arrow-alt-circle-left"></span> Back</a
            >
            <button type="submit" class="btn btn-outline-info">
            <span class="fa fa-save"></span> Submit
            </button>
        </div>
    </div>
<?php ActiveForm::end(); ?>
