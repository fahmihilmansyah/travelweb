<?php
/**
 * Created by PhpStorm.
 * Project : travelwebold
 * User: fahmihilmansyah
 * Date: 09/03/20
 * Email : fahmi.hilmansyah@gmail.com
 * Telp : 0817170820
 * Time: 23.00
 * Dilarang Keras Mengubah atau Mendistribusikan ulang code ini tanpa sepengetahuan.
 */
 ?>
<div class="content-header">
  <div class="container-fluid">
    <h3 class="table-title">List Slides</h3>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?php echo \yii\helpers\Url::to(['/cms'],true) ?>">Home</a></li>
            <li class="breadcrumb-item active"><a href="#">List of Slides</a></li>
        </ol>
    </nav>
  </div>
</div>
<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-md-12">
        <a
          href="<?php echo \yii\helpers\Url::to(['/cms/image-slide/create'],true) ?>"
          class="btn btn-outline-primary float-right"
          ><i class="fa fa-plus"></i> Tambah Image Slide</a
        >
      </div>
      <div class="col-md-12">&nbsp;</div>
      <div class="col-md-12">
        <div class="table-responsive">
          <table id="tableSlides" class="table table-bordered">
            <thead>
              <tr>
                <th width="80%">IMAGE</th>
                <!-- <th>Tagline</th>
                <th width="10%">Type</th> -->
                <th width="15%">Created At</th>
                <th width="10%">Action</th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  var table = $("#tableSlides");
  table.DataTable({
   responsive: true,
   processing: true,
   serverSide: true,
   ajax: {
    type: 'GET',
    url: '<?php echo \yii\helpers\Url::toRoute(['/cms/image-slide/lists']); ?>',
    data: function (d) {
     return $.extend({}, d, {
      extra_search: []
     });
    }
   },
   ordering: false,
   columnDefs: [{
    orderable: false,
    targets: "center",
    className: 'dt-body-center'
   }],
   pageLength: 10,
   searching: false,
   fixedColumns:   {
    rightColumns: 1
   }
  });
</script>
